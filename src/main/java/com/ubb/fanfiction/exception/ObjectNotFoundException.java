package com.ubb.fanfiction.exception;

public class ObjectNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ObjectNotFoundException(String mensaje) {
		super(mensaje);
	}
}
