package com.ubb.fanfiction.exception;

public class DaoException extends Exception{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public DaoException(String mensaje) {
		super(mensaje);
	}
}
