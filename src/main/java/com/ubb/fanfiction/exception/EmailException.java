package com.ubb.fanfiction.exception;

public class EmailException extends Exception{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EmailException(String mensaje) {
		super(mensaje);
	}
}
