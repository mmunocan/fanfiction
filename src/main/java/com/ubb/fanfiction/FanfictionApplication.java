package com.ubb.fanfiction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.ubb.fanfiction.configuration"})
@ComponentScan(basePackages = {"com.ubb.fanfiction.form"})
@ComponentScan(basePackages = {"com.ubb.fanfiction.controller", "com.ubb.fanfiction.service", "com.ubb.fanfiction.service.impl"})
@EntityScan("com.ubb.fanfiction.domain")
@EnableJpaRepositories("com.ubb.fanfiction.dao")
public class FanfictionApplication extends SpringBootServletInitializer{

	
	public static void main(String[] args) {
		SpringApplication.run(FanfictionApplication.class, args);
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(FanfictionApplication.class);
    }

}
