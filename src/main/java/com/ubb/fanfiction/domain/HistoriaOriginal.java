package com.ubb.fanfiction.domain;
// Generated 24-12-2017 19:07:28 by Hibernate Tools 5.2.3.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * HistoriaOriginal generated by hbm2java
 */
@Entity
@Table(name = "historia_original", catalog = "fanfictiondb")
public class HistoriaOriginal implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idHistoriaOriginal;
	private Categoria categoria;
	private String nombre;
	private Set<Fanfiction> fanfictions = new HashSet<Fanfiction>(0);

	public HistoriaOriginal() {
	}

	public HistoriaOriginal(Categoria categoria, String nombre) {
		this.categoria = categoria;
		this.nombre = nombre;
	}

	public HistoriaOriginal(Categoria categoria, String nombre, Set<Fanfiction> fanfictions) {
		this.categoria = categoria;
		this.nombre = nombre;
		this.fanfictions = fanfictions;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id_historia_original", unique = true, nullable = false)
	public Integer getIdHistoriaOriginal() {
		return this.idHistoriaOriginal;
	}

	public void setIdHistoriaOriginal(Integer idHistoriaOriginal) {
		this.idHistoriaOriginal = idHistoriaOriginal;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_categoria", nullable = false)
	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Column(name = "nombre", nullable = false, length = 45)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "historiaOriginals")
	public Set<Fanfiction> getFanfictions() {
		return this.fanfictions;
	}

	public void setFanfictions(Set<Fanfiction> fanfictions) {
		this.fanfictions = fanfictions;
	}

}
