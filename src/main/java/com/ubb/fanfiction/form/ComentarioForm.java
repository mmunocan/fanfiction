package com.ubb.fanfiction.form;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class ComentarioForm {
	@NotEmpty
	private String comentario;
	@NotNull
	private Integer idCapitulo;
	@NotNull
	private Integer idUsuario;
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Integer getIdCapitulo() {
		return idCapitulo;
	}
	public void setIdCapitulo(Integer idCapitulo) {
		this.idCapitulo = idCapitulo;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
	
}
