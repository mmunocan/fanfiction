package com.ubb.fanfiction.form;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

public class FanfictionForm {
	
	@NotEmpty(message = "Por favor, ingrese un título para su fanfiction")
	private String titulo;
	
	@NotEmpty(message = "Por favor, ingrese una sinopsis para su fanfiction")
	private String descripcion;
	
	private int[] historiasOriginales;
	
	private int[] generos;
	
	private String estado;
	
	@Min(1)
	private int clasificacionParental;
	
	@NotEmpty(message = "Por favor, ingrese un título para el primer capítulo")
	private String tituloCapitulo;
	
	@NotEmpty(message = "Por favor, ingrese el contenido del capítulo")
	private String contenido;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int[] getHistoriasOriginales() {
		return historiasOriginales;
	}

	public void setHistoriasOriginales(int[] historiasOriginales) {
		this.historiasOriginales = historiasOriginales;
	}

	public int[] getGeneros() {
		return generos;
	}

	public void setGeneros(int[] generos) {
		this.generos = generos;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getClasificacionParental() {
		return clasificacionParental;
	}

	public void setClasificacionParental(int clasificacionParental) {
		this.clasificacionParental = clasificacionParental;
	}

	public String getTituloCapitulo() {
		return tituloCapitulo;
	}

	public void setTituloCapitulo(String tituloCapitulo) {
		this.tituloCapitulo = tituloCapitulo;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
}
