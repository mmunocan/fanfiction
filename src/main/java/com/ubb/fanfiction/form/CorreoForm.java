package com.ubb.fanfiction.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class CorreoForm {

	@Email(message = "Por favor, ingrese un correo válido")
	@NotEmpty(message = "Por favor, ingrese su correo")
	private String correo;

	private String path;
	
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
}
