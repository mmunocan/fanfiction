package com.ubb.fanfiction.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class RegistroForm {
	
	@NotEmpty(message = "Por favor, ingrese su nombre de usuario")
	private String nombre;
	
	@Email(message = "Por favor, ingrese un correo válido")
	@NotEmpty(message = "Por favor, ingrese su correo")
	private String correo;
	
	@Length(min = 8, message = "Su contraseña debe ser de al menos 8 caracteres")
	private String clave;
	
	@NotEmpty(message = "Por favor, repita su contraseña")
	private String clave2;
	
	private String descripcion;
	
	private String path;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getClave2() {
		return clave2;
	}
	public void setClave2(String clave2) {
		this.clave2 = clave2;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
