package com.ubb.fanfiction.form;

public class OrdenarFanfictionForm {
	private int atributoOrden;
	private int ordenBusqueda;
	
	public final static int FECHA_ACTUALIZACION = 1;
	public final static int FECHA_CREACION = 0;
	public final static int CAPITULOS = 2;
	public final static int COMENTARIO = 3;
	public final static int PUNTUACION = 4;
	
	public final static int DESC = 0;
	public final static int ASC = 1;
	public int getAtributoOrden() {
		return atributoOrden;
	}
	public void setAtributoOrden(int atributoOrden) {
		this.atributoOrden = atributoOrden;
	}
	public int getOrdenBusqueda() {
		return ordenBusqueda;
	}
	public void setOrdenBusqueda(int ordenBusqueda) {
		this.ordenBusqueda = ordenBusqueda;
	}
	
	
}
