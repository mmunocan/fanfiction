package com.ubb.fanfiction.form;

import org.hibernate.validator.constraints.Length;

public class ContraseñaForm {
	@Length(min = 8, message = "Su contraseña debe ser de al menos 8 caracteres")
	private String clave;

	private Integer idUsuario;
	
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
}
