package com.ubb.fanfiction.form;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class PuntuacionForm {
	@NotNull
	@Min(1)
	@Max(5)
	private Integer valor;
	@NotNull
	private Integer idCapitulo;
	@NotNull
	private Integer idUsuario;
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	public Integer getIdCapitulo() {
		return idCapitulo;
	}
	public void setIdCapitulo(Integer idCapitulo) {
		this.idCapitulo = idCapitulo;
	}
	
	
}
