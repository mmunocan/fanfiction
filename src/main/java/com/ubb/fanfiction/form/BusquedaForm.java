package com.ubb.fanfiction.form;


public class BusquedaForm {
	private String titulo;
	private String autor;
	private String inicioCreacion;
	private String finCreacion;
	private String inicioPublicacion;
	private String finPublicacion;
	private Integer comentarioMinimo;
	private Integer comentarioMaximo;
	private Integer capituloMinimo;
	private Integer capituloMaximo;
	private Integer puntuacionMinimo;
	private Integer puntuacionMaximo;
	private int genero;
	private int categoria;
	private int historiaOriginal;
	private int clasificacionParental;
	private String estado;
	private String tipo;
	private int atributoOrden;
	private int ordenBusqueda;
	
	public final static int FECHA_ACTUALIZACION = 0;
	public final static int FECHA_CREACION = 1;
	public final static int CAPITULOS = 2;
	public final static int COMENTARIO = 3;
	public final static int PUNTUACION = 4;
	
	public final static int DESC = 0;
	public final static int ASC = 1;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getInicioCreacion() {
		return inicioCreacion;
	}
	public void setInicioCreacion(String inicioCreacion) {
		this.inicioCreacion = inicioCreacion;
	}
	public String getFinCreacion() {
		return finCreacion;
	}
	public void setFinCreacion(String finCreacion) {
		this.finCreacion = finCreacion;
	}
	public String getInicioPublicacion() {
		return inicioPublicacion;
	}
	public void setInicioPublicacion(String inicioPublicacion) {
		this.inicioPublicacion = inicioPublicacion;
	}
	public String getFinPublicacion() {
		return finPublicacion;
	}
	public void setFinPublicacion(String finPublicacion) {
		this.finPublicacion = finPublicacion;
	}
	public Integer getComentarioMinimo() {
		return comentarioMinimo;
	}
	public void setComentarioMinimo(Integer comentarioMinimo) {
		this.comentarioMinimo = comentarioMinimo;
	}
	public Integer getComentarioMaximo() {
		return comentarioMaximo;
	}
	public void setComentarioMaximo(Integer comentarioMaximo) {
		this.comentarioMaximo = comentarioMaximo;
	}
	public Integer getCapituloMinimo() {
		return capituloMinimo;
	}
	public void setCapituloMinimo(Integer capituloMinimo) {
		this.capituloMinimo = capituloMinimo;
	}
	public Integer getCapituloMaximo() {
		return capituloMaximo;
	}
	public void setCapituloMaximo(Integer capituloMaximo) {
		this.capituloMaximo = capituloMaximo;
	}
	public Integer getPuntuacionMinimo() {
		return puntuacionMinimo;
	}
	public void setPuntuacionMinimo(Integer puntuacionMinimo) {
		this.puntuacionMinimo = puntuacionMinimo;
	}
	public Integer getPuntuacionMaximo() {
		return puntuacionMaximo;
	}
	public void setPuntuacionMaximo(Integer puntuacionMaximo) {
		this.puntuacionMaximo = puntuacionMaximo;
	}
	public int getGenero() {
		return genero;
	}
	public void setGenero(int genero) {
		this.genero = genero;
	}
	public int getCategoria() {
		return categoria;
	}
	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}
	public int getHistoriaOriginal() {
		return historiaOriginal;
	}
	public void setHistoriaOriginal(int historiaOriginal) {
		this.historiaOriginal = historiaOriginal;
	}
	public int getClasificacionParental() {
		return clasificacionParental;
	}
	public void setClasificacionParental(int clasificacionParental) {
		this.clasificacionParental = clasificacionParental;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getAtributoOrden() {
		return atributoOrden;
	}
	public void setAtributoOrden(int atributoOrden) {
		this.atributoOrden = atributoOrden;
	}
	public int getOrdenBusqueda() {
		return ordenBusqueda;
	}
	public void setOrdenBusqueda(int ordenBusqueda) {
		this.ordenBusqueda = ordenBusqueda;
	}
	
	
}
