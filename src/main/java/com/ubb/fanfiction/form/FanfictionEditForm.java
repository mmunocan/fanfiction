package com.ubb.fanfiction.form;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class FanfictionEditForm {
	
	@NotNull
	private Integer id;
	
	@NotEmpty(message = "Por favor, ingrese un título para su fanfiction")
	private String titulo;
	
	@NotEmpty(message = "Por favor, ingrese una sinopsis para su fanfiction")
	private String descripcion;
	
	private int[] historiasOriginales;
	
	private int[] generos;
	
	private String estado;
	
	@Min(1)
	private int clasificacionParental;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int[] getHistoriasOriginales() {
		return historiasOriginales;
	}

	public void setHistoriasOriginales(int[] historiasOriginales) {
		this.historiasOriginales = historiasOriginales;
	}

	public int[] getGeneros() {
		return generos;
	}

	public void setGeneros(int[] generos) {
		this.generos = generos;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getClasificacionParental() {
		return clasificacionParental;
	}

	public void setClasificacionParental(int clasificacionParental) {
		this.clasificacionParental = clasificacionParental;
	}
	
	
}
