package com.ubb.fanfiction.form;

import javax.validation.constraints.NotNull;

public class CategoriaForm {
	@NotNull
	private Integer idCategoria;

	public Integer getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}
	
}
