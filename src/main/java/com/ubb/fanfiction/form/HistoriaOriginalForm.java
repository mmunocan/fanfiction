package com.ubb.fanfiction.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class HistoriaOriginalForm {

	@NotNull
	@Min(value=1,message = "Por favor, seleccione una categoría")
	private Integer categoria;
	@NotEmpty(message = "Por favor, ingrese un título para la historia Original")
	private String nombre;
	
	public Integer getCategoria() {
		return categoria;
	}
	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
