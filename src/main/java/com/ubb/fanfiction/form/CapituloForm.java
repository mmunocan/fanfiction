package com.ubb.fanfiction.form;


import org.hibernate.validator.constraints.NotEmpty;


public class CapituloForm {
	@NotEmpty(message = "Por favor, ingrese un título para el capítulo")
	private String titulo;
	
	@NotEmpty(message = "Por favor, ingrese el contenido del capítulo")
	private String contenido;
	
	private int numero;
	private int idFanfiction;
	private int idCapitulo;
	
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getIdFanfiction() {
		return idFanfiction;
	}
	public void setIdFanfiction(int idFanfiction) {
		this.idFanfiction = idFanfiction;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public int getIdCapitulo() {
		return idCapitulo;
	}
	public void setIdCapitulo(int idCapitulo) {
		this.idCapitulo = idCapitulo;
	}
	
	
}
