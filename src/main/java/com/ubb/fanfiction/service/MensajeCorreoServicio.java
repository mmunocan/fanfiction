package com.ubb.fanfiction.service;

import com.ubb.fanfiction.exception.EmailException;

public interface MensajeCorreoServicio {

	void enviarCorreoActivacionCuenta(String contextPath, Integer id, String token, String correo) throws EmailException;

	void enviarCorreoCambioCorreoCuenta(String contextPath, Integer id, String token, String correo) throws EmailException;

	void enviarCorreoRecuperacionContraseña(String path, Integer id, String token, String correo) throws EmailException ;

}
