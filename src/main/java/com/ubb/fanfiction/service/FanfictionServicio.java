package com.ubb.fanfiction.service;

import java.util.List;
import java.util.Set;

import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.model.Publicacion;

public interface FanfictionServicio {

	Fanfiction guardarFanfiction(Fanfiction fanfiction) throws DaoException;

	Fanfiction buscarFanfictionPorId(Integer id) throws ObjectNotFoundException, DaoException;

	List<Publicacion> obtenerListaFanfictionsUsuario(Integer idUsuario, int atributoOrden, int ordenResultado) throws DaoException;

	List<Publicacion> buscarFanfiction(String titulo, String autor, String inicioCreacion, String finCreacion, String inicioPublicacion, 
			String finPublicacion, Integer comentarioMinimo, Integer comentarioMaximo, Integer capituloMinimo, Integer capituloMaximo, 
			Integer puntuacionMinima, Integer puntuacionMaxima,	Integer genero, Integer categoria, Integer historiaOriginal, 
			Integer clasificacionParental, String estado, String tipo, int atributoOrden, int ordenBusqueda) throws DaoException;

	boolean fanfictionEsDeUsuario(Integer idFanfiction, Integer idUsuario) throws DaoException;

	void actualizarFanfiction(Fanfiction fanfiction) throws DaoException;

	void eliminarFanfiction(Fanfiction fanfiction) throws DaoException;

	void eliminarListaFanfictions(Set<Fanfiction> fanfictions) throws DaoException;

	
}
