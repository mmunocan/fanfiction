package com.ubb.fanfiction.service;

import java.util.List;

import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;

public interface HistoriaOriginalServicio {

	HistoriaOriginal buscarHistoriaOriginalPorId(Integer id) throws DaoException, ObjectNotFoundException;
	
	List<HistoriaOriginal> obtenerListaHistoriasOriginales() throws DaoException;

	List<HistoriaOriginal> buscarHistoriaPorCategoria(Integer idCategoria) throws DaoException;

	void guardarHistoriaOriginal(HistoriaOriginal historiaOriginal) throws DaoException;

}
