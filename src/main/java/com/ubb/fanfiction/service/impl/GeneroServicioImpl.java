package com.ubb.fanfiction.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.GeneroDao;
import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.GeneroServicio;

@Service("generoServicio")
public class GeneroServicioImpl implements GeneroServicio{

	@Autowired
	private GeneroDao generoDao;
	
	@Override
	public Genero buscarGeneroPorId(Integer id) throws DaoException, ObjectNotFoundException {
		Genero genero;
		try{
			genero = generoDao.findOne(id);
		}catch(Exception e){
			throw new DaoException("Error al buscar el género con ID: " + id + " Mensaje: " + e.getMessage());
		}
		if(genero == null)	throw new ObjectNotFoundException("Género no encontrado. ID: " + id);
		return genero;
	}
	
	@Override
	public List<Genero> obtenerListaGeneros() throws DaoException {
		try{
			return generoDao.findAllOrderByNombre();
		}catch(Exception e){
			throw new DaoException("Error al rescatar la lista de géneros. Mensaje: " + e.getMessage());
		}
	}
}
