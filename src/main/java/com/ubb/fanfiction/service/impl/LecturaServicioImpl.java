package com.ubb.fanfiction.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.LecturaDao;
import com.ubb.fanfiction.domain.Lectura;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.service.LecturaServicio;

@Service("lecturaServicio")
public class LecturaServicioImpl implements LecturaServicio{

	@Autowired
	private LecturaDao lecturaDao;
	
	@Override
	public void actualizarLectura(Lectura lectura) throws DaoException {
		Lectura lecturaAnterior;
		try{
			lecturaAnterior = lecturaDao.findByUsuarioAndFanfiction(lectura.getUsuario(), lectura.getCapitulo().getFanfiction().getIdFanfiction());
		}catch(Exception e){
			throw new DaoException("Error al actualizar la lectura: " + lectura.getIdLectura() + " Mensaje: " + e.getMessage());
		}
		if(lecturaAnterior == null){
			lectura.setFecha(Calendar.getInstance().getTime());
			lecturaDao.save(lectura);
		}else{
			lecturaAnterior.setFecha(Calendar.getInstance().getTime());
			lecturaAnterior.setCapitulo(lectura.getCapitulo());
			lecturaDao.save(lecturaAnterior);
		}
		
	}

	@Override
	public void eliminarLectura(Lectura lectura) throws DaoException {
		try{
			lecturaDao.delete(lectura);
		}catch(Exception e){
			throw new DaoException("Error al eliminar la lectura. " + " Mensaje: " + e.getMessage());
		}
		
		
	}

	@Override
	public void eliminarListaLecturas(Set<Lectura> lecturas) throws DaoException {
		try{
			lecturaDao.delete(lecturas);
		}catch(Exception e){
			throw new DaoException("Error al eliminar la lista de lecturas. " + " Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<Lectura> obtenerHistorialLectura(Integer idUsuario) throws DaoException {
		try{
			return lecturaDao.findByUsuarioOrderByFecha(idUsuario);
		}catch(Exception e){
			throw new DaoException("Error al obtener historial de lectura del usuario con ID: " + idUsuario + " Mensaje: " + e.getMessage());
		}
	}

}
