package com.ubb.fanfiction.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.PuntuacionDao;
import com.ubb.fanfiction.domain.Puntuacion;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.PuntuacionServicio;

@Service("puntuacionServicio")
public class PuntuacionServicioImpl implements PuntuacionServicio {

	@Autowired
	private PuntuacionDao puntuacionDao;
	
	@Override
	public Double obtenerPromedioPuntuacionFanfiction(Integer idfanfiction) throws DaoException {
		Double valor;
		try{
			valor = puntuacionDao.findAverageValorByFanfiction(idfanfiction);
		}catch(Exception e){
			throw new DaoException("Error al rescatar promedio de puntuaciones del fanfiction: " + idfanfiction + " Mensaje: " + e.getMessage());
		}
		if(valor != null) return valor;
		else return 0.0;
	}

	@Override
	public Double obtenerPromedioPuntuacionCapitulo(Integer idCapitulo) throws DaoException {
		Double valor;
		try{
			valor = puntuacionDao.findAverageValor(idCapitulo);
		}catch(Exception e){
			throw new DaoException("Error al rescatar promedio de puntuaciones del capitulo: " + idCapitulo + " Mensaje: " + e.getMessage());
		}
		if(valor != null) return valor;
		else return 0.0;
	}

	@Override
	public Puntuacion obtenerPuntuacion(Integer idCapitulo, Integer idUsuario) throws ObjectNotFoundException, DaoException {
		Puntuacion puntuacion;
		try{
			puntuacion = puntuacionDao.findByCapituloAndUsuario(idCapitulo, idUsuario);
		}catch(Exception e){
			throw new DaoException("Error al buscar Puntuación. Capítulo: " + idCapitulo + ", Usuario: " + idUsuario + " Mensaje: " + e.getMessage());
		}
		if(puntuacion == null) throw new ObjectNotFoundException("Puntuación no encontrada. Capitulo: " + idCapitulo + ", Usuario: " + idUsuario);
		return puntuacion;
	}

	@Override
	public void guardarPuntuacion(Puntuacion puntuacion) throws DaoException {
		puntuacion.setFecha(Calendar.getInstance().getTime());
		try{
			puntuacionDao.save(puntuacion);
		}catch(Exception e){
			throw new DaoException("Error al guardar la puntuacion." + " Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<Puntuacion> obtenerPuntuacionesRecibidas(Integer idUsuario) throws DaoException {
		List<Puntuacion> resultado;
		try{
			resultado = puntuacionDao.findByUsuarioFanfictionOrderByFechaDesc(idUsuario);
		}catch(Exception e){
			throw new DaoException("Error rescatar el listado de puntuaciones recibidas para el usuario con id: " + idUsuario + " Mensaje: " + e.getMessage());
		}
		return resultado;
	}

	@Override
	public List<Puntuacion> obtenerPuntuacionesRealizadas(Integer idUsuario) throws DaoException {
		List<Puntuacion> resultado;
		try{
			resultado = puntuacionDao.findByUsuarioOrderByFechaDesc(idUsuario);
		}catch(Exception e){
			throw new DaoException("Error rescatar el listado de puntuaciones realizadas para el usuario con id: " + idUsuario + " Mensaje: " + e.getMessage());
		}
		return resultado;
	}

}
