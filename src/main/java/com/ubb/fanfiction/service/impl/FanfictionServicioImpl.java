package com.ubb.fanfiction.service.impl;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.ComentarioDao;
import com.ubb.fanfiction.dao.FanfictionDao;
import com.ubb.fanfiction.dao.PuntuacionDao;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.BusquedaForm;
import com.ubb.fanfiction.form.OrdenarFanfictionForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.model.PublicacionComparator;
import com.ubb.fanfiction.service.FanfictionServicio;


@Service("fanfictionServicio")
public class FanfictionServicioImpl implements FanfictionServicio{

	@Autowired
	private FanfictionDao fanfictionDao;
	
	@Autowired
	private ComentarioDao comentarioDao;
	
	@Autowired
	private PuntuacionDao puntuacionDao;
	
	@Override
	public Fanfiction guardarFanfiction(Fanfiction fanfiction) throws DaoException {
		fanfiction.setFechaCreacion(Calendar.getInstance().getTime());
		fanfiction.setFechaActualizacion(Calendar.getInstance().getTime());
		fanfiction.setDescripcion(("<p>" + fanfiction.getDescripcion() + "</p>").replaceAll("\n", "</p><p>"));
		try{
			return fanfictionDao.save(fanfiction);
		}catch(Exception e){
			throw new DaoException("Error al guardar fanfiction." + " Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public Fanfiction buscarFanfictionPorId(Integer id) throws ObjectNotFoundException, DaoException {
		Fanfiction fanfiction;
		try{
			fanfiction =  fanfictionDao.findOne(id);
		}catch(Exception e){
			throw new DaoException("Error al buscar el fanfiction: " + id + " Mensaje: " + e.getMessage());
		}
		if(fanfiction == null) throw new ObjectNotFoundException("Fanfiction no encontrado. ID: " + id);
		else return fanfiction;
		
	}

	@Override
	public List<Publicacion> obtenerListaFanfictionsUsuario(Integer idUsuario, int atributoOrden, int ordenResultado) throws DaoException {
		List<Fanfiction> fics = null;
		List<Publicacion> resultado = null;
		try{
			if(atributoOrden == OrdenarFanfictionForm.FECHA_ACTUALIZACION && ordenResultado == OrdenarFanfictionForm.DESC)
				fics = fanfictionDao.findByUsuarioOrderByFechaActualizacionDesc(idUsuario);
			else if(atributoOrden == OrdenarFanfictionForm.FECHA_ACTUALIZACION && ordenResultado == OrdenarFanfictionForm.ASC)
				fics = fanfictionDao.findByUsuarioOrderByFechaActualizacionAsc(idUsuario);
			else if(atributoOrden == OrdenarFanfictionForm.FECHA_CREACION && ordenResultado == OrdenarFanfictionForm.DESC)
				fics = fanfictionDao.findByUsuarioOrderByFechaCreacionDesc(idUsuario);
			else if(atributoOrden == OrdenarFanfictionForm.FECHA_CREACION && ordenResultado == OrdenarFanfictionForm.ASC)
				fics = fanfictionDao.findByUsuarioOrderByFechaCreacionAsc(idUsuario);
			else
				fics = fanfictionDao.findByIdUsuario(idUsuario);
			resultado = new ArrayList<Publicacion>();
			Publicacion fanfictionCompleto;
			for(Fanfiction fanfiction : fics){
				fanfictionCompleto = new Publicacion();
				fanfictionCompleto.setFanfiction(fanfiction);
				fanfictionCompleto.setCantidadComentarios(comentarioDao.countByFanfiction(fanfiction.getIdFanfiction()));
				fanfictionCompleto.setPromedioPuntuacion(puntuacionDao.findAverageValorByFanfiction(fanfiction.getIdFanfiction()));
				resultado.add(fanfictionCompleto);
			}
			
			if(atributoOrden != OrdenarFanfictionForm.FECHA_ACTUALIZACION && atributoOrden != OrdenarFanfictionForm.FECHA_CREACION)
				Collections.sort(resultado, new PublicacionComparator(atributoOrden,ordenResultado));
		}catch(Exception e){
			throw new DaoException("Error al buscar los fanfictions del usuario: " + idUsuario + " Mensaje: " + e.getMessage());
		}
		return resultado;
	}
	
	@Override
	public List<Publicacion> buscarFanfiction(String titulo, String autor, String inicioCreacion, String finCreacion,
			String inicioPublicacion, String finPublicacion, Integer comentarioMinimo, Integer comentarioMaximo,
			Integer capituloMinimo, Integer capituloMaximo, Integer puntuacionMinima, Integer puntuacionMaxima,
			Integer genero, Integer categoria, Integer historiaOriginal, Integer clasificacionParental, String estado,
			String tipo, int atributoOrden, int ordenBusqueda) throws DaoException {
		List<Fanfiction> fanfictions = null;
		List<Publicacion> resultado;
		
		try{
			String atributo = null;
			String orden = null;
			if(atributoOrden == BusquedaForm.FECHA_ACTUALIZACION){
				atributo = "fechaActualizacion";				
			}
			if(atributoOrden == BusquedaForm.FECHA_CREACION){
				atributo = "fechaCreacion";			
			}
			
			if(atributoOrden == BusquedaForm.FECHA_ACTUALIZACION || atributoOrden == BusquedaForm.FECHA_CREACION){
				if(ordenBusqueda == BusquedaForm.ASC){
					orden = "asc";
				}else if(ordenBusqueda == BusquedaForm.DESC){
					orden = "desc";
				}
			}
			
			fanfictions = fanfictionDao.buscar(titulo, autor, inicioCreacion, finCreacion, 
					inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
					puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, atributo, orden);
			
			resultado = new ArrayList<Publicacion>();
			
			Publicacion fanfictionCompleto;
			for(Fanfiction fanfiction : fanfictions){
				fanfictionCompleto = new Publicacion();
				fanfictionCompleto.setFanfiction(fanfiction);
				fanfictionCompleto.setCantidadComentarios(comentarioDao.countByFanfiction(fanfiction.getIdFanfiction()));
				fanfictionCompleto.setPromedioPuntuacion(puntuacionDao.findAverageValorByFanfiction(fanfiction.getIdFanfiction()));
				resultado.add(fanfictionCompleto);
			}
			if(atributoOrden != BusquedaForm.FECHA_ACTUALIZACION && atributoOrden != BusquedaForm.FECHA_CREACION)
				Collections.sort(resultado, new PublicacionComparator(atributoOrden,ordenBusqueda));
		}catch(Exception e){
			throw new DaoException("Error al ejecutar la búsqueda. " + " Mensaje: " + e.getMessage());
		}
		return resultado;
		
	}

	@Override
	public boolean fanfictionEsDeUsuario(Integer idFanfiction, Integer idUsuario) throws DaoException {
		Fanfiction fanfiction;
		try{
			fanfiction = fanfictionDao.findByIdAndUsuario(idFanfiction, idUsuario);
		}catch(Exception e){
			throw new DaoException("Error al consultar por el fanfiction de ID: " + idFanfiction  + " y Usuario: " + idUsuario + " Mensaje: " + e.getMessage());
		}
		return fanfiction != null;
	}

	@Override
	public void actualizarFanfiction(Fanfiction fanfiction) throws DaoException {
		fanfiction.setFechaActualizacion(Calendar.getInstance().getTime());
		fanfiction.setDescripcion(("<p>" + fanfiction.getDescripcion() + "</p>").replaceAll("\n", "</p><p>"));
		try{
			fanfictionDao.save(fanfiction);
		}catch(Exception e){
			throw new DaoException("Error al guardar fanfiction." + " Mensaje: " + e.getMessage());
		}
	}

	@Override
	public void eliminarFanfiction(Fanfiction fanfiction) throws DaoException {
		try{
			fanfictionDao.delete(fanfiction);
		}catch(Exception e){
			throw new DaoException("Error al eliminar el fanfiction." + " Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public void eliminarListaFanfictions(Set<Fanfiction> fanfictions) throws DaoException {
		try{
			fanfictionDao.delete(fanfictions);
		}catch(Exception e){
			throw new DaoException("Error al eliminar la lista de fanfictions." + " Mensaje: " + e.getMessage());
		}
	}


}
