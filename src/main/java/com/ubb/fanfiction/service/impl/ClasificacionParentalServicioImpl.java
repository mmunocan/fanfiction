package com.ubb.fanfiction.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.ClasificacionParentalDao;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.ClasificacionParentalServicio;

@Service("clasificacionParentalServicio")
public class ClasificacionParentalServicioImpl implements  ClasificacionParentalServicio{
	
	@Autowired
	private ClasificacionParentalDao clasificacionParentalDao;
	
	@Override
	public ClasificacionParental buscarClasificacionParentalPorId(Integer id) throws ObjectNotFoundException, DaoException {
		ClasificacionParental clasificacionParental;
		try{
			clasificacionParental = clasificacionParentalDao.findOne(id);
		}catch(Exception e){
			throw new DaoException("Error al buscar la clasificacion parental con ID: " + id + " Mensaje: " + e.getMessage());
		}
		if(clasificacionParental == null) throw new ObjectNotFoundException("Clasificacion parental no encontrada. ID: " + id);
		else return clasificacionParental;
	}
	
	@Override
	public List<ClasificacionParental> obtenerClasificacionesParentales() throws DaoException {
		try{
			return clasificacionParentalDao.findAll();
		}catch(Exception e){
			throw new DaoException("Error al rescatar la lista de clasificaciones parentales" + " Mensaje: " + e.getMessage());
		}
	}


}
