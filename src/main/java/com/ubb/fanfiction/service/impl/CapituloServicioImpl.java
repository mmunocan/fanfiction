package com.ubb.fanfiction.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.CapituloDao;
import com.ubb.fanfiction.dao.ComentarioDao;
import com.ubb.fanfiction.dao.FanfictionDao;
import com.ubb.fanfiction.dao.PuntuacionDao;
import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.model.CapituloPublicacion;
import com.ubb.fanfiction.service.CapituloServicio;

@Service("capituloServicio")
public class CapituloServicioImpl implements CapituloServicio{
	
	@Autowired
	private CapituloDao capituloDao;
	
	@Autowired
	private FanfictionDao fanfictionDao;
	
	@Autowired
	private ComentarioDao comentarioDao;
	
	@Autowired
	private PuntuacionDao puntuacionDao;
		
	@Override
	public void guardarCapitulo(Capitulo capitulo) throws DaoException {
		capitulo.setFechaPublicacion(Calendar.getInstance().getTime());
		capitulo.setFechaActualizacion(Calendar.getInstance().getTime());
		capitulo.setContenido(("<p>" + capitulo.getContenido() + "</p>").replaceAll("\n", "</p><p>"));
		try{
			capituloDao.save(capitulo);
		}catch(Exception e){
			if(capitulo.getNumero() == 1)
				fanfictionDao.delete(capitulo.getFanfiction());
			throw new DaoException("Error al guardar el capítulo." + " Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public Capitulo obtenerCapitulo(Integer idFanfiction, int numeroCapitulo) throws DaoException, ObjectNotFoundException{
		Capitulo capitulo;
		try{
			capitulo = capituloDao.findByFanfictionAndNumero(idFanfiction, numeroCapitulo);
		}catch(Exception e){
			throw new DaoException("Error al buscar capítulo. Fanfiction: " + idFanfiction + ", Capitulo: " + numeroCapitulo + " Mensaje: " + e.getMessage());
		}
		if(capitulo == null) throw new ObjectNotFoundException("Capítulo no encontrado. Fanfiction: " + idFanfiction + ", Capitulo: " + numeroCapitulo);
		else return capitulo;
	}

	@Override
	public List<CapituloPublicacion> obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(Integer idFanfiction) throws DaoException {
		Fanfiction fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		List<CapituloPublicacion> resultado = new ArrayList<CapituloPublicacion>();
		CapituloPublicacion capituloPublicacion;
		try{
			List<Capitulo> listaCapitulos = capituloDao.findByFanfictionOrderByNumeroAsc(idFanfiction);
			for(Capitulo capitulo : listaCapitulos){
				capituloPublicacion = new CapituloPublicacion();
				capituloPublicacion.setCapitulo(capitulo);
				capituloPublicacion.setPromedioPuntuacion(puntuacionDao.findAverageValor(capitulo.getIdCapitulo()));
				capituloPublicacion.setCantidadComentarios(comentarioDao.countByCapitulo(capitulo.getIdCapitulo()));
				resultado.add(capituloPublicacion);
			}
		}catch(Exception e){
			throw new DaoException("Error al buscar los capitulos del fanfiction: " + idFanfiction + " Mensaje: " + e.getMessage());
		}
		return resultado;
	}

	@Override
	public Capitulo obtenerCapitulo(Integer idCapitulo) throws ObjectNotFoundException, DaoException {
		Capitulo capitulo;
		try{
			capitulo = capituloDao.findOne(idCapitulo);
		}catch(Exception e){
			throw new DaoException("Error al buscar capítulo. ID: " + idCapitulo + " Mensaje: " + e.getMessage());
		}
		if(capitulo == null) throw new ObjectNotFoundException("Capítulo no encontrado. ID: " + idCapitulo);
		return capitulo;
	}

	@Override
	public boolean capituloEsDeUsuario(Integer idCapitulo, Integer idUsuario) throws DaoException {
		try{
			return capituloDao.findByIdAndUsuario(idCapitulo, idUsuario) != null;
		}catch(Exception e){
			throw new DaoException("Error al consultar por el capitulo de ID: " + idCapitulo  + " y Usuario: " + idUsuario + " Mensaje: " + e.getMessage());
		}
	}

	@Override
	public void editarCapitulo(Capitulo capitulo) throws DaoException {
		capitulo.setFechaActualizacion(Calendar.getInstance().getTime());
		capitulo.setContenido(("<p>" + capitulo.getContenido() + "</p>").replaceAll("\n", "</p><p>"));
		try{
			capituloDao.save(capitulo);
		}catch(Exception e){
			throw new DaoException("Error al editar el capítulo." + " Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public void eliminarCapitulo(Capitulo capitulo) throws DaoException{
		try{
			capituloDao.borrar(capitulo.getIdCapitulo());
			
			List<Capitulo> capitulos = capituloDao.findByFanfictionOrderByNumeroAsc(capitulo.getFanfiction().getIdFanfiction());
			for(Capitulo c : capitulos){
				if(c.getNumero() > capitulo.getNumero()){
					c.setNumero(c.getNumero()-1);
					capituloDao.save(c);
				}
			}
		}catch(Exception e){
			throw new DaoException("Error al eliminar el capítulo." + " Mensaje: " + e.getMessage());
		}
		
	}

}
