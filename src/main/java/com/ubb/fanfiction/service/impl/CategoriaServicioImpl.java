package com.ubb.fanfiction.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.CategoriaDao;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.CategoriaServicio;

@Service("categoriaServicio")
public class CategoriaServicioImpl implements CategoriaServicio{

	@Autowired
	private CategoriaDao categoriaDao;
	
	@Override
	public List<Categoria> obtenerListaCategorias() throws DaoException {
		try{
			return categoriaDao.findAll();
		}catch(Exception e){
			throw new DaoException("Error al rescatar la lista de categorias. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public Categoria obtenerCategoriaPorId(Integer idCategoria) throws ObjectNotFoundException, DaoException {
		Categoria categoria;
		try{
			categoria = categoriaDao.findOne(idCategoria);
		}catch(Exception e){
			throw new DaoException("Error al rescatar la categoria con ID: "+idCategoria+". Mensaje: " + e.getMessage());
		}
		if(categoria != null)	return categoria;
		else throw new ObjectNotFoundException("Categoria con ID "+idCategoria+" no encontrada.");
	}

}
