package com.ubb.fanfiction.service.impl;

import java.util.Calendar;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.UsuarioDao;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.UsuarioServicio;


@Service("usuarioServicio")
public class UsuarioServicioImpl implements UsuarioServicio {
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	@Override
	public Usuario buscarUsuarioPorCorreo(String correo) throws DaoException{
		Usuario usuario;
		try{
			usuario = usuarioDao.findByCorreo(correo);
		}catch(Exception e){
			throw new DaoException("Error al buscar usuario por correo" + correo + " Mensaje: " + e.getMessage());
		}
		return usuario;
		
	}

	@Override
	public Usuario buscarUsuarioPorNombre(String nombre) throws DaoException {
		Usuario usuario;
		try{
			usuario = usuarioDao.findByNombre(nombre);
		}catch(Exception e){
			throw new DaoException("Error al buscar al usuario con nombre " + " Mensaje: " + e.getMessage());
		}
		return usuario;
	}

	@Override
	public Integer guardarUsuario(Usuario usuario) throws DaoException {
		usuario.setRol("USER");
		String clave = usuario.getClave();
		usuario.setClave(bCryptPasswordEncoder.encode(clave));
		usuario.setEstado(true);
		usuario.setFechaCreacion(Calendar.getInstance().getTime());
		usuario.setDescripcion(("<p>" + usuario.getDescripcion() + "</p>").replaceAll("\n", "</p><p>"));
		usuario.setCorreoActivo(false);
		try{
			return usuarioDao.save(usuario).getIdUsuario();
		}catch(Exception e){
			throw new DaoException("Error al guardar al usuario." + " Mensaje: " + e.getMessage());
		}
	}

	@Override
	public Usuario buscarUsuarioPorId(Integer idUsuario) throws DaoException, ObjectNotFoundException {
		Usuario usuario;
		try{
			usuario = usuarioDao.findOne(idUsuario);
		}catch(Exception e){
			throw new DaoException("Error al buscar al usuario con id " + idUsuario + " Mensaje: " + e.getMessage());
		}
		if(usuario == null)	throw new ObjectNotFoundException("Usuario no encontrado. ID: " + idUsuario);
		return usuario;
	}

	@Override
	public void desactivarUsuario(Usuario usuario) throws DaoException {
		usuario.setEstado(false);
		try{
			usuarioDao.save(usuario);
		}catch(Exception e){
			throw new DaoException("Error al desactivar la cuenta de usuario " + " Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public void eliminarUsuario(Usuario usuario) throws DaoException {
		try{
			usuarioDao.delete(usuario);
		}catch(Exception e){
			throw new DaoException("Error al eliminar al usuario " + " Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public void editarUsuario(Usuario usuario) throws DaoException {
		usuario.setClave(bCryptPasswordEncoder.encode(usuario.getClave()));
		usuario.setDescripcion(("<p>" + usuario.getDescripcion() + "</p>").replaceAll("\n", "</p><p>"));
		try{
			usuarioDao.save(usuario);
		}catch(Exception e){
			throw new DaoException("Error al editar al usuario.  Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public Usuario buscarOtroUsuarioPorNombre(Integer idUsuario, String nombre) throws DaoException {
		try{
			return usuarioDao.findByNotIdUsuarioAndNombre(idUsuario, nombre);
		}catch(Exception e){
			throw new DaoException("Error al al encontrar al usuario con nombre "+nombre+" y sin ID "+idUsuario+" .  Mensaje: " + e.getMessage());
		}
	}

	@Override
	public Usuario buscarOtroUsuarioPorCorreo(Integer idUsuario, String correo) throws DaoException {
		try{
			return usuarioDao.findByNotIdUsuarioAndCorreo(idUsuario, correo);
		}catch(Exception e){
			throw new DaoException("Error al al encontrar al usuario con correo "+correo+" y sin ID "+idUsuario+" .  Mensaje: " + e.getMessage());
		}
	}

	@Override
	public boolean validarToken(String token, int idUsuario) throws DaoException {
		try{
			return usuarioDao.findByIdUsuarioAndToken(idUsuario, token) != null;
		}catch(Exception e){
			throw new DaoException("Error al al validar al usuario con id "+idUsuario+" .  Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public void validarCuenta(Usuario usuario) throws DaoException {
		if(!usuario.isCorreoActivo() && usuario.getNuevoCorreo() == null){
			usuario.setCorreoActivo(true);
		}else{
			usuario.setCorreo(usuario.getNuevoCorreo());
			usuario.setNuevoCorreo(null);
		}
		usuario.setToken(null);
		try{
			usuarioDao.save(usuario);
		}catch(Exception e){
			throw new DaoException("Error al validar cuenta de usuario con id "+usuario.getIdUsuario()+" .  Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public String generarToken() {
		return UUID.randomUUID().toString();
	}

	@Override
	public void guardarUsuarioDirecto(Usuario usuario) throws DaoException {
		try{
			usuarioDao.save(usuario);
		}catch(Exception e){
			throw new DaoException("Error al guarda datos del usuario usuario con id "+usuario.getIdUsuario()+" .  Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public void restablecerContraseña(Usuario usuario, String clave) throws DaoException {
		usuario.setClave(bCryptPasswordEncoder.encode(clave));
		usuario.setToken(null);
		try{
			usuarioDao.save(usuario);
		}catch(Exception e){
			throw new DaoException("Error al restablecer cuenta de usuario con id "+usuario.getIdUsuario()+" .  Mensaje: " + e.getMessage());
		}
		
	}


}
