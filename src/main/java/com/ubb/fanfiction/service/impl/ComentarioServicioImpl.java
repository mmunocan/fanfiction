package com.ubb.fanfiction.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.ComentarioDao;
import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.ComentarioServicio;

@Service("comentarioServicio")
public class ComentarioServicioImpl implements ComentarioServicio {
	
	@Autowired
	private ComentarioDao comentarioDao;
	
	@Override
	public Long contarComentariosPorFanfiction(Integer idFanfiction) throws DaoException {
		Long total;
		try{
			total = comentarioDao.countByFanfiction(idFanfiction);
		}catch(Exception e){
			throw new DaoException("Error al contar comentarios del fanfiction: " + idFanfiction + " Mensaje: " + e.getMessage());
		}
		return total;
	}



	@Override
	public Long contarComentariosPorCapitulo(Integer idCapitulo) throws DaoException {
		Long total;
		try{
			total = comentarioDao.countByCapitulo(idCapitulo);
		}catch(Exception e){
			throw new DaoException("Error al contar comentarios del capítulo: " + idCapitulo + " Mensaje: " + e.getMessage());
		}
		return total;

	}



	@Override
	public List<Comentario> obtenerListaComentariosPorCapituloOrdenadoPorFecha(Integer idCapitulo) throws DaoException {
		List<Comentario> resultado;
		try{
			resultado = comentarioDao.findByCapituloOrderByFechaDesc(idCapitulo);
		}catch(Exception e){
			throw new DaoException("Error al rescatar los comentarios del capítulo: " + idCapitulo + " Mensaje: " + e.getMessage());
		}
		return resultado;
	}



	@Override
	public Comentario guardarComentario(Comentario comentario) throws DaoException {
		Comentario resultante;
		comentario.setFecha(Calendar.getInstance().getTime());
		comentario.setContenido(("<p>" + comentario.getContenido() + "</p>").replaceAll("\n", "</p><p>"));
		try{
			resultante = comentarioDao.save(comentario);
		}catch(Exception e){
			throw new DaoException("Error al guardar el comentario." + " Mensaje: " + e.getMessage());
		}
		return resultante; 
		
	}



	@Override
	public List<Comentario> obtenerComentariosRecibidos(Integer idUsuario) throws DaoException {
		List<Comentario> resultado;
		try{
			resultado = comentarioDao.findByUsuarioFanfictionOrderByFechaDesc(idUsuario);
		}catch(Exception e){
			throw new DaoException("Error al obtener el listado de comentarios recibidos para el usuario con ID: " + idUsuario + " Mensaje: " + e.getMessage());
		}
		return resultado;
	}



	@Override
	public Comentario buscarComentarioPorId(Integer idComentario) throws ObjectNotFoundException, DaoException{
		Comentario comentario;
		try{
			comentario =  comentarioDao.findOne(idComentario);
		}catch(Exception e){
			throw new DaoException("Error al buscar el comentario con ID: ." + idComentario + " Mensaje: " + e.getMessage());
		}
		if(comentario == null)	throw new ObjectNotFoundException("Comentario no encontrado. ID: " + idComentario);
		else return comentario;
	}



	@Override
	public void eliminarComentario(Comentario comentario) throws DaoException {
		try{
			comentarioDao.delete(comentario);
		}catch(Exception e){
			throw new DaoException("Error al eliminar el comentario." + " Mensaje: " + e.getMessage());
		}
	}



	@Override
	public List<Comentario> obtenerComentariosRealizados(Integer idUsuario) throws DaoException {
		List<Comentario> resultado;
		try{
			resultado = comentarioDao.findByUsuarioOrderByFechaDesc(idUsuario);
		}catch(Exception e){
			throw new DaoException("Error al obtener el listado de comentarios realizados para el usuario con ID: " + idUsuario + " Mensaje: " + e.getMessage());
		}
		return resultado;
	}

}
