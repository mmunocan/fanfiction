package com.ubb.fanfiction.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.exception.EmailException;
import com.ubb.fanfiction.service.MensajeCorreoServicio;

@Service("mensajeCorreoServicio")
public class MensajeCorreoServicioImpl implements MensajeCorreoServicio {

	@Autowired
	private JavaMailSender emailSender;
	 
	@Override
	public void enviarCorreoActivacionCuenta(String contextPath, Integer id, String token, String correo) throws EmailException {
		
		SimpleMailMessage message = new SimpleMailMessage(); 
		
		String subject = "Activación de su nueva cuenta en Zona De Fics";
		String url = contextPath+"/validar/"+token+"/"+id;
		String mensaje = "Estimado usuario: \r\n Aquí se encuentra el enlace de activación de la cuenta:\r\n \r\n"+url+" \r\nAtentamente, el equipo de Zona de Fics.";

		message.setFrom("contacto.zdf@gmail.com");
		message.setTo(correo);
		message.setSubject(subject);
		message.setText(mensaje);
		
		enviarCorreo(message);
		
		
	}

	@Override
	public void enviarCorreoCambioCorreoCuenta(String contextPath, Integer id, String token, String correo) throws EmailException {

		SimpleMailMessage message = new SimpleMailMessage(); 
		
		String subject = "Cambio de correo de su cuenta de Zona de Fics";
		String url = contextPath+"/validar/"+token+"/"+id;
		String mensaje = "Estimado usuario: \r\n Aquí se encuentra el enlace de activación del correo: \r\n\r\n"+url+" \r\nAtentamente, el equipo de Zona de Fics.";		

		message.setFrom("contacto.zdf@gmail.com");
		message.setTo(correo);
		message.setSubject(subject);
		message.setText(mensaje);

		enviarCorreo(message);
	}

	@Override
	public void enviarCorreoRecuperacionContraseña(String path, Integer id, String token, String correo)  throws EmailException {

		SimpleMailMessage message = new SimpleMailMessage(); 
		
		String subject = "Restablecer contraseña de su cuenta Zona de Fics";
		String url = path+"/recuperar/"+id+"/"+token;
		String mensaje = "Estimado usuario: \r\n Aquí se encuentra el enlace de recuperación de su contraseña: \r\n\r\n"+url+" \r\nAtentamente, el equipo de Zona de Fics.";		

		message.setFrom("contacto.zdf@gmail.com");
		message.setTo(correo);
		message.setSubject(subject);
		message.setText(mensaje);

		enviarCorreo(message);
	}
	
	private void enviarCorreo(SimpleMailMessage message) throws EmailException{
		byte intentos = 0;
		boolean enviado = false;
		while(!enviado){
			try{
				emailSender.send(message);
				enviado = true;
			}catch(Exception e){
				intentos++;
				if(intentos == 3){
					throw new EmailException("No se pudo enviar el correo. Causa: " + e.getMessage());
				}
			}
		}
	}

}
