package com.ubb.fanfiction.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ubb.fanfiction.dao.HistoriaOriginalDao;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.HistoriaOriginalServicio;

@Service("historiaOriginalServicio")
public class HistoriaOriginalServicioImpl implements HistoriaOriginalServicio{

	@Autowired
	private HistoriaOriginalDao historiaOriginalDao;
	
	@Override
	public HistoriaOriginal buscarHistoriaOriginalPorId(Integer id) throws DaoException, ObjectNotFoundException {
		HistoriaOriginal historiaOriginal;
		try{
			historiaOriginal = historiaOriginalDao.findOne(id);
		}catch(Exception e){
			throw new DaoException("Error al buscar la Historia original con ID: " + id + " Mensaje: " + e.getMessage());
		}
		if(historiaOriginal == null) throw new ObjectNotFoundException("Historia original no encontrada. ID: " + id);
		else return historiaOriginal;
	}

	@Override
	public List<HistoriaOriginal> obtenerListaHistoriasOriginales() throws DaoException {
		try{
			return  historiaOriginalDao.findAllOrderByNombre();
		}catch(Exception e){
			throw new DaoException("Error al rescatar lista de Historias Originales." + " Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<HistoriaOriginal> buscarHistoriaPorCategoria(Integer idCategoria) throws DaoException {
		try{
			return  historiaOriginalDao.findByCategoriaOrderByNombre(idCategoria);
		}catch(Exception e){
			throw new DaoException("Error al rescatar lista de Historias Originales de la Categoria. ID: " + idCategoria + " Mensaje: " + e.getMessage());
		}
		
	}

	@Override
	public void guardarHistoriaOriginal(HistoriaOriginal historiaOriginal) throws DaoException {
		try{
			historiaOriginalDao.save(historiaOriginal);
		}catch(Exception e){
			throw new DaoException("Error al guardar la Historia Original. Mensaje: " + e.getMessage());
		}
	}

}
