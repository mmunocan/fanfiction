package com.ubb.fanfiction.service;

import java.util.List;

import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;

public interface ClasificacionParentalServicio {

	ClasificacionParental buscarClasificacionParentalPorId(Integer id) throws ObjectNotFoundException, DaoException;
	
	List<ClasificacionParental> obtenerClasificacionesParentales() throws DaoException;

}
