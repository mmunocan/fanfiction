package com.ubb.fanfiction.service;

import java.util.List;

import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;

public interface ComentarioServicio {

	Long contarComentariosPorFanfiction(Integer idFanfiction) throws DaoException;

	Long contarComentariosPorCapitulo(Integer idCapitulo) throws DaoException;

	List<Comentario> obtenerListaComentariosPorCapituloOrdenadoPorFecha(Integer idCapitulo) throws DaoException;

	Comentario guardarComentario(Comentario comentario) throws DaoException;

	List<Comentario> obtenerComentariosRecibidos(Integer idUsuario) throws DaoException;

	Comentario buscarComentarioPorId(Integer idComentario) throws ObjectNotFoundException, DaoException;

	void eliminarComentario(Comentario comentario) throws DaoException;

	List<Comentario> obtenerComentariosRealizados(Integer idUsuario) throws DaoException;


}
