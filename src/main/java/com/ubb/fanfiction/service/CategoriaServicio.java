package com.ubb.fanfiction.service;

import java.util.List;

import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;

public interface CategoriaServicio {

	List<Categoria> obtenerListaCategorias() throws DaoException;

	Categoria obtenerCategoriaPorId(Integer idCategoria) throws ObjectNotFoundException, DaoException;

}
