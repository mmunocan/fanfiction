package com.ubb.fanfiction.service;

import java.util.List;

import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;

public interface GeneroServicio {

	Genero buscarGeneroPorId(Integer id) throws DaoException, ObjectNotFoundException;
	
	List<Genero> obtenerListaGeneros() throws DaoException;

}
