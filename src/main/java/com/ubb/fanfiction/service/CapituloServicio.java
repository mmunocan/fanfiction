
package com.ubb.fanfiction.service;

import java.util.List;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.model.CapituloPublicacion;

public interface CapituloServicio {

	void guardarCapitulo(Capitulo capituloUno) throws DaoException;

	Capitulo obtenerCapitulo(Integer idFanfiction, int numeroCapitulo) throws DaoException, ObjectNotFoundException;

	List<CapituloPublicacion> obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(Integer idFanfiction) throws DaoException;

	Capitulo obtenerCapitulo(Integer idCapitulo) throws ObjectNotFoundException, DaoException;

	boolean capituloEsDeUsuario(Integer idCapitulo, Integer idUsuario) throws DaoException;

	void editarCapitulo(Capitulo capitulo) throws DaoException;

	void eliminarCapitulo(Capitulo capitulo) throws DaoException;

}
