package com.ubb.fanfiction.service;


import java.util.List;

import com.ubb.fanfiction.domain.Puntuacion;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;

public interface PuntuacionServicio {

	Double obtenerPromedioPuntuacionFanfiction(Integer idfanfiction) throws DaoException;

	Double obtenerPromedioPuntuacionCapitulo(Integer idCapitulo) throws DaoException;

	Puntuacion obtenerPuntuacion(Integer idCapitulo, Integer idUsuario) throws ObjectNotFoundException, DaoException;

	void guardarPuntuacion(Puntuacion puntuacion) throws DaoException;

	List<Puntuacion> obtenerPuntuacionesRecibidas(Integer idUsuario) throws DaoException;

	List<Puntuacion> obtenerPuntuacionesRealizadas(Integer idUsuario) throws DaoException;


}
