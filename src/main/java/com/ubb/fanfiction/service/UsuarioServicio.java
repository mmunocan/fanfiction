package com.ubb.fanfiction.service;


import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;

public interface UsuarioServicio {

	Usuario buscarUsuarioPorCorreo(String correo) throws DaoException;

	Usuario buscarUsuarioPorNombre(String nombre) throws DaoException;

	Integer guardarUsuario(Usuario usuario) throws DaoException;

	Usuario buscarUsuarioPorId(Integer idUsuario) throws ObjectNotFoundException, DaoException;

	void desactivarUsuario(Usuario usuario) throws DaoException;

	void eliminarUsuario(Usuario usuario) throws DaoException;

	void editarUsuario(Usuario usuario) throws DaoException;

	Usuario buscarOtroUsuarioPorNombre(Integer idUsuario, String nombre) throws DaoException;

	Usuario buscarOtroUsuarioPorCorreo(Integer idUsuario, String correo) throws DaoException;

	boolean validarToken(String token, int idUsuario) throws DaoException;

	void validarCuenta(Usuario usuario) throws DaoException;
	
	String generarToken();

	void guardarUsuarioDirecto(Usuario usuario) throws DaoException;

	void restablecerContraseña(Usuario usuario, String clave) throws DaoException;


}
