package com.ubb.fanfiction.service;

import java.util.List;
import java.util.Set;

import com.ubb.fanfiction.domain.Lectura;
import com.ubb.fanfiction.exception.DaoException;

public interface LecturaServicio {

	void actualizarLectura(Lectura lectura) throws DaoException;

	void eliminarLectura(Lectura lectura) throws DaoException;

	void eliminarListaLecturas(Set<Lectura> lecturas) throws DaoException;

	List<Lectura> obtenerHistorialLectura(Integer idUsuario) throws DaoException;

}
