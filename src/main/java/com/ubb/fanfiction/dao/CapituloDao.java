package com.ubb.fanfiction.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ubb.fanfiction.domain.Capitulo;

public interface CapituloDao extends JpaRepository<Capitulo, Integer>{

	@Query("select c from Capitulo c where c.fanfiction.idFanfiction = ?1 and c.numero = ?2")
	public Capitulo findByFanfictionAndNumero(Integer idFanfiction, int numero);
	
	@Query("select c from Capitulo c where c.fanfiction.idFanfiction = ?1 order by c.numero asc")
	public List<Capitulo> findByFanfictionOrderByNumeroAsc(Integer idFanfiction);
	
	@Query("select c from Capitulo c where idCapitulo = ?1 and c.fanfiction.usuario.idUsuario = ?2")
	public Capitulo findByIdAndUsuario(Integer idCapitulo, Integer idUsuario);
	
	@Transactional
	@Modifying
    @Query("delete from  Capitulo c where c.idCapitulo= ?1")
	public void borrar(Integer idCapitulo);
	
}
