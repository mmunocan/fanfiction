package com.ubb.fanfiction.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ubb.fanfiction.domain.Categoria;

public interface CategoriaDao extends JpaRepository<Categoria, Integer>{
	
	
}
