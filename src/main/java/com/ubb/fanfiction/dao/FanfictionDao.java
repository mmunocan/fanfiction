package com.ubb.fanfiction.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ubb.fanfiction.domain.Fanfiction;

public interface FanfictionDao extends FanfictionIDao,JpaRepository<Fanfiction, Integer>{
	
	@Query("Select f from Fanfiction f where f.usuario.idUsuario = ?1 order by f.fechaActualizacion desc")
	List<Fanfiction> findByUsuarioOrderByFechaActualizacionDesc(Integer idUsuario);
	
	@Query("Select f from Fanfiction f where f.usuario.idUsuario = ?1 order by f.fechaCreacion desc")
	List<Fanfiction> findByUsuarioOrderByFechaCreacionDesc(Integer idUsuario);
	
	@Query("Select f from Fanfiction f where f.idFanfiction = ?1 and f.usuario.idUsuario = ?2 ")
	Fanfiction findByIdAndUsuario(Integer idFanfiction, Integer idUsuario);

	@Query("Select f from Fanfiction f where f.usuario.idUsuario = ?1 order by f.fechaCreacion asc")
	List<Fanfiction> findByUsuarioOrderByFechaCreacionAsc(Integer idUsuario);
	
	@Query("Select f from Fanfiction f where f.usuario.idUsuario = ?1 order by f.fechaActualizacion asc")
	List<Fanfiction> findByUsuarioOrderByFechaActualizacionAsc(Integer idUsuario);

	@Query("Select f from Fanfiction f where f.usuario.idUsuario = ?1")
	List<Fanfiction> findByIdUsuario(Integer idUsuario);
	
}
