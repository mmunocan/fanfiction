package com.ubb.fanfiction.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ubb.fanfiction.domain.Puntuacion;

public interface PuntuacionDao extends JpaRepository<Puntuacion, Integer>{
	
	@Query("Select avg(p.valor) from Puntuacion p where p.capitulo.fanfiction.idFanfiction = ?1")
	Double findAverageValorByFanfiction(Integer idFanfiction);
	
	@Query("Select avg(p.valor) from Puntuacion p where p.capitulo.idCapitulo = ?1")
	Double findAverageValor(Integer idCapitulo);

	@Query("select p from Puntuacion p where p.capitulo.idCapitulo = ?1 and p.usuario.idUsuario = ?2")
	Puntuacion findByCapituloAndUsuario(Integer idCapitulo, int idUsuario);

	@Query("select p from Puntuacion p where p.capitulo.fanfiction.usuario.idUsuario = ?1 order by p.fecha desc")
	List<Puntuacion> findByUsuarioFanfictionOrderByFechaDesc(Integer idUsuario);

	@Query("select p from Puntuacion p where p.usuario.idUsuario = ?1 order by p.fecha desc")
	List<Puntuacion> findByUsuarioOrderByFechaDesc(Integer idUsuario);
	
	
}
