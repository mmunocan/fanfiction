package com.ubb.fanfiction.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ubb.fanfiction.domain.Lectura;
import com.ubb.fanfiction.domain.Usuario;

public interface LecturaDao extends JpaRepository<Lectura, Integer>{

	@Query("Select l from Lectura as l where l.usuario = ?1 and l.capitulo.fanfiction.idFanfiction = ?2")
	Lectura findByUsuarioAndFanfiction(Usuario usuario, Integer idFanfiction);
	
	@Query("Select l from Lectura as l where l.usuario.idUsuario = ?1 order by l.fecha desc")
	List<Lectura> findByUsuarioOrderByFecha(Integer idUsuario);

	
	
}
