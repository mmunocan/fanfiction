package com.ubb.fanfiction.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ubb.fanfiction.domain.ClasificacionParental;

public interface ClasificacionParentalDao extends JpaRepository<ClasificacionParental, Integer>{
	
	
}
