package com.ubb.fanfiction.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ubb.fanfiction.domain.HistoriaOriginal;

public interface HistoriaOriginalDao extends JpaRepository<HistoriaOriginal, Integer>{
	
	@Query("Select h from HistoriaOriginal h order by h.nombre")
	public List<HistoriaOriginal> findAllOrderByNombre();
	
	@Query("Select h from HistoriaOriginal h where h.categoria.idCategoria = ?1 order by h.nombre")
	public List<HistoriaOriginal> findByCategoriaOrderByNombre(Integer idCategoria);
	
}
