package com.ubb.fanfiction.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ubb.fanfiction.domain.Genero;

public interface GeneroDao extends JpaRepository<Genero, Integer>{
	
	@Query("Select g from Genero g order by g.nombre")
	public List<Genero> findAllOrderByNombre();
}
