package com.ubb.fanfiction.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ubb.fanfiction.domain.Comentario;

public interface ComentarioDao extends JpaRepository<Comentario, Integer>{
	
	@Query("Select count(c) from Comentario c where c.capitulo.fanfiction.idFanfiction = ?1")
	Long countByFanfiction(Integer idFanfiction);
	
	@Query("Select count(c) from Comentario c where c.capitulo.idCapitulo = ?1")
	Long countByCapitulo(Integer idCapitulo);
	
	@Query("Select c from Comentario c where c.capitulo.idCapitulo = ?1 order by c.fecha desc")
	List<Comentario> findByCapituloOrderByFechaDesc(Integer idCapitulo);
	
	@Query("Select c from Comentario c where c.capitulo.fanfiction.usuario.idUsuario = ?1 order by c.fecha desc")
	List<Comentario> findByUsuarioFanfictionOrderByFechaDesc(Integer idUsuario);
	
	@Query("Select c from Comentario c where c.usuario.idUsuario = ?1 order by c.fecha desc")
	List<Comentario> findByUsuarioOrderByFechaDesc(Integer idUsuario);

	
}
