package com.ubb.fanfiction.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ubb.fanfiction.domain.Usuario;

public interface UsuarioDao extends CrudRepository<Usuario, Integer>{

	Usuario findByCorreo(String correo);

	Usuario findByNombre(String nombre);

	@Query("Select u from Usuario u where u.idUsuario!=?1 and u.correo = ?2")
	Usuario findByNotIdUsuarioAndCorreo(Integer id, String correo);

	@Query("Select u from Usuario u where u.idUsuario!=?1 and u.nombre = ?2")
	Usuario findByNotIdUsuarioAndNombre(Integer id, String nombre);
	
	@Query("Select u from Usuario u where u.idUsuario=?1 and u.token=?2")
	Usuario findByIdUsuarioAndToken(Integer id, String token);

	
	
}
