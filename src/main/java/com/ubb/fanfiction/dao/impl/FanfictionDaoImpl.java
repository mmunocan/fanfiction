package com.ubb.fanfiction.dao.impl;


import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import org.springframework.stereotype.Repository;

import com.ubb.fanfiction.dao.FanfictionIDao;
import com.ubb.fanfiction.domain.Fanfiction;

@Repository
public class FanfictionDaoImpl implements FanfictionIDao{
	
	@PersistenceContext 
	protected EntityManager em;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Fanfiction> buscar(String titulo, String autor, String inicioCreacion, String finCreacion,
			String inicioPublicacion, String finPublicacion, Integer comentarioMinimo, Integer comentarioMaximo,
			Integer capituloMinimo, Integer capituloMaximo, Integer puntuacionMinima, Integer puntuacionMaxima,
			Integer genero, Integer categoria, Integer historiaOriginal, Integer clasificacionParental, String estado,
			String tipo, String atributo, String orden) throws ParseException {
		StringBuilder sb = new StringBuilder(); 
		Map<String, Object> parametros = new HashMap<String, Object>();
		sb.append("select f from Fanfiction f where 1 = 1");
		
	
		if(titulo != null){
			sb.append(" and f.titulo like concat('%',:titulo,'%') ");
			parametros.put("titulo", titulo);
		}
		if(autor != null){
			sb.append(" and f.usuario.nombre like concat('%',:autor,'%') ");
			parametros.put("autor", autor);
		}
		if(inicioCreacion != null)
			sb.append(" and f.fechaCreacion >= '"+inicioCreacion+"' ");
		if(finCreacion != null)
			sb.append(" and f.fechaCreacion <= '"+finCreacion+"' ");
		if(inicioPublicacion != null)
			sb.append(" and f.fechaActualizacion >= '"+inicioPublicacion+"' ");
		if(finPublicacion != null)
			sb.append(" and f.fechaActualizacion <= '"+finPublicacion+"' ");
		if(comentarioMinimo != null)
			sb.append(" and (Select count(c) from Comentario c where c.capitulo.fanfiction.idFanfiction = f.idFanfiction) >= "+comentarioMinimo+" ");
		if(comentarioMaximo != null)
			sb.append(" and (Select count(c) from Comentario c where c.capitulo.fanfiction.idFanfiction = f.idFanfiction) <= "+comentarioMaximo+" ");
		if(capituloMinimo != null)
			sb.append(" and f.capitulos.size >= "+capituloMinimo+" ");
		if(capituloMaximo != null)
			sb.append(" and f.capitulos.size <= "+capituloMaximo+" ");
		if(puntuacionMinima != null)
			sb.append(" and (Select avg(p.valor) from Puntuacion p where p.capitulo.fanfiction.idFanfiction = f.idFanfiction) >= "+puntuacionMinima+" ");
		if(puntuacionMaxima != null)
			sb.append(" and (Select avg(p.valor) from Puntuacion p where p.capitulo.fanfiction.idFanfiction = f.idFanfiction) <= "+puntuacionMaxima+" ");
		if(genero != null){
			sb.append(" and exists (select g from Genero g where g.idGenero = :genero and f.idFanfiction in (select h.idFanfiction from g.fanfictions h)) ");
			parametros.put("genero", genero);
		}
		if(categoria != null){
			sb.append(" and exists (select h from HistoriaOriginal h where h.categoria.idCategoria = :categoria and f.idFanfiction in (select h.idFanfiction from h.fanfictions h)) ");
			parametros.put("categoria", categoria);
		}
		if(historiaOriginal != null){
			sb.append(" and exists (select h from HistoriaOriginal h where h.idHistoriaOriginal = :historiaOriginal and f.idFanfiction in (select h.idFanfiction from h.fanfictions h)) ");
			parametros.put("historiaOriginal", historiaOriginal);
		}	
		if(clasificacionParental != null){
			sb.append(" and f.clasificacionParental.idClasificacionParental = :clasificacionParental  ");
			parametros.put("clasificacionParental", clasificacionParental);
		}
		if(estado != null){
			sb.append(" and f.estado = :estado  ");
			parametros.put("estado", estado);
		}			
		if(tipo != null){
			if(tipo.equals("Normal"))
				sb.append(" and f.historiaOriginals.size = 1");
			if(tipo.equals("Crossover"))
				sb.append(" and f.historiaOriginals.size > 1");
		}
		if(atributo != null)
			sb.append(" order by f."+atributo+" "+orden);
		Query query = em.createQuery(sb.toString());
		for (String key : parametros.keySet()) {
			query.setParameter(key, parametros.get(key));
		}
		return query.getResultList();
	}
}