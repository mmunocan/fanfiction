package com.ubb.fanfiction.dao;

import java.text.ParseException;
import java.util.List;

import com.ubb.fanfiction.domain.Fanfiction;

public interface FanfictionIDao {
	List<Fanfiction> buscar(String titulo, String autor, String inicioCreacion, String finCreacion,
			String inicioPublicacion, String finPublicacion, Integer comentarioMinimo, Integer comentarioMaximo,
			Integer capituloMinimo, Integer capituloMaximo, Integer puntuacionMinima, Integer puntuacionMaxima,
			Integer genero, Integer categoria, Integer historiaOriginal, Integer clasificacionParental, String estado,
			String tipo, String atributo, String orden) throws ParseException;
}
