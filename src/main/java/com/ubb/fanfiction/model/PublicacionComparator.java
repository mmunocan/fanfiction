package com.ubb.fanfiction.model;

import java.util.Comparator;


public class PublicacionComparator implements Comparator<Publicacion>{
	
	public final static int CAPITULOS = 2;
	public final static int COMENTARIO = 3;
	public final static int PUNTUACION = 4;
	
	public final static int DESC = 0;
	public final static int ASC = 1;
	
	private int atributo;
	private int orden;
	
	public PublicacionComparator(int atributo, int orden){
		this.atributo = atributo;
		this.orden = orden;
	}

	@Override
	public int compare(Publicacion p1, Publicacion p2) {
		int resultado = 0;
		if(atributo == CAPITULOS){
			resultado = p2.getFanfiction().getCapitulos().size() - p1.getFanfiction().getCapitulos().size();
			if(orden == ASC) return -resultado;
			else if(orden == DESC) return resultado;
		}else if(atributo == COMENTARIO){
			resultado = (int) (p2.getCantidadComentarios() - p1.getCantidadComentarios());
			if(orden == ASC) return -resultado;
			else if(orden == DESC) return resultado;
		}else if(atributo == PUNTUACION){
			if(orden == ASC) return p1.getPromedioPuntuacion().compareTo(p2.getPromedioPuntuacion());
			else if(orden == DESC) return p2.getPromedioPuntuacion().compareTo(p1.getPromedioPuntuacion());
		}
		return resultado;
		
	}
	

}
