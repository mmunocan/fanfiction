package com.ubb.fanfiction.model;

import java.util.List;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Comentario;

public class CapituloPublicacion {
	private Capitulo capitulo;
	private Long cantidadComentarios;
	private Double promedioPuntuacion;
	private List<Comentario> listaComentarios;
	public Capitulo getCapitulo() {
		return capitulo;
	}
	public void setCapitulo(Capitulo capitulo) {
		this.capitulo = capitulo;
	}
	public Long getCantidadComentarios() {
		return cantidadComentarios;
	}
	public void setCantidadComentarios(Long cantidadComentarios) {
		this.cantidadComentarios = cantidadComentarios;
	}
	public Double getPromedioPuntuacion() {
		return promedioPuntuacion;
	}
	public void setPromedioPuntuacion(Double promedioPuntuacion) {
		if(promedioPuntuacion == null) this.promedioPuntuacion = 0.0;
		else this.promedioPuntuacion = promedioPuntuacion;
	}
	public List<Comentario> getListaComentarios() {
		return listaComentarios;
	}
	public void setListaComentarios(List<Comentario> listaComentarios) {
		this.listaComentarios = listaComentarios;
	}
	
}
