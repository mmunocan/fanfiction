package com.ubb.fanfiction.model;

import com.ubb.fanfiction.domain.Fanfiction;

public class Publicacion {
	private Fanfiction fanfiction;
	private Long cantidadComentarios;
	private Double promedioPuntuacion;
	public Fanfiction getFanfiction() {
		return fanfiction;
	}
	public void setFanfiction(Fanfiction fanfiction) {
		this.fanfiction = fanfiction;
	}
	public Long getCantidadComentarios() {
		return cantidadComentarios;
	}
	public void setCantidadComentarios(Long long1) {
		this.cantidadComentarios = long1;
	}
	public Double getPromedioPuntuacion() {
		return promedioPuntuacion;
	}
	public void setPromedioPuntuacion(Double promedioPuntuacion) {
		if(promedioPuntuacion == null) this.promedioPuntuacion = 0.0;
		else this.promedioPuntuacion = promedioPuntuacion;
	}
	
	
}
