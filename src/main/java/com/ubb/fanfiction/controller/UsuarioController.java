package com.ubb.fanfiction.controller;

import java.util.Calendar;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.EmailException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.ContraseñaForm;
import com.ubb.fanfiction.form.CorreoForm;
import com.ubb.fanfiction.form.CuentaUsuarioForm;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.LecturaServicio;
import com.ubb.fanfiction.service.MensajeCorreoServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class UsuarioController {

	@Autowired
	private LecturaServicio lecturaServicio;
	@Autowired
	private FanfictionServicio fanfictionServicio;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
		
	@Autowired
	private UsuarioServicio usuarioServicio;
	@Autowired
	private MensajeCorreoServicio mensajeCorreoServicio;
	
	@GetMapping(value={"/user/cuentaDeUsuario","/admin/cuentaDeUsuario"})
	public ModelAndView modificarCuentaUsuario() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("editarUsuario");
		vista.addObject("nombreUsuario", usuario.getNombre());
		
		CuentaUsuarioForm cuentaUsuarioForm = new CuentaUsuarioForm();
		cuentaUsuarioForm.setCorreo(usuario.getCorreo());
		cuentaUsuarioForm.setNombre(usuario.getNombre());
		cuentaUsuarioForm.setDescripcion(usuario.getDescripcion().replaceAll("</p><p>", "\n").replaceAll("<p>", "").replaceAll("</p>", ""));
		vista.addObject("cuentaUsuarioForm", cuentaUsuarioForm);
		return vista;
	}
	
	
	
	@GetMapping("/user/desactivar")
	public ModelAndView desactivarCuentaUsuario() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		usuarioServicio.desactivarUsuario(usuario);
		fanfictionServicio.eliminarListaFanfictions(usuario.getFanfictions());
		lecturaServicio.eliminarListaLecturas(usuario.getLecturas());
		ModelAndView vista = new ModelAndView("redirect:/logout");
		return vista;
	}
	
	@PostMapping(value={"/user/cuentaDeUsuario","/admin/cuentaDeUsuario"})
	public ModelAndView guardarCambiosEdicionUsuario(CuentaUsuarioForm formulario, BindingResult resultado) throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		Usuario existente = usuarioServicio.buscarOtroUsuarioPorNombre(usuario.getIdUsuario(), formulario.getNombre()); 
		if(existente != null){
			resultado.rejectValue("nombre", "error.nombre", "El nombre de usuario ingresado está repetido. Escoja otro");
		}
		existente = usuarioServicio.buscarOtroUsuarioPorCorreo(usuario.getIdUsuario(), formulario.getCorreo()); 
		if(existente != null) {
			resultado.rejectValue("correo", "error.correo", "El correo ingresado ya está registrado en el sistema");
		}
		String raw = formulario.getClaveAntigua();
		if(!bCryptPasswordEncoder.matches(raw, usuario.getClave())){
			resultado.rejectValue("claveAntigua", "error.claveAntigua", "Su contraseña no coincide con la que tenía originalmente");
		}
		if(resultado.hasErrors()){
			vista.setViewName("editarUsuario");
			vista.addObject("cuentaUsuarioForm",formulario);
			vista.addObject("nombreUsuario", usuario.getNombre());
			return vista;
		}
		usuario.setNombre(formulario.getNombre());
		usuario.setDescripcion(formulario.getDescripcion());
		if(!formulario.getClave().equals("")){
			usuario.setClave(formulario.getClave());
		}else{
			usuario.setClave(formulario.getClaveAntigua());
		}
		if(!usuario.getCorreo().equals(formulario.getCorreo())){
			String token = usuarioServicio.generarToken();
			usuario.setNuevoCorreo(formulario.getCorreo());
			usuario.setToken(token);
			try {
				mensajeCorreoServicio.enviarCorreoCambioCorreoCuenta(formulario.getPath(),usuario.getIdUsuario(), token, formulario.getCorreo());
			} catch (EmailException e) {
				vista.setViewName("editarUsuario");
				vista.addObject("cuentaUsuarioForm",formulario);
				vista.addObject("nombreUsuario", usuario.getNombre());
				vista.addObject("mensajeError", "No se pudo enviar el correo. Recomendamos que intente de nuevo más tarde.");
				return vista;
			}
			vista.addObject("mensaje", "Cambios guardados. Podrá iniciar sesión con su nuevo correo cuando sea activado. Si no llega un correo, intente nuevamente.");
		}else{
			vista.addObject("mensaje", "Cambios guardados");
		}
		vista.setViewName("userhome");
		vista.addObject("nombreUsuario", usuario.getNombre());
		
		usuarioServicio.editarUsuario(usuario);
		vista.setViewName("userhome");
		vista.addObject("nombreUsuario",formulario.getNombre());
		return vista;
		
	}

	
	@GetMapping("/validar/{token}/{id}")
	public ModelAndView validarCuenta(@PathVariable("token")String token, @PathVariable("id")int idUsuario) throws ObjectNotFoundException, DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario sesion = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		if(usuarioServicio.validarToken(token,idUsuario)){
			Usuario usuario = usuarioServicio.buscarUsuarioPorId(idUsuario);
			usuarioServicio.validarCuenta(usuario);
			vista.addObject("mensajeExito", "Su correo ha sido activado");
			if(sesion != null){
				vista.setViewName("cerrarSesion");
				vista.addObject("nombreUsuario",sesion.getNombre());
			}
			else{
				vista.setViewName("login");
			}
		}else
			vista.setViewName("enlaceNoValido");
		return vista;
	}


	@GetMapping("/recuperar")
	public ModelAndView recibirCorreoRecuperacionContraseña() {
		ModelAndView vista = new ModelAndView("recuperarContraseña");
		vista.addObject("correoForm", new CorreoForm());
		return vista;
	}


	@PostMapping("/recuperar")
	public ModelAndView generarEnlaceRecuperacion(@Valid CorreoForm formulario, BindingResult resultado) throws DaoException {
		ModelAndView vista = new ModelAndView();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(formulario.getCorreo());
		if(usuario == null){
			resultado.rejectValue("correo", "error.correo", "El correo no se encuentra registrado");
		}if(usuario != null && !usuario.isCorreoActivo()){
			resultado.rejectValue("correo", "error.correo", "El correo no está activado.");
		}
		if(resultado.hasErrors()){
			vista.setViewName("recuperarContraseña");
			vista.addObject("correoForm", formulario);
			return vista;
		}
		String token = usuarioServicio.generarToken();
		usuario.setToken(token);
		Calendar mañana = Calendar.getInstance();
		mañana.add(Calendar.HOUR, 24);
		usuario.setVencimientoToken(mañana.getTime());
		usuarioServicio.guardarUsuarioDirecto(usuario);
		try {
			mensajeCorreoServicio.enviarCorreoRecuperacionContraseña(formulario.getPath(), usuario.getIdUsuario(), token, formulario.getCorreo());
		} catch (EmailException e) {
			vista.setViewName("recuperarContraseña");
			vista.addObject("correoForm", formulario);
			vista.addObject("mensajeError", "No se pudo enviar el correo. Mensaje del error: " + e.getMessage());
			return vista;
		}
		vista.addObject("mensajeExito","El correo de recuperación de la contraseña ha sido enviado correctamente");
		vista.setViewName("login");
		return vista;
	}


	@GetMapping("/recuperar/{id}/{token}")
	public ModelAndView recibirEnlaceRecuperacionContraseña(@PathVariable("id")Integer idUsuario, @PathVariable("token")String token) throws DaoException, ObjectNotFoundException {
		ModelAndView vista = new ModelAndView();	
		if(usuarioServicio.validarToken(token, idUsuario)){
			Usuario usuario = usuarioServicio.buscarUsuarioPorId(idUsuario);
			Date hoy = Calendar.getInstance().getTime();
			Date vencimiento = usuario.getVencimientoToken();
			if(vencimiento.before(hoy)){
				return new ModelAndView("enlaceVencido");
			}
			vista.setViewName("recibirContraseña");
			ContraseñaForm formulario = new ContraseñaForm();
			formulario.setIdUsuario(idUsuario);
			vista.addObject("contrasenaForm", formulario);
		}else{
			vista.setViewName("enlaceNoValido");
		}
		return vista;
	}


	@PostMapping("/recuperar/restablecer")
	public ModelAndView restablecerContraseña(@Valid ContraseñaForm formulario, BindingResult resultado) throws ObjectNotFoundException, DaoException {
		ModelAndView vista = new ModelAndView();
		if(resultado.hasErrors()){
			vista.setViewName("recibirContraseña");
			vista.addObject("contrasenaForm", formulario);
			return vista;
		}
		vista.setViewName("login");
		Usuario usuario = usuarioServicio.buscarUsuarioPorId(formulario.getIdUsuario());
		usuarioServicio.restablecerContraseña(usuario, formulario.getClave());
		vista.addObject("mensajeExito","Su contraseña ha sido restablecida");
		return vista;
	}
}
