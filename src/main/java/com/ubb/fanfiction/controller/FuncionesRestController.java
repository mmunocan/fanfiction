package com.ubb.fanfiction.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.domain.Puntuacion;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.ComentarioForm;
import com.ubb.fanfiction.form.PuntuacionForm;
import com.ubb.fanfiction.model.RespuestaAjax;
import com.ubb.fanfiction.service.CapituloServicio;
import com.ubb.fanfiction.service.ComentarioServicio;
import com.ubb.fanfiction.service.HistoriaOriginalServicio;
import com.ubb.fanfiction.service.PuntuacionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RestController
public class FuncionesRestController {
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	@Autowired
	private CapituloServicio capituloServicio;
	@Autowired
	private ComentarioServicio comentarioServicio;
	@Autowired
	private PuntuacionServicio puntuacionServicio;
	@Autowired
	private HistoriaOriginalServicio historiaOriginalServicio;
	
	@PostMapping("/user/puntuar")
	public ResponseEntity<?> entregarPuntuacion(@Valid @RequestBody PuntuacionForm formulario, Errors errors) throws DaoException, ObjectNotFoundException {
		RespuestaAjax respuesta = new RespuestaAjax();
		if (errors.hasErrors()) {
			respuesta.setMensaje("Error al entregar puntuación");
			return ResponseEntity.badRequest().body(respuesta);
        }
		Puntuacion puntuacion;
		try{
			puntuacion = puntuacionServicio.obtenerPuntuacion(formulario.getIdCapitulo(), formulario.getIdUsuario());
		}catch(ObjectNotFoundException e){
			puntuacion = new Puntuacion();
			puntuacion.setCapitulo(capituloServicio.obtenerCapitulo(formulario.getIdCapitulo()));
			puntuacion.setUsuario(usuarioServicio.buscarUsuarioPorId(formulario.getIdUsuario()));
		}
		puntuacion.setValor((byte)(int) formulario.getValor());
		
		puntuacionServicio.guardarPuntuacion(puntuacion);
		
		respuesta.setMensaje("La puntuación fue entregada exitosamente");
		return ResponseEntity.ok(respuesta);
	}
	
	@PostMapping("/user/comentar")
	public ResponseEntity<?> entregarComentario(@Valid @RequestBody ComentarioForm formulario, Errors errors) throws ObjectNotFoundException, DaoException {
		RespuestaAjax respuesta = new RespuestaAjax();
		if (errors.hasErrors()) {
			respuesta.setMensaje("Error al enviar comentario");
			return ResponseEntity.badRequest().body(respuesta);
        }
		
		Comentario comentario = new Comentario();
		comentario.setCapitulo(capituloServicio.obtenerCapitulo(formulario.getIdCapitulo()));
		comentario.setUsuario(usuarioServicio.buscarUsuarioPorId(formulario.getIdUsuario()));
		comentario.setContenido(formulario.getComentario());
		comentario = comentarioServicio.guardarComentario(comentario);
		
		respuesta.setMensaje("El comentario ha sido enviado");
		return ResponseEntity.ok(respuesta);
		
	}
	
	@GetMapping("/fanfiction/listHU/{id}")
	public ResponseEntity<?> buscarHistoriasOriginalesDeCategoria(@PathVariable("id")Integer idCategoria) {
		RespuestaAjax respuesta = new RespuestaAjax();
		List<HistoriaOriginal> historias;
		try{
			historias = historiaOriginalServicio.buscarHistoriaPorCategoria(idCategoria);
		}catch(Exception e){
			respuesta.setMensaje("Error al buscar las historias originales");
			return ResponseEntity.badRequest().body(respuesta);
		}
		
		String json = "[";
		for(int i = 0; i < historias.size(); i++){
			json += "{\"id\":\""+historias.get(i).getIdHistoriaOriginal()+"\",\"nombre\":\""+historias.get(i).getNombre()+"\"}";
			if(i != historias.size()-1) json+=",";
		}
		json+= "]";
		
		respuesta.setMensaje(json);
		return ResponseEntity.ok(respuesta);
	}
}
