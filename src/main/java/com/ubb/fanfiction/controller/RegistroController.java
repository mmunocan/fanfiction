package com.ubb.fanfiction.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.EmailException;
import com.ubb.fanfiction.form.RegistroForm;
import com.ubb.fanfiction.service.MensajeCorreoServicio;
import com.ubb.fanfiction.service.UsuarioServicio;
@Controller
public class RegistroController {
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	@Autowired
	private MensajeCorreoServicio mensajeCorreoServicio;
	
	@GetMapping("/registro")
	public ModelAndView irAlRegistro() {
		ModelAndView vista = new ModelAndView("registro");
		vista.addObject("registroForm", new RegistroForm());
		return vista;
	}

	@PostMapping("/registro")
	public ModelAndView registrarUsuario(@Valid RegistroForm formulario, BindingResult resultado) throws DaoException {
		ModelAndView vista = new ModelAndView();
		Usuario existente = usuarioServicio.buscarUsuarioPorCorreo(formulario.getCorreo());
		if(existente != null)
			resultado.rejectValue("correo", "error.correo", "El correo ingresado ya está registrado en el sistema");
		existente = usuarioServicio.buscarUsuarioPorNombre(formulario.getNombre());
		if(existente != null)
			resultado.rejectValue("nombre", "error.nombre", "El nombre de usuario ingresado está repetido. Escoja otro");
		if(resultado.hasErrors()){
			vista.addObject("registroForm", formulario);
			vista.setViewName("registro");
			return vista;
		}
		Usuario usuario = new Usuario();
		usuario.setNombre(formulario.getNombre());
		usuario.setCorreo(formulario.getCorreo());
		usuario.setClave(formulario.getClave());
		String token = usuarioServicio.generarToken();
		usuario.setToken(token);
		usuario.setDescripcion(formulario.getDescripcion());
		
		try {
			mensajeCorreoServicio.enviarCorreoActivacionCuenta(formulario.getPath(),usuarioServicio.guardarUsuario(usuario),token, formulario.getCorreo());
		} catch (EmailException e) {
			vista.addObject("registroForm", formulario);
			vista.setViewName("registro");
			vista.addObject("mensajeError", "No se pudo enviar el correo. ");
			return vista;
		}
		vista.addObject("mensajeExito", "La cuenta ha sido creada. Ingrese usando el enlace enviado a su correo para activar su cuenta");
		vista.setViewName("registro");
		return vista;
		
		
	}


}
