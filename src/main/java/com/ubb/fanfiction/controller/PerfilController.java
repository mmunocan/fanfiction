package com.ubb.fanfiction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.OrdenarFanfictionForm;
import com.ubb.fanfiction.model.PublicacionComparator;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class PerfilController {
	
	@Autowired
	private FanfictionServicio fanfictionServicio;
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@GetMapping("/fanfiction/perfilUsuario/{id}")
	public ModelAndView mostrarPerfilDeUsuario(@PathVariable("id")Integer idUsuario) throws DaoException {
		Usuario perfil;
		ModelAndView vista = new ModelAndView();
		
		try {
			perfil = usuarioServicio.buscarUsuarioPorId(idUsuario);
		} catch (ObjectNotFoundException e) {
			vista.setViewName("usuarioNoExiste");
			return vista;
		}
		if(!perfil.isEstado()){
			vista.setViewName("usuarioNoExiste");
			return vista;
		}
		if(perfil.getRol().equals("ADMIN")){
			vista.setViewName("usuarioNoExiste");
			return vista;
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario!=null)
			vista.addObject("nombreUsuario", usuario.getNombre());
		
		vista.setViewName("perfilusuario");
		vista.addObject("usuario",perfil);
		vista.addObject("fanfictions",fanfictionServicio.obtenerListaFanfictionsUsuario(idUsuario, OrdenarFanfictionForm.FECHA_ACTUALIZACION, PublicacionComparator.DESC));
		return vista;
	}
	
}
