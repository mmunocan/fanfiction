package com.ubb.fanfiction.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.form.FanfictionEditForm;
import com.ubb.fanfiction.form.FanfictionForm;
import com.ubb.fanfiction.form.OrdenarFanfictionForm;
import com.ubb.fanfiction.service.CapituloServicio;
import com.ubb.fanfiction.service.ClasificacionParentalServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.GeneroServicio;
import com.ubb.fanfiction.service.HistoriaOriginalServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class FanfictionController {
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@Autowired
	private FanfictionServicio fanfictionServicio;
	@Autowired
	private CapituloServicio capituloServicio;
	
	@Autowired
	private HistoriaOriginalServicio historiaOriginalServicio;
	@Autowired
	private GeneroServicio generoServicio;
	@Autowired
	private ClasificacionParentalServicio clasificacionParentalServicio;
	
	
	@GetMapping("/user/fanfictions")
	public ModelAndView abrirListadoDeFanfictions() throws ObjectNotFoundException, DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("listadefanfictions");
		vista.addObject("fanfictions", fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC));
		vista.addObject("nombreUsuario", usuario.getNombre());		
		vista.addObject("ordenarFanfictionForm",new OrdenarFanfictionForm());
		return vista;
	}
	
	@PostMapping("/user/fanfictions")
	public ModelAndView ordenarListaFanfiction(OrdenarFanfictionForm orden) throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("listadefanfictions");
		vista.addObject("fanfictions", fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), orden.getAtributoOrden(), orden.getOrdenBusqueda()));
		vista.addObject("nombreUsuario", usuario.getNombre());	
		vista.addObject("ordenarFanfictionForm",orden);
		return vista;
	}
	
	@GetMapping("/user/fanfiction/nuevo")
	public ModelAndView abrirFormularioCrearNuevoFic(HttpSession sesion) throws ObjectNotFoundException, DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		
		List<HistoriaOriginal> historiasOriginales = historiaOriginalServicio.obtenerListaHistoriasOriginales();
		List<ClasificacionParental> clasificacionesParentales = clasificacionParentalServicio.obtenerClasificacionesParentales();
		List<Genero> generos = generoServicio.obtenerListaGeneros();
		
		sesion.setAttribute("historiasOriginales", historiasOriginales);
		sesion.setAttribute("clasificacionesParentales", clasificacionesParentales);
		sesion.setAttribute("generos", generos);
		
		ModelAndView vista = new ModelAndView("nuevofanfiction");
		vista.addObject("historiasOriginales", historiasOriginales);
		vista.addObject("clasificacionesParentales", clasificacionesParentales);
		vista.addObject("generos", generos);
		vista.addObject("fanfictionForm", new FanfictionForm());
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}
	
	@PostMapping("/user/fanfiction/nuevo")
	public ModelAndView guardarFanfiction(@Valid FanfictionForm formulario, BindingResult resultado, HttpSession sesion) throws ObjectNotFoundException, DaoException  {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(formulario.getHistoriasOriginales() == null || formulario.getHistoriasOriginales().length == 0){
			resultado.rejectValue("historiasOriginales", "error.historiasOriginales", "´Por favor, seleccione al menos una historia original");
		}
		if(formulario.getGeneros() == null || formulario.getGeneros().length == 0){
			resultado.rejectValue("generos", "error.generos", "Por favor, seleccione al menos un género");
		}
		
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(resultado.hasErrors()){
			vista.setViewName("nuevoFanfiction");
			vista.addObject("historiasOriginales",sesion.getAttribute("historiasOriginales"));
			vista.addObject("clasificacionesParentales",sesion.getAttribute("clasificacionesParentales"));
			vista.addObject("generos",sesion.getAttribute("generos"));
			vista.addObject("fanfictionForm", formulario);
			return vista;
		}
		
		Fanfiction fanfiction = new Fanfiction();
		fanfiction.setTitulo(formulario.getTitulo());
		fanfiction.setDescripcion(formulario.getDescripcion());
		HashSet<HistoriaOriginal> historiasElegidas = new HashSet<HistoriaOriginal>();
		for(int idHistoriaOriginal : formulario.getHistoriasOriginales())
			historiasElegidas.add(historiaOriginalServicio.buscarHistoriaOriginalPorId(idHistoriaOriginal));
		fanfiction.setHistoriaOriginals(historiasElegidas);
		HashSet<Genero> generosElegidos = new HashSet<Genero>();
		for(int idGenero : formulario.getGeneros())
			generosElegidos.add(generoServicio.buscarGeneroPorId(idGenero));
		fanfiction.setGeneros(generosElegidos);
		fanfiction.setEstado(formulario.getEstado());
		fanfiction.setClasificacionParental(clasificacionParentalServicio.buscarClasificacionParentalPorId(formulario.getClasificacionParental()));
		fanfiction.setUsuario(usuario);
		
		Capitulo capituloUno = new Capitulo();
		capituloUno.setTitulo(formulario.getTituloCapitulo());
		capituloUno.setNumero(1);
		capituloUno.setContenido(formulario.getContenido());
		
		Fanfiction guardado = fanfictionServicio.guardarFanfiction(fanfiction);
		capituloUno.setFanfiction(guardado);
		capituloServicio.guardarCapitulo(capituloUno);
		
		vista.setViewName("userhome");
		vista.addObject("fanfictions", fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC));
		vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("mensaje", "El Fanfiction ha sido creado exitosamente");		
		return vista;
		
	}
	
	
	@GetMapping("/user/fanfiction/editar/{id}")
	public ModelAndView abrirFormularioEditarFanfiction(@PathVariable("id")Integer id, HttpSession sesion) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(!fanfictionServicio.fanfictionEsDeUsuario(id, usuario.getIdUsuario())){
			vista.setViewName("403");
			return vista;
		}
		
		List<HistoriaOriginal> historiasOriginales = historiaOriginalServicio.obtenerListaHistoriasOriginales();
		List<ClasificacionParental> clasificacionesParentales = clasificacionParentalServicio.obtenerClasificacionesParentales();
		List<Genero> generos = generoServicio.obtenerListaGeneros();
		sesion.setAttribute("historiasOriginales", historiasOriginales);
		sesion.setAttribute("clasificacionesParentales", clasificacionesParentales);
		sesion.setAttribute("generos", generos);
		
		Fanfiction fanfiction = fanfictionServicio.buscarFanfictionPorId(id);
		FanfictionEditForm fanfictionForm = new FanfictionEditForm();
		fanfictionForm.setId(id);
		fanfictionForm.setTitulo(fanfiction.getTitulo());
		fanfictionForm.setEstado(fanfiction.getEstado());
		fanfictionForm.setDescripcion(fanfiction.getDescripcion().replaceAll("<p>", "").replaceAll("</p>", "").replaceAll("<p></p>", "\n"));
		fanfictionForm.setClasificacionParental(fanfiction.getClasificacionParental().getIdClasificacionParental());
		List<Integer> idGenerosSeleccionados = new ArrayList<Integer>();
		List<Integer> idHistoriasSeleccionadas = new ArrayList<Integer>();
		for(Genero genero : fanfiction.getGeneros())
			idGenerosSeleccionados.add(genero.getIdGenero());
		for(HistoriaOriginal historia: fanfiction.getHistoriaOriginals())
			idHistoriasSeleccionadas.add(historia.getIdHistoriaOriginal());

		vista.setViewName("editarFanfiction");
		vista.addObject("historiasOriginales", historiasOriginales);
		vista.addObject("clasificacionesParentales", clasificacionesParentales);
		vista.addObject("generos", generos);
		vista.addObject("fanfictionEditForm", fanfictionForm);
		vista.addObject("fanfiction",fanfiction);
		vista.addObject("generosSeleccionados",idGenerosSeleccionados);
		vista.addObject("historiasSeleccionadas",idHistoriasSeleccionadas);
		return vista;
	}
	
	@PostMapping("/user/fanfiction/editar")
	public ModelAndView editarFanfiction(@Valid FanfictionEditForm formulario, BindingResult resultado, HttpSession sesion) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(formulario.getGeneros() == null || formulario.getGeneros().length == 0){
			resultado.rejectValue("generos", "error.generos", "Por favor, seleccione al menos un género");
		}
		if(formulario.getHistoriasOriginales() == null || formulario.getHistoriasOriginales().length == 0){
			resultado.rejectValue("historiasOriginales", "error.historiasOriginales", "Por favor, seleccione al menos una historia original");
		}
		
		if(resultado.hasErrors()){
			vista.setViewName("editarFanfiction");
			vista.addObject("historiasOriginales",sesion.getAttribute("historiasOriginales"));
			vista.addObject("clasificacionesParentales",sesion.getAttribute("clasificacionesParentales"));
			vista.addObject("generos",sesion.getAttribute("generos"));
			vista.addObject("fanfictionEditForm", formulario);
			
			List<Integer> idGenerosSeleccionados = new ArrayList<Integer>();
			List<Integer> idHistoriasSeleccionadas = new ArrayList<Integer>();
			
			if(formulario.getGeneros() != null){
				for(int i : formulario.getGeneros())
					idGenerosSeleccionados.add(i);
			}
			if(formulario.getHistoriasOriginales() != null){
				for(int i : formulario.getHistoriasOriginales())
					idHistoriasSeleccionadas.add(i);
			}
			vista.addObject("generosSeleccionados",idGenerosSeleccionados);
			vista.addObject("historiasSeleccionadas",idHistoriasSeleccionadas);
			return vista;
		}		
		
		Fanfiction fanfiction = fanfictionServicio.buscarFanfictionPorId(formulario.getId());
		fanfiction.setTitulo(formulario.getTitulo());
		fanfiction.setDescripcion(formulario.getDescripcion());
		HashSet<HistoriaOriginal> historiasElegidas = new HashSet<HistoriaOriginal>();
		for(int idHistoriaOriginal : formulario.getHistoriasOriginales())
			historiasElegidas.add(historiaOriginalServicio.buscarHistoriaOriginalPorId(idHistoriaOriginal));
		fanfiction.setHistoriaOriginals(historiasElegidas);
		HashSet<Genero> generosElegidos = new HashSet<Genero>();
		for(int idGenero : formulario.getGeneros())
			generosElegidos.add(generoServicio.buscarGeneroPorId(idGenero));
		fanfiction.setGeneros(generosElegidos);
		fanfiction.setEstado(formulario.getEstado());
		fanfiction.setClasificacionParental(clasificacionParentalServicio.buscarClasificacionParentalPorId(formulario.getClasificacionParental()));
		fanfiction.setUsuario(usuario);
		fanfictionServicio.actualizarFanfiction(fanfiction);
		
		vista.setViewName("listadefanfictions");
		vista.addObject("fanfictions", fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC));
		vista.addObject("mensaje", "Cambios guardados");
		vista.addObject("ordenarFanfictionForm", new OrdenarFanfictionForm());
		return vista;
	}
	
	@GetMapping("/user/fanfiction/eliminar/{id}")
	public ModelAndView eliminarFanfiction(@PathVariable("id")Integer id) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(!fanfictionServicio.fanfictionEsDeUsuario(id, usuario.getIdUsuario())){
			vista.setViewName("403");
			return vista;
		}
		fanfictionServicio.eliminarFanfiction(fanfictionServicio.buscarFanfictionPorId(id));
		
		vista.setViewName("listadefanfictions");
		vista.addObject("fanfictions", fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC));
		vista.addObject("mensaje", "El fanfiction ha sido eliminado");
		vista.addObject("ordenarFanfictionForm", new OrdenarFanfictionForm());
		return vista;
	}

	

}
