package com.ubb.fanfiction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.service.LecturaServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class LecturaController {
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	@Autowired
	private LecturaServicio lecturaServicio;

	@GetMapping("/user/historial/lecturas")
	public ModelAndView obtenerHistorialLectura() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("historialLecturas");
		vista.addObject("lecturas", lecturaServicio.obtenerHistorialLectura(usuario.getIdUsuario()));
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}

}
