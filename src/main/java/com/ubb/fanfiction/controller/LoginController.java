package com.ubb.fanfiction.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class LoginController {
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@GetMapping("/")
	public ModelAndView irAlIndex() throws DaoException {
		ModelAndView vista = new ModelAndView("index");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario != null){
			vista.addObject("nombreUsuario", usuario.getNombre());
		}
		return vista;
	}
		
	
	@GetMapping("/403")
	public ModelAndView error() throws DaoException{
		ModelAndView vista = new ModelAndView("403");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario != null){
			vista.addObject("nombreUsuario", usuario.getNombre());
		}
		return vista;
	}
	
	@GetMapping("/login")
	public String irAlLogin() {
		return "login";
	}
	
	@GetMapping("/exito")
	public String inicioSesion() throws ObjectNotFoundException, DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario.getRol().equals("ADMIN"))
			return "redirect:/admin/home";
		else 
			return "redirect:/user/home";
		
		
	}
	
	@GetMapping(value={"/user/home","/admin/home"})
	public ModelAndView cargarPaginaPrincipal() throws ObjectNotFoundException, DaoException {
		ModelAndView vista = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(usuario.getRol().equals("USER"))	vista.setViewName("userhome");
		if(usuario.getRol().equals("ADMIN"))	vista.setViewName("adminhome");
		return vista;
	
	}

	@GetMapping("/condiciones")
	public ModelAndView verTerminosYCondiciones() throws DaoException {
		ModelAndView vista = new ModelAndView("terminosYCondiciones");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario != null){
			vista.addObject("nombreUsuario", usuario.getNombre());
		}
		return vista;
	}

	@GetMapping("/acerca")
	public ModelAndView acercaDe() throws DaoException {
		ModelAndView vista = new ModelAndView("acercaDe");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario != null){
			vista.addObject("nombreUsuario", usuario.getNombre());
		}
		return vista;
	}
	
}
