package com.ubb.fanfiction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Lectura;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.model.CapituloPublicacion;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.CapituloServicio;
import com.ubb.fanfiction.service.ComentarioServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.LecturaServicio;
import com.ubb.fanfiction.service.PuntuacionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class LeerCapituloController {

	@Autowired
	private LecturaServicio lecturaServicio;
	@Autowired
	private PuntuacionServicio puntuacionServicio;
	@Autowired
	private ComentarioServicio comentarioServicio;
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@Autowired
	private FanfictionServicio fanfictionServicio;
	@Autowired
	private CapituloServicio capituloServicio;
	
	@GetMapping("/fanfiction/mostrar/{id}/{numero}")
	public ModelAndView leerCapitulo(@PathVariable("id")Integer idFanfiction, @PathVariable("numero")int numeroCapitulo) throws DaoException, ObjectNotFoundException {
		ModelAndView vista = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario != null)
			vista.addObject("nombreUsuario", usuario.getNombre());
		
		Capitulo capitulo;
		try{
			capitulo = capituloServicio.obtenerCapitulo(idFanfiction, numeroCapitulo);
		}catch(ObjectNotFoundException e){
			vista.setViewName("capituloNoExiste");
			return vista;
		}
		
		Fanfiction fanfiction = fanfictionServicio.buscarFanfictionPorId(idFanfiction);
		List<Comentario> comentariosCapitulo = comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(capitulo.getIdCapitulo());
		Publicacion publicacion = new Publicacion();
		publicacion.setFanfiction(fanfiction);
		publicacion.setCantidadComentarios(comentarioServicio.contarComentariosPorFanfiction(idFanfiction));
		publicacion.setPromedioPuntuacion(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction));
		
		CapituloPublicacion capituloPublicacion = new CapituloPublicacion();
		capituloPublicacion.setCapitulo(capitulo);
		capituloPublicacion.setCantidadComentarios((long) comentariosCapitulo.size());
		capituloPublicacion.setListaComentarios(comentariosCapitulo);
		capituloPublicacion.setPromedioPuntuacion(puntuacionServicio.obtenerPromedioPuntuacionCapitulo(capitulo.getIdCapitulo()));
		
		vista.setViewName("capitulo");
		vista.addObject("publicacion", publicacion);
		vista.addObject("capituloPublicacion", capituloPublicacion);
		
		if(numeroCapitulo != 1)
			vista.addObject("anterior", "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo - 1));
		if(numeroCapitulo != capitulo.getFanfiction().getCapitulos().size())
			vista.addObject("siguiente", "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo + 1));
		
		if(usuario != null){
			Lectura lectura = new Lectura();
			lectura.setCapitulo(capitulo);
			lectura.setUsuario(usuario);
			lecturaServicio.actualizarLectura(lectura);
			int valorPuntuacion = 0;
			try{
				valorPuntuacion = puntuacionServicio.obtenerPuntuacion(capitulo.getIdCapitulo(), usuario.getIdUsuario()).getValor();
			}catch(ObjectNotFoundException e){}
			vista.addObject("idUsuario", usuario.getIdUsuario());
			vista.addObject("puntuacion", valorPuntuacion);
		}
		return vista;
	}
	
}
