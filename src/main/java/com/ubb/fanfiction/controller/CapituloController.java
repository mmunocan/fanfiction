package com.ubb.fanfiction.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.CapituloForm;
import com.ubb.fanfiction.form.OrdenarFanfictionForm;
import com.ubb.fanfiction.service.CapituloServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class CapituloController {

	@Autowired
	private CapituloServicio capituloServicio;
	@Autowired
	private FanfictionServicio fanfictionServicio;
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@GetMapping("/user/capitulo/ver/{id}")
	public ModelAndView obtenerListaDeCapitulos(@PathVariable("id")Integer idFanfiction) throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(!fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, usuario.getIdUsuario())){
			vista.setViewName("403");
			return vista;
		}
		vista.setViewName("listadecapitulos");	
		vista.addObject("capitulosPublicacion", capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(idFanfiction));
		return vista;
	}
	
	@GetMapping("/user/capitulo/nuevo/{id}")
	public ModelAndView abrirFormularioCrearNuevoCapitulo(@PathVariable("id")Integer idFanfiction) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(!fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, usuario.getIdUsuario())){
			vista.setViewName("403");
			return vista;
		}
		vista.setViewName("nuevocapitulo");
		vista.addObject("capituloForm", new CapituloForm());
		vista.addObject("fanfiction", fanfictionServicio.buscarFanfictionPorId(idFanfiction));
		return vista;
	}
	
	@PostMapping("/user/capitulo/guardar")
	public ModelAndView guardarCapitulo(@Valid CapituloForm formulario, BindingResult resultado) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		Fanfiction fanfiction = fanfictionServicio.buscarFanfictionPorId(formulario.getIdFanfiction());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(resultado.hasErrors()){
			vista.setViewName("nuevocapitulo");
			vista.addObject("capituloForm", formulario);
			vista.addObject("fanfiction", fanfiction);
			return vista;
		}
		
		Capitulo capitulo = new Capitulo();
		capitulo.setTitulo(formulario.getTitulo());
		capitulo.setNumero(formulario.getNumero());
		capitulo.setContenido(formulario.getContenido());
		capitulo.setFanfiction(fanfiction);
		
		capituloServicio.guardarCapitulo(capitulo);		
		fanfictionServicio.actualizarFanfiction(fanfiction);
		
		vista.setViewName("listadefanfictions");
		vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("fanfictions", fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC));
		vista.addObject("ordenarFanfictionForm",new OrdenarFanfictionForm());
		vista.addObject("mensaje", "El Capítulo ha sido creado exitosamente");
		return vista;		
		
	}
	
	@GetMapping("/user/capitulo/editar/{id}")
	public ModelAndView abrirFormularioEditarCapitulo(@PathVariable("id")Integer idCapitulo) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario",usuario.getNombre());
		if(!capituloServicio.capituloEsDeUsuario(idCapitulo, usuario.getIdUsuario())){
			vista.setViewName("403");
			return vista;
		}
		
		Capitulo capitulo = capituloServicio.obtenerCapitulo(idCapitulo);
		CapituloForm capituloForm = new CapituloForm();
		capituloForm.setIdCapitulo(capitulo.getIdCapitulo());
		capituloForm.setTitulo(capitulo.getTitulo());
		capituloForm.setContenido(capitulo.getContenido().replaceAll("<p>", "").replaceAll("</p>", "").replaceAll("<p></p>", "\n"));
		
		vista.setViewName("editarCapitulo");
		vista.addObject("capituloForm",capituloForm);
		vista.addObject("capitulo", capitulo);
		return vista;
	}
	
	@PostMapping("/user/capitulo/editar")
	public ModelAndView editarCapitulo(@Valid CapituloForm formulario, BindingResult resultado) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario",usuario.getNombre());
		Capitulo capitulo = capituloServicio.obtenerCapitulo(formulario.getIdCapitulo());
		if(resultado.hasErrors()){
			vista.setViewName("editarCapitulo");
			vista.addObject("capituloForm", formulario);
			vista.addObject("capitulo", capitulo);
			return vista;
		}
		
		capitulo.setTitulo(formulario.getTitulo());
		capitulo.setContenido(formulario.getContenido());
		
		capituloServicio.editarCapitulo(capitulo);
		fanfictionServicio.actualizarFanfiction(capitulo.getFanfiction());
		
		vista.setViewName("listadecapitulos");
		vista.addObject("capitulosPublicacion", capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(capitulo.getFanfiction().getIdFanfiction()));
		vista.addObject("mensaje", "Cambios guardados");
		return vista;
	}

	@GetMapping("/user/capitulo/eliminar/{id}")
	public ModelAndView eliminarCapitulo(@PathVariable("id")Integer idCapitulo) throws  ObjectNotFoundException, DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario",usuario.getNombre());
		if(!capituloServicio.capituloEsDeUsuario(idCapitulo, usuario.getIdUsuario())){
			vista.setViewName("403");
			return vista;
		}
		vista.setViewName("listadecapitulos");
		Capitulo capitulo = capituloServicio.obtenerCapitulo(idCapitulo);
		
		if(capitulo.getFanfiction().getCapitulos().size() != 1){
			capituloServicio.eliminarCapitulo(capitulo);
			fanfictionServicio.actualizarFanfiction(capitulo.getFanfiction());
			vista.addObject("mensaje","El capítulo ha sido eliminado");
			
		}else{
			vista.addObject("mensajeError","El capítulo no se puede eliminar por ser el único del Fanfiction");
		}
		vista.addObject("capitulosPublicacion", capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(capitulo.getFanfiction().getIdFanfiction()));
		return vista;
	}

}
