package com.ubb.fanfiction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.service.PuntuacionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class PuntuacionController {
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	@Autowired
	private PuntuacionServicio puntuacionServicio;
	
	@GetMapping("/user/historial/puntuacionesRecibidas")
	public ModelAndView obtenerListaPuntuacionesRecibidas() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("puntuacionesRecibidas");
		vista.addObject("puntuacionesRecibidas", puntuacionServicio.obtenerPuntuacionesRecibidas(usuario.getIdUsuario()));
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}
	
	@GetMapping("/user/historial/puntuacionesRealizadas")
	public ModelAndView obtenerListaPuntuacionesRealizadas() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("puntuacionesRealizadas");
		vista.addObject("puntuacionesRealizadas", puntuacionServicio.obtenerPuntuacionesRealizadas(usuario.getIdUsuario()));
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}
	
		
}
