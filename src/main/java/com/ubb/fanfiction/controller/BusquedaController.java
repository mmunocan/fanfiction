package com.ubb.fanfiction.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.form.BusquedaForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.CategoriaServicio;
import com.ubb.fanfiction.service.ClasificacionParentalServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.GeneroServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class BusquedaController {
	
	@Autowired
	private GeneroServicio generoServicio;
	@Autowired
	private ClasificacionParentalServicio clasificacionParentalServicio;
	@Autowired
	private CategoriaServicio categoriaServicio;
	@Autowired
	private FanfictionServicio fanfictionServicio;
	@Autowired
	private UsuarioServicio usuarioServicio;
	
	@GetMapping("/fanfiction/buscar")
	public ModelAndView buscarFanfiction() throws DaoException {
		ModelAndView vista = new ModelAndView("buscarFanfiction");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario != null)
			vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("clasificacionesParentales", clasificacionParentalServicio.obtenerClasificacionesParentales());
		vista.addObject("generos", generoServicio.obtenerListaGeneros());
		vista.addObject("categorias", categoriaServicio.obtenerListaCategorias());
		vista.addObject("busquedaForm",new BusquedaForm());
		return vista;
	}
	
	@PostMapping("/fanfiction/resultado")
	public ModelAndView buscarFanfiction(@Valid BusquedaForm formulario, BindingResult resultado) throws DaoException {
		String titulo = formulario.getTitulo().equals("")?null:formulario.getTitulo();
		String autor = formulario.getAutor().equals("")?null:formulario.getAutor();
		String inicioCreacion = formulario.getInicioCreacion().equals("")?null:formulario.getInicioCreacion();
		String finCreacion = formulario.getFinCreacion().equals("")?null:formulario.getFinCreacion();
		String inicioPublicacion = formulario.getInicioPublicacion().equals("")?null:formulario.getInicioPublicacion();
		String finPublicacion = formulario.getFinPublicacion().equals("")?null:formulario.getFinPublicacion();
		Integer comentarioMinimo = formulario.getComentarioMinimo();
		Integer comentarioMaximo = formulario.getComentarioMaximo();
		Integer capituloMinimo = formulario.getCapituloMinimo();
		Integer capituloMaximo = formulario.getCapituloMaximo();
		Integer puntuacionMinima = formulario.getPuntuacionMinimo();
		Integer puntuacionMaxima = formulario.getPuntuacionMaximo();
		Integer genero = formulario.getGenero() == -1 ? null : formulario.getGenero();
		Integer categoria = formulario.getCategoria() == -1 ? null : formulario.getCategoria();
		Integer historiaOriginal = formulario.getHistoriaOriginal() == -1 ? null : formulario.getHistoriaOriginal();
		Integer clasificacionParental = formulario.getClasificacionParental() == -1 ? null : formulario.getClasificacionParental();
		String estado = formulario.getEstado().equals("-1") ? null : formulario.getEstado();
		String tipo = formulario.getTipo().equals("-1") ? null : formulario.getTipo();
		int atributoOrden = formulario.getAtributoOrden();
		int ordenBusqueda = formulario.getOrdenBusqueda();
		
		List<Publicacion> resultadoBusqueda = fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda);
		
		ModelAndView vista = new ModelAndView("resultadoBusqueda");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		if(usuario != null)
			vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("fanfictions", resultadoBusqueda);
		vista.addObject("busquedaForm",formulario);
		return vista;
	}
	
	
}
