package com.ubb.fanfiction.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.AdminEliminacionForm;
import com.ubb.fanfiction.form.HistoriaOriginalForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.CategoriaServicio;
import com.ubb.fanfiction.service.ComentarioServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.HistoriaOriginalServicio;
import com.ubb.fanfiction.service.PuntuacionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class AdministradorController {

	@Autowired
	private UsuarioServicio usuarioServicio;
	@Autowired
	private CategoriaServicio categoriaServicio;
	@Autowired
	private HistoriaOriginalServicio historiaOriginalServicio;
	@Autowired
	private ComentarioServicio comentarioServicio;
	@Autowired
	private PuntuacionServicio puntuacionServicio;
	@Autowired
	private FanfictionServicio fanfictionServicio;
	
	@GetMapping("/admin/agregarHistoriaOriginal")
	public ModelAndView abrirFormularioAgregarHistoriaOriginal() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("nuevaHistoriaOriginal");
		vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("historiaOriginalForm",new HistoriaOriginalForm());
		vista.addObject("categorias",categoriaServicio.obtenerListaCategorias());
		return vista;
	}
	
	@PostMapping("/admin/agregarHistoriaOriginal")
	public ModelAndView guardarHistoriaOriginal(@Valid HistoriaOriginalForm historiaOriginalForm, BindingResult resultadoHistoria) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("nuevaHistoriaOriginal");
		vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("categorias",categoriaServicio.obtenerListaCategorias());
		if(resultadoHistoria.hasErrors()){
			vista.addObject("historiaOriginalForm",historiaOriginalForm);
			return vista;
		}
		HistoriaOriginal historia = new HistoriaOriginal();
		historia.setNombre(historiaOriginalForm.getNombre());
		historia.setCategoria(categoriaServicio.obtenerCategoriaPorId(historiaOriginalForm.getCategoria()));
		historiaOriginalServicio.guardarHistoriaOriginal(historia);
		vista.addObject("historiaOriginalForm",new HistoriaOriginalForm());
		vista.addObject("mensajeExito","La Historia Original ha sido creada exitosamente");
		return vista;
	}
	
	@GetMapping("/admin/eliminarComentario")
	public ModelAndView abrirFormularioEliminarComentario() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("eliminarComentario");
		vista.addObject("adminEliminacionForm",new AdminEliminacionForm());
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}
	
	@PostMapping("/admin/confirmarComentario")
	public ModelAndView buscarComentarioAEliminar(@Valid AdminEliminacionForm formulario, BindingResult resultado) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("confirmarEliminarComentario");
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(formulario.getId() == null){
			vista.addObject("adminEliminacionForm",formulario);
			vista.addObject("mensajeNegativo","No existe el comentario con el ID señalado");
			vista.setViewName("eliminarComentario");
			vista.addObject("mensajeNegativo","El valor del ID no puede quedar vacío");
			return vista;
		}
		Comentario comentario;
		try{
			comentario = comentarioServicio.buscarComentarioPorId(formulario.getId());
			vista.addObject("comentario",comentario);
			vista.setViewName("confirmarEliminarComentario");
			return vista;
		}catch(ObjectNotFoundException e){
			vista.addObject("adminEliminacionForm",formulario);
			vista.addObject("mensajeNegativo","No existe el comentario con el ID señalado");
			vista.setViewName("eliminarComentario");
			return vista;
		}
	}
	
	@PostMapping("/admin/eliminarComentario")
	public ModelAndView eliminarComentario(@Valid AdminEliminacionForm formulario, BindingResult resultado) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		comentarioServicio.eliminarComentario(comentarioServicio.buscarComentarioPorId(formulario.getId()));
		ModelAndView vista = new ModelAndView("eliminarComentario");
		vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("adminEliminacionForm",new AdminEliminacionForm());
		vista.addObject("mensajePositivo","El comentario ha sido eliminado");
		return vista;
	}

	@GetMapping("/admin/eliminarFanfiction")
	public ModelAndView abrirFormularioEliminarFanfiction() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("eliminarFanfiction");
		vista.addObject("adminEliminacionForm",new AdminEliminacionForm());
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}
	
	@PostMapping("/admin/confirmarFanfiction")
	public ModelAndView buscarFanfictionAEliminar(@Valid AdminEliminacionForm formulario, BindingResult resultado) throws ObjectNotFoundException, DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(formulario.getId() == null){
			vista.setViewName("eliminarFanfiction");
			vista.addObject("mensajeNegativo","El valor del ID no puede quedar vacío");
			vista.addObject("adminEliminacionForm",formulario);
			return vista;
		}
		Fanfiction fanfiction;
		try{
			fanfiction = fanfictionServicio.buscarFanfictionPorId(formulario.getId());
			vista.setViewName("confirmarEliminarFanficion");
			Publicacion publicacion = new Publicacion();
			publicacion.setFanfiction(fanfiction);
			publicacion.setCantidadComentarios(comentarioServicio.contarComentariosPorFanfiction(formulario.getId()));
			publicacion.setPromedioPuntuacion(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(formulario.getId()));
			vista.addObject("publicacion", publicacion);
			return vista;
		}catch(ObjectNotFoundException e){
			vista.setViewName("eliminarFanfiction");
			vista.addObject("adminEliminacionForm",formulario);
			vista.addObject("mensajeNegativo","No existe el fanfiction con el ID señalado");
			return vista;
		}
		
		
	}
	
	@PostMapping("admin/eliminarFanfiction")
	public ModelAndView eliminarFanfiction(@Valid AdminEliminacionForm formulario,BindingResult resultado) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		fanfictionServicio.eliminarFanfiction(fanfictionServicio.buscarFanfictionPorId(formulario.getId()));
		ModelAndView vista = new ModelAndView("eliminarFanfiction");
		vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("adminEliminacionForm",new AdminEliminacionForm());
		vista.addObject("mensajePositivo","El fanfiction ha sido eliminado");
		return vista;
	}
	
	@GetMapping("/admin/eliminarUsuario")
	public ModelAndView abrirFormularioEliminarUsuario() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("eliminarUsuario");
		vista.addObject("adminEliminacionForm",new AdminEliminacionForm());
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}
	
	@PostMapping("/admin/confirmarUsuario")
	public ModelAndView buscarUsuarioAEliminar(AdminEliminacionForm formulario, BindingResult resultado) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView();
		vista.addObject("nombreUsuario", usuario.getNombre());
		if(formulario.getId() == null){
			vista.setViewName("eliminarUsuario");
			vista.addObject("mensajeNegativo","El valor del ID no puede quedar vacío");
			vista.addObject("adminEliminacionForm",formulario);
			return vista;
		}
		Usuario eliminar;
		try{
			eliminar = usuarioServicio.buscarUsuarioPorId(formulario.getId());
			if(eliminar.getRol().equals("ADMIN")){
				vista.setViewName("eliminarUsuario");
				vista.addObject("adminEliminacionForm",formulario);
				vista.addObject("mensajeNegativo","No puede eliminar la cuenta de administrador");
				return vista;
			}
			vista.setViewName("confirmarEliminarUsuario");
			vista.addObject("usuario", eliminar);
			return vista;
		}catch(ObjectNotFoundException e){
			vista.setViewName("eliminarUsuario");
			vista.addObject("adminEliminacionForm",formulario);
			vista.addObject("mensajeNegativo","No existe el usuario con el ID señalado");
			return vista;
		}
		
	}
	
	@PostMapping("/admin/eliminarUsuario")
	public ModelAndView eliminarUsuario(@Valid AdminEliminacionForm formulario, BindingResult resultado) throws DaoException, ObjectNotFoundException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		usuarioServicio.eliminarUsuario(usuarioServicio.buscarUsuarioPorId(formulario.getId()));
		ModelAndView vista = new ModelAndView("eliminarUsuario");
		vista.addObject("nombreUsuario", usuario.getNombre());
		vista.addObject("adminEliminacionForm",new AdminEliminacionForm());
		vista.addObject("mensajePositivo","El usuario ha sido eliminado");
		return vista;
	}
}
