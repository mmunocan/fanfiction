package com.ubb.fanfiction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.service.ComentarioServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@Controller
public class ComentarioController {
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	@Autowired
	private ComentarioServicio comentarioServicio;

	@GetMapping("/user/historial/comentariosRecibidos")
	public ModelAndView obtenerListaComentariosRecibidos() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("comentariosRecibidos");
		vista.addObject("comentariosRecibidos", comentarioServicio.obtenerComentariosRecibidos(usuario.getIdUsuario()));
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}

	@GetMapping("/user/historial/comentariosRealizados")
	public ModelAndView obtenerListaComentariosRealizados() throws DaoException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = usuarioServicio.buscarUsuarioPorCorreo(auth.getName());
		ModelAndView vista = new ModelAndView("comentariosRealizados");
		vista.addObject("comentariosRealizados", comentarioServicio.obtenerComentariosRealizados(usuario.getIdUsuario()));
		vista.addObject("nombreUsuario", usuario.getNombre());
		return vista;
	}
	
	

}
