/**
 * 
 */

$(document).ready(function() {
	$('.nav li').mouseover(function() {
        $(this).addClass('active');
    });
	$('.nav li').mouseout(function() {
        $(this).removeClass('active');
    });

});

function getContextPath(){
	var url = window.location.pathname.split('/');
	var path = url[1];
	if(path == 'mmunocan' || path == 'fanfiction'){
		if(path == 'mmunocan'){
			return '/'+path;
		}else{
			var segundo = url[2];
			if(segundo == '' || segundo == 'login' || segundo == 'registro' || segundo == 'acerca' || segundo == 'condiciones'
				|| segundo == 'validar' || segundo == 'recuperar' || segundo == 'fanfiction' || segundo == 'admin' || segundo == 'user'){
				return '/'+path;
			}else{
				return '';
			}
		}
	}else{
		return '';
	}
}
function getFullPath(){
	return 'http://'+window.location.host+getContextPath();
}
function puntuar(val){
	var puntuacion = {};
	puntuacion["valor"] = val;
	puntuacion["idCapitulo"] = $('#idCapitulo').val();
	puntuacion["idUsuario"] = $('#idUsuario').val();
	
	$("#valor").prop("disabled", true);
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: getContextPath()+"/user/puntuar",
        data: JSON.stringify(puntuacion),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
        	$('#respuestaPuntuacion').show();
        	$('#respuestaPuntuacion').html(data.mensaje);
        	$("#valor").prop("disabled", false);
        	
        },
        error: function (e) {
        	$('#respuestaPuntuacion').html(e.mensaje);
        	$("#valor").prop("disabled", false);
        	
        	
        }
    });
}

function comentar(){
	var comentario = {};
	comentario["comentario"] = $('#contenido').val();
	comentario["idCapitulo"] = $('#idCapitulo').val();
	comentario["idUsuario"] = $('#idUsuario').val();
	$('#comentar').prop("disabled", true);
	
	$.ajax({
        type: "POST",
        contentType: "application/json",
        url: getContextPath()+"/user/comentar",
        data: JSON.stringify(comentario),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
        	$('#respuestaComentario').show();
        	$('#respuestaComentario').html(data.mensaje);
        	$("#comentar").prop("disabled", false);
        	
        },
        error: function (e) {
        	$('#respuestaComentario').html(e.mensaje);
        	$("#comentar").prop("disabled", false);
        	
        }
    });
}


function cargarHistorias(idCategoria){
	$('#historiaOriginal').empty();
	if(idCategoria != -1){
		$('#categoria').prop("disabled", true);
		var html = "<option value='-1' selected='selected'>Todas las historias</option>";
		$('#historiaOriginal').parent().find('select').append(html);
		$.ajax({
			type: "GET",
	        contentType: "application/json",
	        url: getContextPath()+"/fanfiction/listHU/"+idCategoria,
	        dataType: "json",
	        cache: false,
	        timeout: 600000,
	        success: function (respuesta) {
	        	var lista = JSON.parse(respuesta.mensaje);
	        	for(var i = 0; i < lista.length; i++){
	        		html = "<option value='"+lista[i].id+"'>"+lista[i].nombre+"</option>";
	        		$('#historiaOriginal').parent().find('select').append(html);
	        	}
	        	
	        	$('#categoria').prop("disabled", false);
	        	$('#historiaOriginal').prop("disabled", false);
	        	$('#historiaOriginal').parent().find('select').selectpicker("refresh");
	        },
	        error: function (jqXHR, exception) {
	            
	            $('#categoria').prop("disabled", false);
	        	$('#historiaOriginal').prop("disabled", true);
	        }
	    });
	}else{
		$('#categoria').prop("disabled", true);
		var html = "<option value='-1' selected='selected'>Todas las historias</option>";
		$('#historiaOriginal').parent().find('select').append(html);
		$('#categoria').prop("disabled", false);
    	$('#historiaOriginal').prop("disabled", false);
    	$('#historiaOriginal').parent().find('select').selectpicker("refresh");
		
	}
	
}