package com.ubb.fanfiction.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.domain.Puntuacion;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.ComentarioForm;
import com.ubb.fanfiction.form.PuntuacionForm;
import com.ubb.fanfiction.model.RespuestaAjax;
import com.ubb.fanfiction.service.CapituloServicio;
import com.ubb.fanfiction.service.ComentarioServicio;
import com.ubb.fanfiction.service.HistoriaOriginalServicio;
import com.ubb.fanfiction.service.PuntuacionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(MockitoJUnitRunner.class)
public class FuncionesRestControllerTests {

	private Integer idUsuario;
	private Integer idCapitulo;
	private Integer idCategoria;
	private Puntuacion puntuacion;
	private Usuario usuario;
	private Capitulo capitulo;
	private PuntuacionForm formularioPuntuacion;
	private Comentario comentario;
	private ComentarioForm formularioComentario;
	private List<HistoriaOriginal> listaHistorias;
	
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private CapituloServicio capituloServicio;
	@Mock
	private PuntuacionServicio puntuacionServicio;
	@Mock
	private ComentarioServicio comentarioServicio;
	@Mock
	private HistoriaOriginalServicio historiaOriginalServicio;
	
	@InjectMocks
	private FuncionesRestController controlador = new FuncionesRestController();
	
	@Before
	public void setUp(){
		idUsuario = 2;
		String nombre = "mmunocan";
		String correo = "mmunocan@gmail.com";
		Date fecha = Calendar.getInstance().getTime();
		idCapitulo = 1;
		idCategoria = 5;
		
		capitulo = new Capitulo();
		capitulo.setIdCapitulo(idCapitulo);
		capitulo.setNumero(1);
		capitulo.setTitulo("Una carta amenazante");
		capitulo.setFechaPublicacion(fecha);
		capitulo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		usuario = new Usuario();
		usuario.setIdUsuario(idUsuario);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(fecha);
		
		comentario = new Comentario();
		comentario.setIdComentario(1);
		comentario.setCapitulo(capitulo);
		comentario.setUsuario(usuario);
		comentario.setFecha(fecha);
		comentario.setContenido("Es una grandiosa historia");
		
		puntuacion = new Puntuacion();
		puntuacion.setIdPuntuacion(1);
		puntuacion.setFecha(fecha);
		puntuacion.setCapitulo(capitulo);
		puntuacion.setUsuario(usuario);
		puntuacion.setValor((byte) 3);
		
		formularioPuntuacion = new PuntuacionForm();
		formularioPuntuacion.setIdCapitulo(idCapitulo);
		formularioPuntuacion.setValor((int) puntuacion.getValor());
		formularioPuntuacion.setIdUsuario(idUsuario);

		formularioComentario = new ComentarioForm();
		formularioComentario.setIdCapitulo(idCapitulo);
		formularioComentario.setComentario("Es una grandiosa historia");
		formularioComentario.setIdUsuario(idUsuario);
		
		Categoria categoria = new Categoria();
		categoria.setIdCategoria(idCategoria);
		categoria.setNombre("Libro");
		
		listaHistorias = new ArrayList<HistoriaOriginal>();
		HistoriaOriginal historia = new HistoriaOriginal();
		historia.setIdHistoriaOriginal(1);
		historia.setNombre("Harry Potter");
		listaHistorias.add(historia);
		historia = new HistoriaOriginal();
		historia.setIdHistoriaOriginal(1);
		historia.setNombre("El quijote de la mancha");
		listaHistorias.add(historia);
		historia = new HistoriaOriginal();
		historia.setIdHistoriaOriginal(1);
		historia.setNombre("El símbolo perdido");
		listaHistorias.add(historia);
		
	}
	
	@Test
	public void entregarComentarioDeFormaExitosa() throws ObjectNotFoundException, DaoException{
		when(usuarioServicio.buscarUsuarioPorId(idUsuario)).thenReturn(usuario);
		when(capituloServicio.obtenerCapitulo(idCapitulo)).thenReturn(capitulo);
		when(comentarioServicio.guardarComentario((Comentario)anyObject())).thenReturn(comentario);
		Errors errors = new BeanPropertyBindingResult(formularioComentario, "comentarioForm");
		int estadoEsperado = 200;
		
		ResponseEntity<?> resultado = controlador.entregarComentario(formularioComentario,errors);
		int estadoRecibido = resultado.getStatusCodeValue();
		RespuestaAjax respuesta = (RespuestaAjax) resultado.getBody();
		
		verify(comentarioServicio).guardarComentario((Comentario)anyObject());
		assertNotNull(resultado);
		assertNotNull(respuesta);
		assertNotNull(respuesta.getMensaje());
		assertEquals(estadoEsperado, estadoRecibido);
	}
	
	@Test
	public void errorEnEnvioDeComentario() throws ObjectNotFoundException, DaoException{
		Errors errors = new BeanPropertyBindingResult(formularioComentario, "comentarioForm");
		errors.reject("commentario");
		int estadoEsperado = 400;
		
		ResponseEntity<?> resultado = controlador.entregarComentario(formularioComentario,errors);
		int estadoRecibido = resultado.getStatusCodeValue();
		RespuestaAjax respuesta = (RespuestaAjax) resultado.getBody();
		
		assertNotNull(resultado);
		assertNotNull(respuesta);
		assertNotNull(respuesta.getMensaje());
		assertEquals(estadoEsperado, estadoRecibido);
	}
	
	@Test
	public void entregarPuntuacionNuevaDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		when(usuarioServicio.buscarUsuarioPorId(idUsuario)).thenReturn(usuario);
		when(capituloServicio.obtenerCapitulo(idCapitulo)).thenReturn(capitulo);
		doThrow(ObjectNotFoundException.class).when(puntuacionServicio).obtenerPuntuacion(idCapitulo,idUsuario);
		Errors errors = new BeanPropertyBindingResult(formularioPuntuacion, "puntuacionForm");
		int estadoEsperado = 200;
		
		ResponseEntity<?> resultado = controlador.entregarPuntuacion(formularioPuntuacion,errors);
		int estadoRecibido = resultado.getStatusCodeValue();
		RespuestaAjax respuesta = (RespuestaAjax) resultado.getBody();
		
		verify(puntuacionServicio).guardarPuntuacion((Puntuacion)anyObject());
		assertNotNull(resultado);
		assertNotNull(respuesta);
		assertNotNull(respuesta.getMensaje());
		assertEquals(estadoEsperado, estadoRecibido);
	}
	
	@Test
	public void modificarPuntuacionEntregadaDeFormaExitosa() throws ObjectNotFoundException, DaoException{
		when(usuarioServicio.buscarUsuarioPorId(idUsuario)).thenReturn(usuario);
		when(capituloServicio.obtenerCapitulo(idCapitulo)).thenReturn(capitulo);
		when(puntuacionServicio.obtenerPuntuacion(idCapitulo, idUsuario)).thenReturn(puntuacion);
		Errors errors = new BeanPropertyBindingResult(formularioPuntuacion, "puntuacionForm");
		int estadoEsperado = 200;
		
		ResponseEntity<?> resultado = controlador.entregarPuntuacion(formularioPuntuacion,errors);
		int estadoRecibido = resultado.getStatusCodeValue();
		RespuestaAjax respuesta = (RespuestaAjax) resultado.getBody();
		
		assertNotNull(resultado);
		assertNotNull(respuesta);
		assertNotNull(respuesta.getMensaje());
		assertEquals(estadoEsperado, estadoRecibido);
	}
	
	@Test
	public void envioFallidoDePuntuacion() throws ObjectNotFoundException, DaoException{
		Errors errors = new BeanPropertyBindingResult(formularioPuntuacion, "puntuacionForm");
		errors.reject("valor");
		int estadoEsperado = 400;
		
		ResponseEntity<?> resultado = controlador.entregarPuntuacion(formularioPuntuacion,errors);
		int estadoRecibido = resultado.getStatusCodeValue();
		RespuestaAjax respuesta = (RespuestaAjax) resultado.getBody();
		
		assertNotNull(resultado);
		assertNotNull(respuesta);
		assertNotNull(respuesta.getMensaje());
		assertEquals(estadoEsperado, estadoRecibido);
	}
	
	@Test
	public void entregarListaHistoriasOriginalesPorCategoriaDeFormaExitosa() throws DaoException{
		when(historiaOriginalServicio.buscarHistoriaPorCategoria(idCategoria)).thenReturn(listaHistorias);
		int estadoEsperado = 200;
		
		ResponseEntity<?> resultado = controlador.buscarHistoriasOriginalesDeCategoria(idCategoria);
		int estadoRecibido = resultado.getStatusCodeValue();
		RespuestaAjax respuesta = (RespuestaAjax) resultado.getBody();
		
		assertNotNull(resultado);
		assertNotNull(respuesta);
		assertNotNull(respuesta.getMensaje());
		assertEquals(estadoEsperado, estadoRecibido);
	}
	
	@Test
	public void entregarListaHistoriasOriginalesPorCategoriaDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(DaoException.class).when(historiaOriginalServicio).buscarHistoriaPorCategoria(idCategoria);
		int estadoEsperado = 400;
		
		ResponseEntity<?> resultado = controlador.buscarHistoriasOriginalesDeCategoria(idCategoria);
		int estadoRecibido = resultado.getStatusCodeValue();
		
		assertNotNull(resultado);
		assertNotNull(((RespuestaAjax)resultado.getBody()).getMensaje());
		assertEquals(estadoEsperado, estadoRecibido);
	}
	
}
