package com.ubb.fanfiction.controller;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.AdminEliminacionForm;
import com.ubb.fanfiction.form.HistoriaOriginalForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.CategoriaServicio;
import com.ubb.fanfiction.service.ComentarioServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.HistoriaOriginalServicio;
import com.ubb.fanfiction.service.PuntuacionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class AdministradorControllerTests {
	private AdminEliminacionForm adminEliminacionForm;
	private BindingResult resultado;
	private Usuario administrador;
	private String correo;
	private String nombre;
	private HistoriaOriginalForm historiaOriginalForm;
	private List<Categoria> listaCategoria;
	private BindingResult resultadoHistoria;
	private Integer idCategoria;
	private Categoria cat1;
	private Comentario comentario;
	private Integer idFanfiction;
	private Fanfiction fanfiction;
	private Usuario usuario;
	
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private ComentarioServicio comentarioServicio;
	
	
	@Mock
	private PuntuacionServicio puntuacionServicio;
	@Mock
	private CategoriaServicio categoriaServicio;
	@Mock
	private HistoriaOriginalServicio historiaOriginalServicio;

	@Mock
	private FanfictionServicio fanfictionServicio;
	
	
	@InjectMocks
	private AdministradorController controlador = new AdministradorController();
	
	@Before
	public void setUp(){
		Integer idAdministrador = 1;
		correo = "admin@admin.cl";
		idCategoria = 1;
		nombre = "admin";
		idFanfiction = 1;
		
		administrador = new Usuario();
		administrador.setIdUsuario(idAdministrador);
		administrador.setNombre(nombre);
		administrador.setCorreo(correo);
		administrador.setClave("c14v3");
		administrador.setRol("ADMIN");
		administrador.setEstado(true);
		administrador.setFechaCreacion(Calendar.getInstance().getTime());
				
		Capitulo capitulo = new Capitulo();
		capitulo.setIdCapitulo(1);
		capitulo.setNumero(1);
		capitulo.setTitulo("Una carta amenazante");
		capitulo.setFechaPublicacion(Calendar.getInstance().getTime());
		capitulo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		historiaOriginalForm = new HistoriaOriginalForm();
		historiaOriginalForm.setCategoria(1);
		historiaOriginalForm.setNombre("The Loud House");
		resultadoHistoria = new BeanPropertyBindingResult(historiaOriginalForm, "historiaOriginalForm");
		
		cat1 = new Categoria();
		cat1.setIdCategoria(idCategoria);
		cat1.setNombre("Libro");
		
		Categoria cat2 = new Categoria();
		cat2.setIdCategoria(2);
		cat2.setNombre("Película");
		
		listaCategoria = new ArrayList<Categoria>();
		listaCategoria.add(cat1);
		listaCategoria.add(cat2);
		
		comentario = new Comentario();
		comentario.setIdComentario(1);
		comentario.setFecha(Calendar.getInstance().getTime());
		comentario.setUsuario(new Usuario());
		comentario.setCapitulo(capitulo);
		comentario.setContenido("Es un muy buen episodio. Sigue así y muchas felicidades");
		
		adminEliminacionForm = new AdminEliminacionForm();
		resultado = new BeanPropertyBindingResult(adminEliminacionForm, "adminEliminacionForm");
		
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(Calendar.getInstance().getTime());
		fanfiction.setFechaActualizacion(Calendar.getInstance().getTime());
		fanfiction.setEstado("Incompleto");
		
		usuario = new Usuario();
		usuario.setIdUsuario(2);
		usuario.setNombre("mmunocan");
		usuario.setCorreo("mmunocan@gmail.com");
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(Calendar.getInstance().getTime());
		
	}

	
	@Test
	public void cargarFormularioAgregarNuevaHistoriaOriginalExitosa() throws DaoException{
		chargeAuthentication();
		when(categoriaServicio.obtenerListaCategorias()).thenReturn(listaCategoria);
		String vistaEsperada = "nuevaHistoriaOriginal";
		
		ModelAndView vista = controlador.abrirFormularioAgregarHistoriaOriginal();
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Categoria> listaRecibida = (List<Categoria>) vista.getModelMap().get("categorias");
		HistoriaOriginalForm formulario = (HistoriaOriginalForm) vista.getModelMap().get("historiaOriginalForm");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(listaCategoria,listaRecibida);
		assertNotNull(formulario);
		assertNotNull(nombre, nombreRecibido);
	}

	@Test
	public void guardarHistoriaOriginalDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		when(categoriaServicio.obtenerListaCategorias()).thenReturn(listaCategoria);
		when(categoriaServicio.obtenerCategoriaPorId(idCategoria)).thenReturn(cat1);
		String vistaEsperada = "nuevaHistoriaOriginal";
		
		ModelAndView vista = controlador.guardarHistoriaOriginal(historiaOriginalForm, resultadoHistoria);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Categoria> listaRecibida = (List<Categoria>) vista.getModelMap().get("categorias");
		HistoriaOriginalForm formulario = (HistoriaOriginalForm) vista.getModelMap().get("historiaOriginalForm");
		String mensaje = (String) vista.getModelMap().get("mensajeExito");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(listaCategoria,listaRecibida);
		assertNotNull(formulario);
		assertNotNull(mensaje);
		assertEquals(nombre, nombreRecibido);
		verify(historiaOriginalServicio).guardarHistoriaOriginal((HistoriaOriginal)anyObject());
	}
	
	@Test
	public void guardarHistoriaOriginalDeFormaFallidaPorErrorEnFormulario() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		resultadoHistoria.rejectValue("nombre", "error.nombre");
		when(categoriaServicio.obtenerListaCategorias()).thenReturn(listaCategoria);
		String vistaEsperada = "nuevaHistoriaOriginal";
		
		ModelAndView vista = controlador.guardarHistoriaOriginal(historiaOriginalForm, resultadoHistoria);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Categoria> listaRecibida = (List<Categoria>) vista.getModelMap().get("categorias");
		HistoriaOriginalForm formulario = (HistoriaOriginalForm) vista.getModelMap().get("historiaOriginalForm");
		String mensaje = (String) vista.getModelMap().get("mensajeExito");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(listaCategoria,listaRecibida);
		assertNotNull(formulario);
		assertNull(mensaje);
		assertEquals(nombre, nombreRecibido);
	}
	
	@Test
	public void abrirFormularioEliminarComentarioDeFormaExitosa() throws DaoException{
		chargeAuthentication();
		String vistaEsperada = "eliminarComentario";
		
		ModelAndView vista = controlador.abrirFormularioEliminarComentario();
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
	}
	
	@Test
	public void obtenerDatosDelComentarioDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		adminEliminacionForm.setId(1);
		chargeAuthentication();
		when(comentarioServicio.buscarComentarioPorId(adminEliminacionForm.getId())).thenReturn(comentario);
		String vistaEsperada = "confirmarEliminarComentario";
		
		ModelAndView vista = controlador.buscarComentarioAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		Comentario resultado = (Comentario) vista.getModelMap().get("comentario");
		
		assertNotNull(vista);
		assertNotNull(resultado);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
	}
	
	@Test
	public void busquedaDelComentarioFallidaPorNoExistirComentario() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		adminEliminacionForm.setId(comentario.getIdComentario());
		doThrow(ObjectNotFoundException.class).when(comentarioServicio).buscarComentarioPorId(comentario.getIdComentario());
		String vistaEsperada = "eliminarComentario";
		
		ModelAndView vista = controlador.buscarComentarioAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajeNegativo");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	@Test
	public void busquedaDeComentarioFallidaPorVenirIdVacio() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		String vistaEsperada = "eliminarComentario";
		
		ModelAndView vista = controlador.buscarComentarioAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajeNegativo");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	@Test
	public void eliminarComentarioDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		adminEliminacionForm.setId(comentario.getIdComentario());
		when(comentarioServicio.buscarComentarioPorId(comentario.getIdComentario())).thenReturn(comentario);
		String vistaEsperada = "eliminarComentario";
		
		ModelAndView vista = controlador.eliminarComentario(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajePositivo");
		
		verify(comentarioServicio).eliminarComentario(comentario);
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	@Test
	public void abrirFormularioEliminarFanfictionDeFormaExitosa() throws DaoException{
		chargeAuthentication();
		String vistaEsperada = "eliminarFanfiction";
		
		ModelAndView vista = controlador.abrirFormularioEliminarFanfiction();
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
	}
	
	@Test
	public void obtenerDatosDelFanfictionDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		adminEliminacionForm.setId(1);
		chargeAuthentication();
		when(fanfictionServicio.buscarFanfictionPorId(adminEliminacionForm.getId())).thenReturn(fanfiction);
		when(comentarioServicio.contarComentariosPorFanfiction(adminEliminacionForm.getId())).thenReturn(anyLong());
		when(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(adminEliminacionForm.getId())).thenReturn(3.5);
		String vistaEsperada = "confirmarEliminarFanficion";
		
		ModelAndView vista = controlador.buscarFanfictionAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		Publicacion resultado = (Publicacion) vista.getModelMap().get("publicacion");
		
		assertNotNull(vista);
		assertNotNull(resultado);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
	}
	
	@Test
	public void busquedaDeFanfictionFallidaPorNoExistirFanfiction() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		adminEliminacionForm.setId(idFanfiction);
		doThrow(ObjectNotFoundException.class).when(fanfictionServicio).buscarFanfictionPorId(idFanfiction);
		String vistaEsperada = "eliminarFanfiction";
		
		ModelAndView vista = controlador.buscarFanfictionAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajeNegativo");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	@Test
	public void busquedaDeFanfictionFallidaPorIdVacio() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		String vistaEsperada = "eliminarFanfiction";
		
		ModelAndView vista = controlador.buscarFanfictionAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajeNegativo");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	@Test
	public void eliminarFanfictionDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		adminEliminacionForm.setId(idFanfiction);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		String vistaEsperada = "eliminarFanfiction";
		
		ModelAndView vista = controlador.eliminarFanfiction(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajePositivo");
		
		verify(fanfictionServicio).eliminarFanfiction(fanfiction);
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	@Test
	public void abrirFormularioEliminarUsuarioDeFormaExitosa() throws DaoException{
		chargeAuthentication();
		String vistaEsperada = "eliminarUsuario";
		
		ModelAndView vista = controlador.abrirFormularioEliminarUsuario();
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
	}
	
	@Test
	public void obtenerDatosDelUsuarioDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		adminEliminacionForm.setId(3);
		chargeAuthentication();
		when(usuarioServicio.buscarUsuarioPorId(adminEliminacionForm.getId())).thenReturn(usuario);
		String vistaEsperada = "confirmarEliminarUsuario";
		
		ModelAndView vista = controlador.buscarUsuarioAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		Usuario resultado = (Usuario) vista.getModelMap().get("usuario");
		
		assertNotNull(vista);
		assertNotNull(resultado);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
	}
	
	@Test
	public void busquedaDeUsuarioFallidaPorNoExistirUsuario() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		adminEliminacionForm.setId(usuario.getIdUsuario());
		doThrow(ObjectNotFoundException.class).when(usuarioServicio).buscarUsuarioPorId(usuario.getIdUsuario());
		String vistaEsperada = "eliminarUsuario";
		
		ModelAndView vista = controlador.buscarUsuarioAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajeNegativo");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	@Test
	public void busquedaDeUsuarioFallidaPorIdVacio() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		String vistaEsperada = "eliminarUsuario";
		
		ModelAndView vista = controlador.buscarUsuarioAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajeNegativo");
		
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	@Test
	public void busquedaDeUsuarioFallidaPorSerAdministrador() throws ObjectNotFoundException, DaoException{
		adminEliminacionForm.setId(1);
		usuario.setRol("ADMIN");
		chargeAuthentication();
		when(usuarioServicio.buscarUsuarioPorId(adminEliminacionForm.getId())).thenReturn(usuario);
		String vistaEsperada = "eliminarUsuario";
		
		ModelAndView vista = controlador.buscarUsuarioAEliminar(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajeNegativo");
		
		assertNotNull(vista);
		assertNotNull(resultado);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
		
	@Test
	public void eliminarUsuarioDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication();
		adminEliminacionForm.setId(usuario.getIdUsuario());
		when(usuarioServicio.buscarUsuarioPorId(usuario.getIdUsuario())).thenReturn(usuario);
		String vistaEsperada = "eliminarUsuario";
		
		ModelAndView vista = controlador.eliminarUsuario(adminEliminacionForm,resultado);
		String vistaRecibida = vista.getViewName();
		AdminEliminacionForm formularioRecibido = (AdminEliminacionForm) vista.getModel().get("adminEliminacionForm");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensaje = (String) vista.getModelMap().get("mensajePositivo");
		
		verify(usuarioServicio).eliminarUsuario(usuario);
		assertNotNull(vista);
		assertNotNull(formularioRecibido);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensaje);
	}
	
	private void chargeAuthentication() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(administrador);
	}
	
	
	
}
