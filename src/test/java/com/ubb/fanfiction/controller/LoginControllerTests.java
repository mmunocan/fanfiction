package com.ubb.fanfiction.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.Calendar;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class LoginControllerTests {
	private Usuario usuario;
	private Usuario administrador;
	private Integer idUsuario;
	private String correoUsuario;
	private String nombreUsuario;
	private Integer idAdmin;
	private String correoAdmin;
	private String nombreAdmin;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	
	@InjectMocks
	private LoginController controlador = new LoginController();
	
	@Before
	public void setUp(){
		idUsuario = 2;
		correoUsuario = "mmunocan@gmail.com";
		nombreUsuario = "mmunocan";
		usuario = new Usuario();
		usuario.setIdUsuario(idUsuario);
		usuario.setNombre(nombreUsuario);
		usuario.setCorreo(correoUsuario);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(Calendar.getInstance().getTime());
		
		idAdmin = 1;
		correoAdmin = "admin@admin.com";
		nombreAdmin = "admin";
		administrador = new Usuario();
		administrador.setIdUsuario(idAdmin);
		administrador.setNombre(nombreAdmin);
		administrador.setCorreo(correoAdmin);
		administrador.setClave("c14v3");
		administrador.setRol("ADMIN");
		administrador.setEstado(true);
		administrador.setFechaCreacion(Calendar.getInstance().getTime());
	}
	
	@Test
	public void redireccionAlIndex() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(null);
		String vistaEsperada = "index";
		
		ModelAndView vista = controlador.irAlIndex();
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		
	}
	
	@Test
	public void redireccionAlIndexConSesionIniciada() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(usuario);
		String vistaEsperada = "index";
		
		ModelAndView vista = controlador.irAlIndex();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombreUsuario,nombreRecibido);
		
		
	}
	
	@Test
	public void redireccionarATerminosYCondiciones() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(null);
		String vistaEsperada = "terminosYCondiciones";
		
		ModelAndView vista = controlador.verTerminosYCondiciones();
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void redireccionarATerminosYCondicionesConSesionIniciada() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(usuario);
		String vistaEsperada = "terminosYCondiciones";
		
		ModelAndView vista = controlador.verTerminosYCondiciones();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombreUsuario,nombreRecibido);
		
		
	}
	
	@Test
	public void redireccionarAAcercaDe() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(null);
		String vistaEsperada = "acercaDe";
		
		ModelAndView vista = controlador.acercaDe();
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void redireccionarAAcercaDeSesionIniciada() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(usuario);
		String vistaEsperada = "acercaDe";
		
		ModelAndView vista = controlador.acercaDe();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombreUsuario,nombreRecibido);
	}
	
	@Test
	public void redireccionError() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(null);
		String vistaEsperada = "403";
		
		ModelAndView vista = controlador.error();
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		
	}
	
	@Test
	public void redireccionErrorConSesionIniciada() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(usuario);
		String vistaEsperada = "403";
		
		ModelAndView vista = controlador.error();
		Integer idRecibido = (Integer) vista.getModelMap().get("idUsuario");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombreUsuario,nombreRecibido);
		assertNull(idRecibido);
		
	}
	
	@Test
	public void redireccionAlLogin(){
		String vistaEsperada = "login";
		
		String vistaRecibida = controlador.irAlLogin();
		
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void loginUsuarioExitoso()throws ObjectNotFoundException, DaoException {
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(usuario);
		String vistaEsperada = "redirect:/user/home";
		
		String vistaRecibida = controlador.inicioSesion();
		
		assertEquals(vistaEsperada,vistaRecibida);
	}
	
	@Test
	public void loginAdministradorExitoso()throws ObjectNotFoundException, DaoException {
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correoAdmin);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoAdmin)).thenReturn(administrador);
		String vistaEsperada = "redirect:/admin/home";
		
		String vistaRecibida = controlador.inicioSesion();
	
		assertEquals(vistaEsperada,vistaRecibida);
	}
	
	
	@Test
	public void cargarPaginaPrincipalUsuarioExitoso()throws ObjectNotFoundException, DaoException {
		chargeAuthentication();
		when(auth.getName()).thenReturn(correoUsuario);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoUsuario)).thenReturn(usuario);
		String vistaEsperada = "userhome";
		
		ModelAndView vista = controlador.cargarPaginaPrincipal();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombreUsuario,nombreRecibido);
		
	}
	
	@Test
	public void cargarPaginaPrincipalAdministradorExitoso() throws ObjectNotFoundException, DaoException{
		chargeAuthentication();
		when(auth.getName()).thenReturn(correoAdmin);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoAdmin)).thenReturn(administrador);String vistaEsperada = "adminhome";
		
		ModelAndView vista = controlador.cargarPaginaPrincipal();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombreAdmin,nombreRecibido);
		
	}
	
	private void chargeAuthentication(){
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
	}
}
