package com.ubb.fanfiction.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.form.BusquedaForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.CategoriaServicio;
import com.ubb.fanfiction.service.ClasificacionParentalServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.GeneroServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class BusquedaControllerTests {
	private Usuario usuario;
	private String nombre;
	private String correo;
	private BusquedaForm formulario;
	private BindingResult resultado;
	private List<ClasificacionParental> listaClasificacionesParentales;
	private List<Genero> listaGeneros;
	private List<Categoria> listaCategorias;
	private List<Publicacion> listaFanfiction;
	
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	
	
	@Mock
	private GeneroServicio generoServicio;
	@Mock
	private ClasificacionParentalServicio clasificacionParentalServicio;
	@Mock
	private CategoriaServicio categoriaServicio;
	@Mock
	private FanfictionServicio fanfictionServicio;
	
	@InjectMocks
	private BusquedaController controlador = new BusquedaController();
	
	@Before
	public void setUp(){
		Integer idUsuario = 2;
		nombre = "mmunocan";
		correo = "mmunocan@gmail.com";
		Date fecha = Calendar.getInstance().getTime();
		
		
		usuario = new Usuario();
		usuario.setIdUsuario(idUsuario);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(fecha);
		
		formulario = new BusquedaForm();
		formulario.setTitulo("Una Historia Infinita");
		formulario.setAutor("mmunocan");
		formulario.setInicioCreacion("2017-10-28");
		formulario.setFinCreacion("2017-10-28");
		formulario.setInicioPublicacion("2017-10-28");
		formulario.setFinPublicacion("2017-10-28");
		formulario.setComentarioMinimo(1);
		formulario.setComentarioMaximo(3);
		formulario.setCapituloMinimo(1);
		formulario.setCapituloMaximo(3);
		formulario.setPuntuacionMinimo(1);
		formulario.setPuntuacionMaximo(3);
		formulario.setGenero(-1);
		formulario.setCategoria(-1);
		formulario.setClasificacionParental(-1);
		formulario.setHistoriaOriginal(-1);
		formulario.setEstado("Terminado");
		formulario.setTipo("Normal");
		formulario.setAtributoOrden(0);
		formulario.setOrdenBusqueda(0);
		
		resultado = new BeanPropertyBindingResult(formulario, "busquedaForm");
		
		Categoria cat1 = new Categoria("Libro");
		Categoria cat2 = new Categoria("Película");
		Genero genero1 = new Genero("Drama");
		Genero genero2 = new Genero("Comedia");		
		ClasificacionParental clas1 = new ClasificacionParental("K (5+)");
		ClasificacionParental clas2 = new ClasificacionParental("K+ (9+)");
		
		
		listaGeneros = new ArrayList<Genero>();
		listaGeneros.add(genero1);
		listaGeneros.add(genero2);
		
		listaClasificacionesParentales = new ArrayList<ClasificacionParental>();
		listaClasificacionesParentales.add(clas1);
		listaClasificacionesParentales.add(clas2);
		
		listaCategorias = new ArrayList<Categoria>();
		listaCategorias.add(cat1);
		listaCategorias.add(cat2);
		
		Fanfiction fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(1);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(Calendar.getInstance().getTime());
		fanfiction.setFechaActualizacion(Calendar.getInstance().getTime());
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(usuario);
		fanfiction.setClasificacionParental(clas1);
		fanfiction.setGeneros(new HashSet<Genero>());
		fanfiction.setHistoriaOriginals(new HashSet<HistoriaOriginal>());
		fanfiction.setCapitulos(new HashSet<Capitulo>());
		
		listaFanfiction = new ArrayList<Publicacion>();
		Publicacion fanfictionCompleto = new Publicacion();
		fanfictionCompleto.setFanfiction(fanfiction);
		fanfictionCompleto.setCantidadComentarios((long) 123);
		fanfictionCompleto.setPromedioPuntuacion(12.1);
		listaFanfiction.add(fanfictionCompleto);
	}
	
	@Test
	public void mostrarFormularioBuscarFanfictionAUsuarioNoRegistradoDeFormaExitosa() throws DaoException{
		notChargeAuthentication();
		when(clasificacionParentalServicio.obtenerClasificacionesParentales()).thenReturn(listaClasificacionesParentales);
		when(generoServicio.obtenerListaGeneros()).thenReturn(listaGeneros);
		when(categoriaServicio.obtenerListaCategorias()).thenReturn(listaCategorias);
		String vistaEsperada = "buscarFanfiction";
		
		ModelAndView vista = controlador.buscarFanfiction();
		@SuppressWarnings("unchecked")
		List<ClasificacionParental> clasificacionesParentales = (List<ClasificacionParental>) vista.getModelMap().get("clasificacionesParentales");
		@SuppressWarnings("unchecked")
		List<Genero> generos = (List<Genero>) vista.getModelMap().get("generos");
		@SuppressWarnings("unchecked")
		List<Categoria> categorias = (List<Categoria>) vista.getModelMap().get("categorias");
		String vistaRecibida = vista.getViewName();
		BusquedaForm formulario = (BusquedaForm) vista.getModelMap().get("busquedaForm");
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(listaClasificacionesParentales, clasificacionesParentales);
		assertEquals(listaGeneros, generos);
		assertEquals(listaCategorias, categorias);
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void mostrarFormularioBuscarFanfictionAUsuarioRegistradoDeFormaExitosa() throws DaoException{
		chargeAuthentication();
		when(clasificacionParentalServicio.obtenerClasificacionesParentales()).thenReturn(listaClasificacionesParentales);
		when(generoServicio.obtenerListaGeneros()).thenReturn(listaGeneros);
		when(categoriaServicio.obtenerListaCategorias()).thenReturn(listaCategorias);
		String vistaEsperada = "buscarFanfiction";
		
		ModelAndView vista = controlador.buscarFanfiction();
		@SuppressWarnings("unchecked")
		List<ClasificacionParental> clasificacionesParentales = (List<ClasificacionParental>) vista.getModelMap().get("clasificacionesParentales");
		@SuppressWarnings("unchecked")
		List<Genero> generos = (List<Genero>) vista.getModelMap().get("generos");
		@SuppressWarnings("unchecked")
		List<Categoria> categorias = (List<Categoria>) vista.getModelMap().get("categorias");
		String vistaRecibida = vista.getViewName();
		BusquedaForm formulario = (BusquedaForm) vista.getModelMap().get("busquedaForm");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(listaClasificacionesParentales, clasificacionesParentales);
		assertEquals(listaGeneros, generos);
		assertEquals(listaCategorias, categorias);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		
	}
	
	@Test
	public void mostrarResultadosBusquedaFanfictionDeFormaExitosaAUsuarioNoRegistrado() throws DaoException{
		String titulo = formulario.getTitulo();
		String autor = formulario.getAutor();
		String inicioCreacion = formulario.getInicioCreacion();
		String finCreacion = formulario.getInicioCreacion();
		String inicioPublicacion = formulario.getInicioPublicacion();
		String finPublicacion = formulario.getFinPublicacion();
		Integer comentarioMinimo = formulario.getComentarioMinimo();
		Integer comentarioMaximo = formulario.getComentarioMaximo();
		Integer capituloMinimo = formulario.getCapituloMinimo();
		Integer capituloMaximo = formulario.getCapituloMaximo();
		Integer puntuacionMinima = formulario.getPuntuacionMinimo();
		Integer puntuacionMaxima = formulario.getPuntuacionMaximo();
		Integer genero = null;
		Integer categoria = null;
		Integer historiaOriginal = null;
		Integer clasificacionParental = null;
		String estado = formulario.getEstado();
		String tipo = formulario.getTipo();
		int atributoOrden = formulario.getAtributoOrden();
		int ordenBusqueda = formulario.getOrdenBusqueda();
		notChargeAuthentication();
		when(fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, inicioPublicacion, finPublicacion, 
				comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo,	puntuacionMinima, puntuacionMaxima, genero, 
				categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda)).thenReturn(listaFanfiction);
		String vistaEsperada = "resultadoBusqueda";
		
		ModelAndView vista = controlador.buscarFanfiction(formulario,resultado);
		@SuppressWarnings("unchecked")
		List<Publicacion> resultado = (List<Publicacion>)vista.getModel().get("fanfictions");
		BusquedaForm parametros = (BusquedaForm) vista.getModelMap().get("busquedaForm");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(listaFanfiction,resultado);
		assertNotNull(parametros);
		
	}
	
	@Test
	public void mostrarResultadosBusquedaFanfictionDeFormaExitosaAUsuarioRegistrado() throws DaoException{
		String titulo = formulario.getTitulo();
		String autor = formulario.getAutor();
		String inicioCreacion = formulario.getInicioCreacion();
		String finCreacion = formulario.getInicioCreacion();
		String inicioPublicacion = formulario.getInicioPublicacion();
		String finPublicacion = formulario.getFinPublicacion();
		Integer comentarioMinimo = formulario.getComentarioMinimo();
		Integer comentarioMaximo = formulario.getComentarioMaximo();
		Integer capituloMinimo = formulario.getCapituloMinimo();
		Integer capituloMaximo = formulario.getCapituloMaximo();
		Integer puntuacionMinima = formulario.getPuntuacionMinimo();
		Integer puntuacionMaxima = formulario.getPuntuacionMaximo();
		Integer genero = null;
		Integer categoria = null;
		Integer historiaOriginal = null;
		Integer clasificacionParental = null;
		String estado = formulario.getEstado();
		String tipo = formulario.getTipo();
		int atributoOrden = formulario.getAtributoOrden();
		int ordenBusqueda = formulario.getOrdenBusqueda();
		chargeAuthentication();
		when(fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, inicioPublicacion, finPublicacion, 
				comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo,	puntuacionMinima, puntuacionMaxima, genero, 
				categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda)).thenReturn(listaFanfiction);
		String vistaEsperada = "resultadoBusqueda";
		
		ModelAndView vista = controlador.buscarFanfiction(formulario,resultado);
		@SuppressWarnings("unchecked")
		List<Publicacion> resultado = (List<Publicacion>)vista.getModel().get("fanfictions");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String vistaRecibida = vista.getViewName();
		BusquedaForm parametros = (BusquedaForm) vista.getModelMap().get("busquedaForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(listaFanfiction,resultado);
		assertEquals(nombre,nombreRecibido);
		assertNotNull(parametros);
		
	}
	
	@Test
	public void ordenarResultadoDeFormaExitosa() throws DaoException{
		formulario.setAtributoOrden(BusquedaForm.CAPITULOS);
		formulario.setOrdenBusqueda(BusquedaForm.ASC);
		String titulo = formulario.getTitulo();
		String autor = formulario.getAutor();
		String inicioCreacion = formulario.getInicioCreacion();
		String finCreacion = formulario.getInicioCreacion();
		String inicioPublicacion = formulario.getInicioPublicacion();
		String finPublicacion = formulario.getFinPublicacion();
		Integer comentarioMinimo = formulario.getComentarioMinimo();
		Integer comentarioMaximo = formulario.getComentarioMaximo();
		Integer capituloMinimo = formulario.getCapituloMinimo();
		Integer capituloMaximo = formulario.getCapituloMaximo();
		Integer puntuacionMinima = formulario.getPuntuacionMinimo();
		Integer puntuacionMaxima = formulario.getPuntuacionMaximo();
		Integer genero = null;
		Integer categoria = null;
		Integer historiaOriginal = null;
		Integer clasificacionParental = null;
		String estado = formulario.getEstado();
		String tipo = formulario.getTipo();
		int atributoOrden = formulario.getAtributoOrden();
		int ordenBusqueda = formulario.getOrdenBusqueda();
		notChargeAuthentication();
		when(fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, inicioPublicacion, finPublicacion, 
				comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo,	puntuacionMinima, puntuacionMaxima, genero, 
				categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda)).thenReturn(listaFanfiction);
		String vistaEsperada = "resultadoBusqueda";
		
		ModelAndView vista = controlador.buscarFanfiction(formulario,resultado);
		@SuppressWarnings("unchecked")
		List<Publicacion> resultado = (List<Publicacion>)vista.getModel().get("fanfictions");
		BusquedaForm parametros = (BusquedaForm) vista.getModelMap().get("busquedaForm");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(listaFanfiction,resultado);
		assertNotNull(parametros);
	}
	
	
	private void chargeAuthentication() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
	}
	
	private void notChargeAuthentication() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(null);
	}
	
}
