package com.ubb.fanfiction.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.EmailException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.RegistroForm;
import com.ubb.fanfiction.service.MensajeCorreoServicio;
import com.ubb.fanfiction.service.UsuarioServicio;


@RunWith(MockitoJUnitRunner.class)
public class RegistroControllerTests {
	private RegistroForm formulario;
	private BindingResult resultado;
	private Usuario usuario;
	private String nombre;
	private String correo;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private MensajeCorreoServicio mensajeCorreoServicio;
	
	@InjectMocks
	private RegistroController controlador = new RegistroController();
	
	@Before
	public void setUp(){
		nombre = "mmunocan";
		correo = "mmunocan@gmail.com";
		String clave = "cl4v3";
		String clave2 = "cl4v3";
		String descripcion = "¡Bienvenidos sean todos a mi cuenta de usuario!";
		formulario = new RegistroForm();
		formulario.setNombre(nombre);
		formulario.setCorreo(correo);
		formulario.setClave(clave);
		formulario.setClave2(clave2);
		formulario.setDescripcion(descripcion);
		formulario.setPath("www.zonadefics.cl");
		
		resultado = new BeanPropertyBindingResult(formulario, "registroForm");
		
		usuario = new Usuario();
		usuario.setIdUsuario(1);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave(clave);
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(Calendar.getInstance().getTime());
		
		
	}
	
	@Test
	public void redireccionAlFormularioRegistro(){
		ModelAndView vista = controlador.irAlRegistro();
		String vistaEsperada = "registro";
		
		String vistaRecibida = vista.getViewName();
		RegistroForm registro = (RegistroForm) vista.getModelMap().get("registroForm");
		
		assertNotNull(vista);
		assertNotNull(registro);
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void registroExitosoDeUsuario() throws DaoException, EmailException{
		Integer id = usuario.getIdUsuario();
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.guardarUsuario((Usuario)anyObject())).thenReturn(id);
		when(usuarioServicio.generarToken()).thenReturn(token);
		String vistaEsperada = "registro";
		
		ModelAndView vista = controlador.registrarUsuario(formulario, resultado);
		String vistaRecibida = vista.getViewName();
		String mensaje = (String) vista.getModelMap().get("mensajeExito");
		
		verify(usuarioServicio).guardarUsuario((Usuario)anyObject());
		verify(mensajeCorreoServicio).enviarCorreoActivacionCuenta(formulario.getPath(),id,token, formulario.getCorreo());
		assertNotNull(mensaje);
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void registroFallidoPorErrorEnEnvioDeCorreo() throws DaoException, EmailException{
		Integer id = usuario.getIdUsuario();
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.guardarUsuario((Usuario)anyObject())).thenReturn(id);
		doThrow(EmailException.class).when(mensajeCorreoServicio).enviarCorreoActivacionCuenta(formulario.getPath(),id,token, formulario.getCorreo());
		when(usuarioServicio.generarToken()).thenReturn(token);
		String vistaEsperada = "registro";
		
		ModelAndView vista = controlador.registrarUsuario(formulario, resultado);
		String vistaRecibida = vista.getViewName();
		String mensaje = (String) vista.getModelMap().get("mensajeError");
		
		assertNotNull(mensaje);
		assertEquals(vistaEsperada, vistaRecibida);
	}
		
	@Test
	public void registroFallidoPorCorreoRepetido() throws DaoException{
		String vistaEsperada = "registro";
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
		
		ModelAndView vista = controlador.registrarUsuario(formulario, resultado);
		String vistaRecibida = vista.getViewName();
		
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void registroFallidoPorNombreDeUsuarioRepetido() throws DaoException{
		String vistaEsperada = "registro";
		when(usuarioServicio.buscarUsuarioPorNombre(nombre)).thenReturn(usuario);
		
		ModelAndView vista = controlador.registrarUsuario(formulario, resultado);
		String vistaRecibida = vista.getViewName();
		
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void registroFallidoPorOtroErrorEnFormulario() throws DaoException, ObjectNotFoundException{
		resultado.reject("nombre", "error.nombre");
		String vistaEsperada = "registro";
		
		ModelAndView vista = controlador.registrarUsuario(formulario, resultado);
		String vistaRecibida = vista.getViewName();
		
		assertEquals(vistaEsperada, vistaRecibida);
	}
}
