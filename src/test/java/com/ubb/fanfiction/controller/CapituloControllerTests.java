package com.ubb.fanfiction.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.CapituloForm;
import com.ubb.fanfiction.form.OrdenarFanfictionForm;
import com.ubb.fanfiction.model.CapituloPublicacion;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.CapituloServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class CapituloControllerTests {
	private Usuario usuario;
	private Usuario infiltrado;
	private String nombre;
	private String correo;
	private Integer idFanfiction;
	private Fanfiction fanfiction;
	private List<CapituloPublicacion> listaCapitulos;
	private CapituloForm formulario;
	private BindingResult resultado;
	private Capitulo primero;
	private Capitulo segundo;
	private Capitulo tercero;
	private Integer idCapitulo;
	private CapituloPublicacion capituloPublicacionUno;
	private CapituloPublicacion capituloPublicacionDos;
	private CapituloPublicacion capituloPublicacionTres;
	private List<Publicacion> listaFanfiction;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	
	@Mock
	private FanfictionServicio fanfictionServicio;
	@Mock
	private CapituloServicio capituloServicio;
	
	@InjectMocks
	private CapituloController controlador = new CapituloController();
	
	@Before
	public void setUp(){
		Integer idUsuario = 2;
		nombre = "mmunocan";
		idFanfiction = 1;
		correo = "mmunocan@gmail.com";
		idCapitulo = 1;
		Date fechaCreacion = Calendar.getInstance().getTime();
		Date fechaActualizacion = Calendar.getInstance().getTime();
		ClasificacionParental clasificacion = new ClasificacionParental("K (5+)");
		
		usuario = new Usuario();
		usuario.setIdUsuario(idUsuario);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(fechaCreacion);
		
		infiltrado = new Usuario();
		infiltrado.setIdUsuario(23);
		infiltrado.setNombre("Pedro");
		infiltrado.setCorreo("pedro@hotmail.com");
		infiltrado.setClave("p3dr14o631b0");
		infiltrado.setRol("USER");
		infiltrado.setEstado(true);
		infiltrado.setFechaCreacion(fechaCreacion);
		
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(fechaCreacion);
		fanfiction.setFechaActualizacion(fechaActualizacion);
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(usuario);
		fanfiction.setClasificacionParental(clasificacion);
		fanfiction.setGeneros(new HashSet<Genero>());
		
		capituloPublicacionUno = new CapituloPublicacion();
		capituloPublicacionDos = new CapituloPublicacion();
		capituloPublicacionTres = new CapituloPublicacion();
		capituloPublicacionUno.setCantidadComentarios((long)12);
		capituloPublicacionDos.setCantidadComentarios((long)6);
		capituloPublicacionTres.setCantidadComentarios((long)0);
		capituloPublicacionUno.setPromedioPuntuacion((double)3.5);
		capituloPublicacionDos.setPromedioPuntuacion((double)4.0);
		capituloPublicacionTres.setPromedioPuntuacion((double)0.0);
		
		primero = new Capitulo();
		primero.setIdCapitulo(idCapitulo);
		primero.setNumero(1);
		primero.setTitulo("Una carta amenazante");
		primero.setFechaPublicacion(fechaCreacion);
		primero.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		segundo = new Capitulo();
		segundo.setIdCapitulo(2);
		segundo.setNumero(2);
		segundo.setTitulo("La sorpresa de Hakimoto");
		segundo.setFechaPublicacion(fechaCreacion);
		segundo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		tercero = new Capitulo();
		tercero.setIdCapitulo(3);
		tercero.setNumero(3);
		tercero.setTitulo("La sorpresa de Hakimoto 2");
		tercero.setFechaPublicacion(fechaCreacion);
		tercero.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		capituloPublicacionUno.setCapitulo(primero);
		capituloPublicacionDos.setCapitulo(segundo);
		capituloPublicacionTres.setCapitulo(tercero);
		
		listaCapitulos = new ArrayList<CapituloPublicacion>();
		listaCapitulos.add(capituloPublicacionUno);
		listaCapitulos.add(capituloPublicacionDos);
		listaCapitulos.add(capituloPublicacionTres);
		
		primero.setFanfiction(fanfiction);
		segundo.setFanfiction(fanfiction);
		tercero.setFanfiction(fanfiction);
		Set<Capitulo> capitulos = new HashSet<Capitulo>();
		capitulos.add(primero);
		capitulos.add(segundo);
		capitulos.add(tercero);
		fanfiction.setCapitulos(capitulos);
		
		formulario = new CapituloForm();
		formulario.setIdCapitulo(idCapitulo);
		formulario.setIdFanfiction(idFanfiction);
		formulario.setNumero(3);
		formulario.setTitulo(primero.getTitulo());
		formulario.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		resultado = new BeanPropertyBindingResult(formulario, "capituloForm");
		
		listaFanfiction = new ArrayList<Publicacion>();
		Publicacion fanfictionCompleto = new Publicacion();
		fanfictionCompleto.setFanfiction(fanfiction);
		fanfictionCompleto.setCantidadComentarios((long) 123);
		fanfictionCompleto.setPromedioPuntuacion(12.1);
		listaFanfiction.add(fanfictionCompleto);
		
	}
	
	@Test
	public void obtenerListaCapitulosDeFanfictionOrdenadosPorNumeroExitoso() throws DaoException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, usuario.getIdUsuario())).thenReturn(true);
		when(capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(idFanfiction)).thenReturn(listaCapitulos);
		String vistaEsperada = "listadecapitulos";
		
		ModelAndView vista = controlador.obtenerListaDeCapitulos(idFanfiction);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<CapituloPublicacion> listaCapitulosResultante = (List<CapituloPublicacion>) vista.getModelMap().get("capitulosPublicacion");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(listaCapitulos,listaCapitulosResultante);
	}
	
	@Test
	public void obtenerListaCapituloDeFanfictionOrdenadosPorNumeroDeFormaFallidaPorIngresoDeUsuarioNoAutorizado() throws DaoException{
		chargeAuthentication(infiltrado.getCorreo(),infiltrado);
		when(fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, infiltrado.getIdUsuario())).thenReturn(false);
		String vistaEsperada = "403";
		
		ModelAndView vista = controlador.obtenerListaDeCapitulos(idFanfiction);
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(infiltrado.getNombre(), nombreRecibido);
		assertEquals(vistaEsperada,vistaRecibida);
	}
	
	@Test
	public void abrirFormularioParaCrearNuevoCapituloExitoso() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, usuario.getIdUsuario())).thenReturn(true);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		String vistaEsperada = "nuevocapitulo";
		
		ModelAndView vista = controlador.abrirFormularioCrearNuevoCapitulo(idFanfiction);
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		CapituloForm formulario = (CapituloForm)vista.getModelMap().get("capituloForm");
		Fanfiction fanfiction = (Fanfiction)vista.getModelMap().get("fanfiction");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(nombre,nombreRecibido);
		assertEquals(this.fanfiction,fanfiction);
		assertEquals(vistaEsperada,vistaRecibida);
		
	}
	
	@Test
	public void abrirFormularioParaCrearNuevoCapituloDeFormaFallidaPorUsuarioNoAutorizado() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(infiltrado.getCorreo(),infiltrado);
		when(fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, infiltrado.getIdUsuario())).thenReturn(false);
		String vistaEsperada = "403";
		
		ModelAndView vista = controlador.abrirFormularioCrearNuevoCapitulo(idFanfiction);
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
				
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(infiltrado.getNombre(), nombreRecibido);
		
	}
	
	@Test
	public void guardarCapituloDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(),OrdenarFanfictionForm.FECHA_CREACION,OrdenarFanfictionForm.DESC)).thenReturn(listaFanfiction);
		when(fanfictionServicio.buscarFanfictionPorId(formulario.getIdFanfiction())).thenReturn(fanfiction);
		String vistaEsperada = "listadefanfictions";
		
		ModelAndView vista = controlador.guardarCapitulo(formulario, resultado);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Publicacion> listaRecibida = (List<Publicacion>) vista.getModelMap().get("fanfictions");
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		OrdenarFanfictionForm formulario = (OrdenarFanfictionForm) vista.getModelMap().get("ordenarFanfictionForm");
		
		verify(capituloServicio).guardarCapitulo((Capitulo)anyObject());
		verify(fanfictionServicio).actualizarFanfiction(fanfiction);
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(vistaEsperada,vistaRecibida);
		assertNotNull(mensajeExito);
		assertEquals(nombre,nombreRecibido);
		assertEquals(listaFanfiction, listaRecibida);
		
	}
	
	
	@Test
	public void guardarCapituloDeFormaErroneaPorErrorEnFormulario() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.buscarFanfictionPorId(formulario.getIdFanfiction())).thenReturn(fanfiction);
		resultado.rejectValue("titulo", "error.titulo");
		String vistaEsperada = "nuevocapitulo";
		
		ModelAndView vista = controlador.guardarCapitulo(formulario,resultado);
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		CapituloForm formulario = (CapituloForm)vista.getModelMap().get("capituloForm");
		Fanfiction fanfiction = (Fanfiction)vista.getModelMap().get("fanfiction");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(nombre,nombreRecibido);
		assertEquals(this.fanfiction,fanfiction);
		assertEquals(vistaEsperada,vistaRecibida);
	}
	
	@Test
	public void abrirFormularioEdicionCapituloDeFormaCorrecta() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(capituloServicio.capituloEsDeUsuario(idCapitulo,usuario.getIdUsuario())).thenReturn(true);
		when(capituloServicio.obtenerCapitulo(primero.getIdCapitulo())).thenReturn(primero);
		String vistaEsperada = "editarCapitulo";
		
		ModelAndView vista = controlador.abrirFormularioEditarCapitulo(idCapitulo);
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		CapituloForm formulario = (CapituloForm)vista.getModelMap().get("capituloForm");
		Capitulo capituloRecibido = (Capitulo) vista.getModelMap().get("capitulo");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertNotNull(formulario.getIdCapitulo());
		assertNotNull(formulario.getTitulo());
		assertNotNull(formulario.getContenido());
		assertEquals(nombre,nombreRecibido);
		assertEquals(primero,capituloRecibido);
		assertEquals(vistaEsperada,vistaRecibida);
	}
	
	@Test
	public void abrirFormularioEdicionCapituloDeFormaFallidaPorIngresoUsuarioNoAutorizado() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(infiltrado.getCorreo(), infiltrado);
		when(capituloServicio.capituloEsDeUsuario(idCapitulo, infiltrado.getIdUsuario())).thenReturn(false);
		String vistaEsperada = "403";
		
		ModelAndView vista = controlador.abrirFormularioEditarCapitulo(idCapitulo);
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(infiltrado.getNombre(), nombreRecibido);
	}
	
	@Test
	public void editarCapituloDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(capituloServicio.obtenerCapitulo(idCapitulo)).thenReturn(primero);
		when(capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(primero.getFanfiction().getIdFanfiction())).thenReturn(listaCapitulos);
		String vistaEsperada = "listadecapitulos";
		
		ModelAndView vista = controlador.editarCapitulo(formulario, resultado);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<CapituloPublicacion> listaCapitulosResultante = (List<CapituloPublicacion>) vista.getModelMap().get("capitulosPublicacion");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		
		verify(capituloServicio).editarCapitulo(primero);
		verify(fanfictionServicio).actualizarFanfiction(primero.getFanfiction());
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(listaCapitulos,listaCapitulosResultante);
		assertNotNull(mensajeExito);

	}
	
	@Test
	public void editarCapituloDeFormaFallidaPorErrorEnFormulario() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		resultado.rejectValue("titulo", "error.titulo");
		when(capituloServicio.obtenerCapitulo(idCapitulo)).thenReturn(primero);
		String vistaEsperada = "editarCapitulo";
		
		ModelAndView vista = controlador.editarCapitulo(formulario, resultado);
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		CapituloForm capituloForm = (CapituloForm) vista.getModelMap().get("capituloForm");
		Capitulo capituloRecibido = (Capitulo) vista.getModelMap().get("capitulo");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertNotNull(capituloForm);
		assertEquals(primero, capituloRecibido);
	}
	
	@Test
	public void eliminarCapituloDeFormaCorrecta() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo, usuario);
		Integer idCapitulo = segundo.getIdCapitulo();
		when(capituloServicio.capituloEsDeUsuario(idCapitulo, usuario.getIdUsuario())).thenReturn(true);
		when(capituloServicio.obtenerCapitulo(idCapitulo)).thenReturn(segundo);
		when(capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(idFanfiction)).thenReturn(listaCapitulos);
		String vistaEsperada = "listadecapitulos";
		
		ModelAndView vista = controlador.eliminarCapitulo(idCapitulo);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<CapituloPublicacion> listaCapitulosResultante = (List<CapituloPublicacion>) vista.getModelMap().get("capitulosPublicacion");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(listaCapitulos,listaCapitulosResultante);
		assertNotNull(mensajeExito);
		verify(capituloServicio).eliminarCapitulo(segundo);
		verify(fanfictionServicio).actualizarFanfiction(segundo.getFanfiction());
		
	}
	
	@Test
	public void eliminarCapituloDeFormaFallidaPorSerUnicoCapitulo() throws DaoException, ObjectNotFoundException{
		Set<Capitulo> caps = fanfiction.getCapitulos();
		caps.remove(segundo);
		caps.remove(tercero);
		listaCapitulos.remove(capituloPublicacionDos);
		listaCapitulos.remove(capituloPublicacionTres);
		segundo.setFanfiction(null);
		tercero.setFanfiction(null);
		chargeAuthentication(correo, usuario);
		Integer idCapitulo = primero.getIdCapitulo();
		when(capituloServicio.capituloEsDeUsuario(idCapitulo, usuario.getIdUsuario())).thenReturn(true);
		when(capituloServicio.obtenerCapitulo(idCapitulo)).thenReturn(primero);
		when(capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(idFanfiction)).thenReturn(listaCapitulos);
		String vistaEsperada = "listadecapitulos";
		
		ModelAndView vista = controlador.eliminarCapitulo(idCapitulo);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<CapituloPublicacion> listaCapitulosResultante = (List<CapituloPublicacion>) vista.getModelMap().get("capitulosPublicacion");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensajeError = (String) vista.getModelMap().get("mensajeError");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(listaCapitulos,listaCapitulosResultante);
		assertNotNull(mensajeError);

		
	}
	
	@Test
	public void eliminarCapituloDeFormaFallidaPorUsuarioNoAutorizado() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(infiltrado.getCorreo(), infiltrado);
		when(capituloServicio.capituloEsDeUsuario(idCapitulo, infiltrado.getIdUsuario())).thenReturn(false);
		String vistaEsperada = "403";
		
		ModelAndView vista = controlador.eliminarCapitulo(idCapitulo);
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(infiltrado.getNombre(), nombreRecibido);
		
	}
	
	private void chargeAuthentication(String correo, Usuario usuario) throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
	}
	
}
