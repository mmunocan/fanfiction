package com.ubb.fanfiction.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.service.ComentarioServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class ComentarioControllerTests {
	private Integer idCapitulo;
	private String nombre;
	private String correo;
	private Usuario usuario;
	private List<Comentario> comentarios;
	private List<Comentario> comentariosRealizados;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	
	@Mock
	private ComentarioServicio comentarioServicio;
	
	@InjectMocks
	private ComentarioController controlador = new ComentarioController();
	
	@Before
	public void setUp(){
		Integer idUsuario = 2;
		nombre = "mmunocan";
		correo = "mmunocan@gmail.com";
		Date fecha = Calendar.getInstance().getTime();
		idCapitulo = 1;
		
		Capitulo capitulo = new Capitulo();
		capitulo.setIdCapitulo(idCapitulo);
		capitulo.setNumero(1);
		capitulo.setTitulo("Una carta amenazante");
		capitulo.setFechaPublicacion(fecha);
		capitulo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		usuario = new Usuario();
		usuario.setIdUsuario(idUsuario);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(fecha);
		
		Usuario comentador1 = new Usuario();
		comentador1.setIdUsuario(3);
		comentador1.setNombre("pedro");
		comentador1.setCorreo("pedro@gmail.com");
		comentador1.setClave("hola3kmwkoo");
		comentador1.setRol("USER");
		comentador1.setEstado(true);
		comentador1.setFechaCreacion(fecha);
		Usuario comentador2 = new Usuario();
		comentador2.setIdUsuario(4);
		comentador2.setNombre("diego");
		comentador2.setCorreo("diego@yahoo.com");
		comentador2.setClave("dplvmpvwp");
		comentador2.setRol("USER");
		comentador2.setEstado(true);
		comentador2.setFechaCreacion(fecha);
		
		Comentario comentario1 = new Comentario();
		Comentario comentario2 = new Comentario();
		comentario1.setCapitulo(capitulo);
		comentario2.setCapitulo(capitulo);
		comentario1.setUsuario(comentador1);
		comentario2.setUsuario(comentador2);
		comentario1.setFecha(fecha);
		comentario2.setFecha(fecha);
		comentario1.setContenido("Me encantó tu historia, realmente es algo fascinante.");
		comentario2.setContenido("Conti plz");
		Comentario comentario3 = new Comentario();
		Comentario comentario4 = new Comentario();
		comentario3.setCapitulo(capitulo);
		comentario4.setCapitulo(capitulo);
		comentario3.setUsuario(usuario);
		comentario4.setUsuario(usuario);
		comentario3.setFecha(fecha);
		comentario4.setFecha(fecha);
		comentario3.setContenido("Me encantó tu historia, realmente es algo fascinante.");
		comentario4.setContenido("Conti plz");
		
		comentarios = new ArrayList<Comentario>();
		comentarios.add(comentario1);
		comentarios.add(comentario2);
		
		comentariosRealizados = new ArrayList<Comentario>();
		comentariosRealizados.add(comentario3);
		comentariosRealizados.add(comentario4);
	}
	
	@Test
	public void abrirHistorialComentariosRecibidas() throws DaoException{
		chargeAuthentication();
		when(comentarioServicio.obtenerComentariosRecibidos(usuario.getIdUsuario())).thenReturn(comentarios);
		String vistaEsperada = "comentariosRecibidos";
		
		ModelAndView vista = controlador.obtenerListaComentariosRecibidos();
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Comentario> comentariosRecibidos = (List<Comentario>) vista.getModelMap().get("comentariosRecibidos");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(comentarios,comentariosRecibidos);
	}
	
	@Test
	public void abrirHistorialComentariosRealizadas() throws DaoException{
		chargeAuthentication();
		when(comentarioServicio.obtenerComentariosRealizados(usuario.getIdUsuario())).thenReturn(comentariosRealizados);
		String vistaEsperada = "comentariosRealizados";
		
		ModelAndView vista = controlador.obtenerListaComentariosRealizados();
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Comentario> comentariosRecibidos = (List<Comentario>) vista.getModelMap().get("comentariosRealizados");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(comentariosRealizados,comentariosRecibidos);
	}
	
	private void chargeAuthentication() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
	}
}
