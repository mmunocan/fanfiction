package com.ubb.fanfiction.controller;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.FanfictionEditForm;
import com.ubb.fanfiction.form.FanfictionForm;
import com.ubb.fanfiction.form.OrdenarFanfictionForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.CapituloServicio;
import com.ubb.fanfiction.service.ClasificacionParentalServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.GeneroServicio;
import com.ubb.fanfiction.service.HistoriaOriginalServicio;
import com.ubb.fanfiction.service.LecturaServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class FanfictionControllerTests {
	private Usuario usuario;
	private Usuario impostor;
	private Integer idUsuario;
	private String nombre;
	private String correo;
	private Integer idFanfiction;
	private FanfictionForm formulario;
	private BindingResult resultado;
	private Fanfiction fanfiction;
	private List<HistoriaOriginal> listaHistoriasOriginales;
	private List<ClasificacionParental> listaClasificacionesParentales;
	private List<Genero> listaGeneros;
	private List<Publicacion> listaFanfiction;
	private FanfictionEditForm formularioEdit;
	private BindingResult resultadoEdit;
	
	
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	@Mock
	private HttpSession sesion;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private CapituloServicio capituloServicio;
	@Mock
	private HistoriaOriginalServicio historiaOriginalServicio;
	@Mock
	private GeneroServicio generoServicio;
	@Mock
	private ClasificacionParentalServicio clasificacionParentalServicio;
	
	@Mock
	private FanfictionServicio fanfictionServicio;
	
	@Mock
	private LecturaServicio lecturaServicio;
	
	@InjectMocks
	private FanfictionController controlador = new FanfictionController();
	
	@Before
	public void setUp(){
		idUsuario = 2;
		nombre = "mmunocan";
		idFanfiction = 1;
		correo = "mmunocan@gmail.com";
		
		usuario = new Usuario();
		usuario.setIdUsuario(idUsuario);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(Calendar.getInstance().getTime());
		
		impostor = new Usuario();
		impostor.setIdUsuario(3);
		impostor.setNombre("pedro");
		impostor.setCorreo("pedro@gmail.com");
		impostor.setClave("sdvwavwae");
		impostor.setRol("USER");
		impostor.setEstado(true);
		impostor.setFechaCreacion(Calendar.getInstance().getTime());
		
		int [] idGeneros = {1, 2, 3};
		int [] idHistoriasOriginales = {1,2};
		formulario = new FanfictionForm();
		formulario.setTitulo("Este es mi fanfiction");
		formulario.setDescripcion("¡Este fanfiction les va a encantar!");
		formulario.setClasificacionParental(1);
		formulario.setEstado("Incompleto");
		formulario.setGeneros(idGeneros);
		formulario.setHistoriasOriginales(idHistoriasOriginales);
		formulario.setTituloCapitulo("Capítulo 1: El comienzo de la historia");
		formulario.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		resultado = new BeanPropertyBindingResult(formulario, "fanfictionForm");
		
		formularioEdit = new FanfictionEditForm();
		formularioEdit.setId(idFanfiction);
		formularioEdit.setTitulo("Este es mi fanfiction");
		formularioEdit.setDescripcion("¡Este fanfiction les va a encantar!");
		formularioEdit.setClasificacionParental(1);
		formularioEdit.setEstado("Terminado");
		formularioEdit.setGeneros(idGeneros);
		formularioEdit.setHistoriasOriginales(idHistoriasOriginales);
		
		resultadoEdit = new BeanPropertyBindingResult(formularioEdit, "fanfictionForm");
		
		
		Categoria cat1 = new Categoria("Libro");
		Categoria cat2 = new Categoria("Película");
		HistoriaOriginal hist1 = new HistoriaOriginal(cat1,"Harry Potter");
		hist1.setIdHistoriaOriginal(12);
		HistoriaOriginal hist2 = new HistoriaOriginal(cat2,"Star Wars");
		hist2.setIdHistoriaOriginal(13);
		Genero genero1 = new Genero("Drama");
		genero1.setIdGenero(3);
		Genero genero2 = new Genero("Comedia");
		genero2.setIdGenero(4);
		ClasificacionParental clas1 = new ClasificacionParental("K (5+)");
		clas1.setIdClasificacionParental(12);
		ClasificacionParental clas2 = new ClasificacionParental("K+ (9+)");
		clas2.setIdClasificacionParental(4);
		
		
		HashSet<Genero> listaGeneros = new HashSet<Genero>();
		listaGeneros.add(genero1);
		listaGeneros.add(genero2);
		HashSet<HistoriaOriginal> listaHO = new HashSet<HistoriaOriginal>();
		listaHO.add(hist1);
		listaHO.add(hist2);
		
		Date fechaCreacion = Calendar.getInstance().getTime();
		Date fechaActualizacion = Calendar.getInstance().getTime();
		
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(fechaCreacion);
		fanfiction.setFechaActualizacion(fechaActualizacion);
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(usuario);
		fanfiction.setClasificacionParental(clas1);
		fanfiction.setGeneros(listaGeneros);
		fanfiction.setHistoriaOriginals(listaHO);
		
		HashSet<Capitulo> listaCapitulos = new HashSet<Capitulo>();
		fanfiction.setCapitulos(listaCapitulos);
		
		listaHistoriasOriginales = new ArrayList<HistoriaOriginal>();
		listaHistoriasOriginales.add(hist1);
		listaHistoriasOriginales.add(hist2);
		
		listaClasificacionesParentales = new ArrayList<ClasificacionParental>();
		listaClasificacionesParentales.add(clas1);
		listaClasificacionesParentales.add(clas2);
		
		this.listaGeneros = new ArrayList<Genero>();
		this.listaGeneros.add(genero1);
		this.listaGeneros.add(genero2);
		
		listaFanfiction = new ArrayList<Publicacion>();
		Publicacion fanfictionCompleto = new Publicacion();
		fanfictionCompleto.setFanfiction(fanfiction);
		fanfictionCompleto.setCantidadComentarios((long) 123);
		fanfictionCompleto.setPromedioPuntuacion(12.1);
		listaFanfiction.add(fanfictionCompleto);
		
		
	}
	
	@Test
	public void abrirListadoDeFanfictionsExitosa() throws ObjectNotFoundException, DaoException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.obtenerListaFanfictionsUsuario(idUsuario, OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC)).thenReturn(listaFanfiction);
		String vistaEsperada = "listadefanfictions";
		
		ModelAndView vista = controlador.abrirListadoDeFanfictions();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		@SuppressWarnings("unchecked")
		List<Publicacion> listaFanfictionRecibido =  (List<Publicacion>) vista.getModelMap().get("fanfictions");
		String vistaRecibida = vista.getViewName();
		OrdenarFanfictionForm formulario = (OrdenarFanfictionForm) vista.getModelMap().get("ordenarFanfictionForm");
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(listaFanfiction, listaFanfictionRecibido);
	}
	
	@Test
	public void ordenarListaFanfiction() throws DaoException{
		OrdenarFanfictionForm orden = new OrdenarFanfictionForm();
		orden.setAtributoOrden(OrdenarFanfictionForm.CAPITULOS);
		orden.setAtributoOrden(OrdenarFanfictionForm.ASC);
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.obtenerListaFanfictionsUsuario(idUsuario, orden.getAtributoOrden(), orden.getOrdenBusqueda())).thenReturn(listaFanfiction);
		String vistaEsperada = "listadefanfictions";
		
		ModelAndView vista = controlador.ordenarListaFanfiction(orden);
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		@SuppressWarnings("unchecked")
		List<Publicacion> listaFanfictionRecibido =  (List<Publicacion>) vista.getModelMap().get("fanfictions");
		String vistaRecibida = vista.getViewName();
		OrdenarFanfictionForm formulario = (OrdenarFanfictionForm) vista.getModelMap().get("ordenarFanfictionForm");
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(listaFanfiction, listaFanfictionRecibido);
	}
	
	@Test
	public void abrirFormularioParaCrearNuevoFanfictionExitoso() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(historiaOriginalServicio.obtenerListaHistoriasOriginales()).thenReturn(listaHistoriasOriginales);
		when(clasificacionParentalServicio.obtenerClasificacionesParentales()).thenReturn(listaClasificacionesParentales);
		when(generoServicio.obtenerListaGeneros()).thenReturn(listaGeneros);
		String vistaEsperada = "nuevofanfiction";
		
		ModelAndView vista = controlador.abrirFormularioCrearNuevoFic(sesion);
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		FanfictionForm formulario = (FanfictionForm) vista.getModelMap().get("fanfictionForm");
		@SuppressWarnings("unchecked")
		List<HistoriaOriginal> historiasOriginales = (List<HistoriaOriginal>) vista.getModelMap().get("historiasOriginales");
		@SuppressWarnings("unchecked")
		List<ClasificacionParental> clasificacionesParentales = (List<ClasificacionParental>) vista.getModelMap().get("clasificacionesParentales");
		@SuppressWarnings("unchecked")
		List<Genero> generos = (List<Genero>) vista.getModelMap().get("generos");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(listaHistoriasOriginales,historiasOriginales);
		assertEquals(listaClasificacionesParentales, clasificacionesParentales);
		assertEquals(listaGeneros, generos);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
	}
	
	
	@Test
	public void guardarFanfictionDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.obtenerListaFanfictionsUsuario(idUsuario, OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC)).thenReturn(listaFanfiction);
		when(fanfictionServicio.guardarFanfiction((Fanfiction)anyObject())).thenReturn(fanfiction);
		String vistaEsperada = "userhome";
		
		ModelAndView vista = controlador.guardarFanfiction(formulario, resultado, sesion);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Publicacion> listaFanfictionRecibido =  (List<Publicacion>) vista.getModelMap().get("fanfictions");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		
		verify(fanfictionServicio).guardarFanfiction((Fanfiction)anyObject());
		verify(capituloServicio).guardarCapitulo((Capitulo)anyObject());
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensajeExito);
		assertEquals(listaFanfiction, listaFanfictionRecibido);
	}
	
	
	
	@Test
	public void guardarFanfictionFallidoPorNoSeleccionarHistoriaOriginal() throws DaoException, ObjectNotFoundException{
		formulario.setHistoriasOriginales(null);
		chargeAuthentication(correo,usuario);
		when(sesion.getAttribute("historiasOriginales")).thenReturn(listaHistoriasOriginales);
		when(sesion.getAttribute("clasificacionesParentales")).thenReturn(listaClasificacionesParentales);
		when(sesion.getAttribute("generos")).thenReturn(listaGeneros);
		String vistaEsperada = "nuevoFanfiction";
		
		ModelAndView vista = controlador.guardarFanfiction(formulario, resultado, sesion);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		FanfictionForm formularioRespuesta = (FanfictionForm) vista.getModelMap().get("fanfictionForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(formularioRespuesta);
	}
	
	@Test
	public void guardarFanfictionFallidoPorNoSeleccionarGenero() throws DaoException, ObjectNotFoundException{
		formulario.setGeneros(null);
		chargeAuthentication(correo,usuario);
		when(sesion.getAttribute("historiasOriginales")).thenReturn(listaHistoriasOriginales);
		when(sesion.getAttribute("clasificacionesParentales")).thenReturn(listaClasificacionesParentales);
		when(sesion.getAttribute("generos")).thenReturn(listaGeneros);
		String vistaEsperada = "nuevoFanfiction";
		
		ModelAndView vista = controlador.guardarFanfiction(formulario, resultado, sesion);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		FanfictionForm formularioRespuesta = (FanfictionForm) vista.getModelMap().get("fanfictionForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(formularioRespuesta);
	}
	
	@Test
	public void guardarFanfictionFallidoPorOtroErrorEnFormulario() throws DaoException, ObjectNotFoundException{
		resultado.reject("titulo", "error.titulo");
		chargeAuthentication(correo,usuario);
		when(sesion.getAttribute("historiasOriginales")).thenReturn(listaHistoriasOriginales);
		when(sesion.getAttribute("clasificacionesParentales")).thenReturn(listaClasificacionesParentales);
		when(sesion.getAttribute("generos")).thenReturn(listaGeneros);
		String vistaEsperada = "nuevoFanfiction";
		
		ModelAndView vista = controlador.guardarFanfiction(formulario, resultado, sesion);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		FanfictionForm formularioRespuesta = (FanfictionForm) vista.getModelMap().get("fanfictionForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(formularioRespuesta);
	}
	
	
	@Test
	public void abrirFormularioParaEditarFanfictionExitoso() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, idUsuario)).thenReturn(true);
		when(historiaOriginalServicio.obtenerListaHistoriasOriginales()).thenReturn(listaHistoriasOriginales);
		when(clasificacionParentalServicio.obtenerClasificacionesParentales()).thenReturn(listaClasificacionesParentales);
		when(generoServicio.obtenerListaGeneros()).thenReturn(listaGeneros);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		String vistaEsperada = "editarFanfiction";
		
		ModelAndView vista = controlador.abrirFormularioEditarFanfiction(idFanfiction,sesion);
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		FanfictionEditForm formulario = (FanfictionEditForm) vista.getModelMap().get("fanfictionEditForm");
		Fanfiction fanfictionEditar = (Fanfiction) vista.getModelMap().get("fanfiction");
		@SuppressWarnings("unchecked")
		List<HistoriaOriginal> historiasOriginales = (List<HistoriaOriginal>) vista.getModelMap().get("historiasOriginales");
		@SuppressWarnings("unchecked")
		List<ClasificacionParental> clasificacionesParentales = (List<ClasificacionParental>) vista.getModelMap().get("clasificacionesParentales");
		@SuppressWarnings("unchecked")
		List<Genero> generos = (List<Genero>) vista.getModelMap().get("generos");
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Integer> generosSeleccionados = (List<Integer>) vista.getModelMap().get("generosSeleccionados");
		@SuppressWarnings("unchecked")
		List<Integer> historiasSeleccionadas = (List<Integer>) vista.getModelMap().get("historiasSeleccionadas");
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertNotNull(generosSeleccionados);
		assertNotNull(historiasSeleccionadas);
		assertEquals(fanfiction, fanfictionEditar);
		assertEquals(listaHistoriasOriginales,historiasOriginales);
		assertEquals(listaClasificacionesParentales, clasificacionesParentales);
		assertEquals(listaGeneros, generos);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
	}
	
	@Test
	public void abrirFormularioParaEditarFanfictionDeFormaFallidaPorIngresoDeUsuarioNoAutorizado() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(impostor.getCorreo(),impostor);
		when(fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, impostor.getIdUsuario())).thenReturn(false);
		String vistaEsperada = "403";
		
		ModelAndView vista = controlador.abrirFormularioEditarFanfiction(idFanfiction,sesion);
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(impostor.getNombre(), nombreRecibido);
	}
	
	@Test
	public void editarFanfictionDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.obtenerListaFanfictionsUsuario(idUsuario, OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC)).thenReturn(listaFanfiction);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		String vistaEsperada = "listadefanfictions";
		
		ModelAndView vista = controlador.editarFanfiction(formularioEdit, resultadoEdit, sesion);
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Publicacion> listaFanfictionRecibido =  (List<Publicacion>) vista.getModelMap().get("fanfictions");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		OrdenarFanfictionForm formulario = (OrdenarFanfictionForm) vista.getModelMap().get("ordenarFanfictionForm");
		
		verify(fanfictionServicio).actualizarFanfiction((Fanfiction)anyObject());
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(mensajeExito);
		assertEquals(listaFanfiction, listaFanfictionRecibido);
	}
	
	@Test
	public void editarFanfictionDeFormaFallidaPorNoSeleccionarGenero() throws DaoException, ObjectNotFoundException{
		formularioEdit.setGeneros(null);
		chargeAuthentication(correo,usuario);
		when(sesion.getAttribute("historiasOriginales")).thenReturn(listaHistoriasOriginales);
		when(sesion.getAttribute("clasificacionesParentales")).thenReturn(listaClasificacionesParentales);
		when(sesion.getAttribute("generos")).thenReturn(listaGeneros);
		String vistaEsperada = "editarFanfiction";
		
		ModelAndView vista = controlador.editarFanfiction(formularioEdit, resultadoEdit, sesion);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		FanfictionEditForm formularioRespuesta = (FanfictionEditForm) vista.getModelMap().get("fanfictionEditForm");
		@SuppressWarnings("unchecked")
		List<Integer> generosSeleccionados = (List<Integer>) vista.getModelMap().get("generosSeleccionados");
		@SuppressWarnings("unchecked")
		List<Integer> historiasSeleccionadas = (List<Integer>) vista.getModelMap().get("historiasSeleccionadas");
		
		assertNotNull(vista);
		assertNotNull(generosSeleccionados);
		assertNotNull(historiasSeleccionadas);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(formularioRespuesta);
	}
	
	@Test
	public void editarFanfictionDeFormaFallidaPorNoSeleccionarHistoriaOriginal() throws DaoException, ObjectNotFoundException{
		formularioEdit.setHistoriasOriginales(null);
		chargeAuthentication(correo,usuario);
		when(sesion.getAttribute("historiasOriginales")).thenReturn(listaHistoriasOriginales);
		when(sesion.getAttribute("clasificacionesParentales")).thenReturn(listaClasificacionesParentales);
		when(sesion.getAttribute("generos")).thenReturn(listaGeneros);
		String vistaEsperada = "editarFanfiction";
		
		ModelAndView vista = controlador.editarFanfiction(formularioEdit, resultadoEdit, sesion);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		FanfictionEditForm formularioRespuesta = (FanfictionEditForm) vista.getModelMap().get("fanfictionEditForm");
		@SuppressWarnings("unchecked")
		List<Integer> generosSeleccionados = (List<Integer>) vista.getModelMap().get("generosSeleccionados");
		@SuppressWarnings("unchecked")
		List<Integer> historiasSeleccionadas = (List<Integer>) vista.getModelMap().get("historiasSeleccionadas");
		
		assertNotNull(vista);
		assertNotNull(generosSeleccionados);
		assertNotNull(historiasSeleccionadas);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(formularioRespuesta);
	}
	
	@Test
	public void editarFanfictionDeFormaFallidaPorOtroError() throws DaoException, ObjectNotFoundException{
		resultadoEdit.reject("titulo", "error.titulo");
		chargeAuthentication(correo,usuario);
		when(sesion.getAttribute("historiasOriginales")).thenReturn(listaHistoriasOriginales);
		when(sesion.getAttribute("clasificacionesParentales")).thenReturn(listaClasificacionesParentales);
		when(sesion.getAttribute("generos")).thenReturn(listaGeneros);
		String vistaEsperada = "editarFanfiction";
		
		ModelAndView vista = controlador.editarFanfiction(formularioEdit, resultadoEdit, sesion);
		String vistaRecibida = vista.getViewName();
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		FanfictionEditForm formularioRespuesta = (FanfictionEditForm) vista.getModelMap().get("fanfictionEditForm");
		@SuppressWarnings("unchecked")
		List<Integer> generosSeleccionados = (List<Integer>) vista.getModelMap().get("generosSeleccionados");
		@SuppressWarnings("unchecked")
		List<Integer> historiasSeleccionadas = (List<Integer>) vista.getModelMap().get("historiasSeleccionadas");
		
		assertNotNull(vista);
		assertNotNull(generosSeleccionados);
		assertNotNull(historiasSeleccionadas);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre, nombreUsuarioRecibido);
		assertNotNull(formularioRespuesta);
	}
	
	@Test
	public void eliminarFanfictionDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		when(fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, idUsuario)).thenReturn(true);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		when(fanfictionServicio.obtenerListaFanfictionsUsuario(idUsuario, OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC)).thenReturn(listaFanfiction);
		String vistaEsperada = "listadefanfictions";
		
		ModelAndView vista = controlador.eliminarFanfiction(idFanfiction);
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		@SuppressWarnings("unchecked")
		List<Publicacion> listaFanfictionRecibido =  (List<Publicacion>) vista.getModelMap().get("fanfictions");
		String vistaRecibida = vista.getViewName();
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		OrdenarFanfictionForm formulario = (OrdenarFanfictionForm) vista.getModelMap().get("ordenarFanfictionForm");
		
		verify(fanfictionServicio).eliminarFanfiction(fanfiction);
		assertNotNull(vista);
		assertNotNull(formulario);
		assertNotNull(mensajeExito);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(listaFanfiction, listaFanfictionRecibido);
	}
	
	@Test
	public void eliminarFanfictionDeFormaFallidaPorIngresoDeUsuarioNoAutorizado() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(impostor.getCorreo(),impostor);
		when(fanfictionServicio.fanfictionEsDeUsuario(idFanfiction, impostor.getIdUsuario())).thenReturn(false);
		String vistaEsperada = "403";
		
		ModelAndView vista = controlador.eliminarFanfiction(idFanfiction);
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(impostor.getNombre(), nombreRecibido);
	}
	
	private void chargeAuthentication(String correo, Usuario usuario) throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
	}
	
	
	
}
