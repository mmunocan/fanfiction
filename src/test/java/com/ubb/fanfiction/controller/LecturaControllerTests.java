package com.ubb.fanfiction.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Lectura;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.service.LecturaServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class LecturaControllerTests {
	private Usuario usuario;
	private String nombre;
	private String correo;
	private List<Lectura> historialLectura;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	
	@Mock
	private LecturaServicio lecturaServicio;
	
	@InjectMocks
	private LecturaController controlador = new LecturaController();
	
	@Before
	public void setUp(){
		nombre = "mmunocan";
		correo = "mmunocan@gmail.com";
		Date fecha = Calendar.getInstance().getTime();
		
		usuario = new Usuario();
		usuario.setIdUsuario(2);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(fecha);
		
		Usuario escritor = new Usuario();
		escritor.setIdUsuario(2);
		escritor.setNombre("rg21xdw");
		escritor.setCorreo("rg21xdw@yahoo.com");
		escritor.setClave("ewinfgiwoemnfpo");
		escritor.setRol("USER");
		escritor.setEstado(true);
		escritor.setFechaCreacion(fecha);
		
		Fanfiction fanfiction1 = new Fanfiction();
		fanfiction1.setIdFanfiction(1);
		fanfiction1.setTitulo("Una historia infinita");
		fanfiction1.setDescripcion("Espero poder actualizar a diario");
		fanfiction1.setFechaCreacion(fecha);
		fanfiction1.setFechaActualizacion(fecha);
		fanfiction1.setEstado("Incompleto");
		fanfiction1.setUsuario(escritor);
		Fanfiction fanfiction2 = new Fanfiction();
		fanfiction2.setIdFanfiction(2);
		fanfiction2.setTitulo("Kya, la aventura del poder");
		fanfiction2.setDescripcion("Acompáñenme a ver esta triste historia.");
		fanfiction2.setFechaCreacion(fecha);
		fanfiction2.setFechaActualizacion(fecha);
		fanfiction2.setEstado("Incompleto");
		fanfiction2.setUsuario(escritor);
		
		Capitulo capituloFic1 = new Capitulo();
		capituloFic1.setIdCapitulo(1);
		capituloFic1.setNumero(1);
		capituloFic1.setTitulo("Una carta amenazante");
		capituloFic1.setFechaPublicacion(fecha);
		capituloFic1.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		capituloFic1.setFanfiction(fanfiction1);
		Capitulo capituloFic2 = new Capitulo();
		capituloFic2.setIdCapitulo(2);
		capituloFic2.setNumero(1);
		capituloFic2.setTitulo("El inicio de una gran historia");
		capituloFic2.setFechaPublicacion(fecha);
		capituloFic2.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		capituloFic2.setFanfiction(fanfiction2);
		
		Set<Capitulo> listFic1 = new HashSet<Capitulo>();
		listFic1.add(capituloFic1);
		Set<Capitulo> listFic2 = new HashSet<Capitulo>();
		listFic2.add(capituloFic2);
		
		fanfiction1.setCapitulos(listFic1);
		fanfiction2.setCapitulos(listFic2);
		
		Lectura lectura1 = new Lectura();
		lectura1.setIdLectura(1);
		lectura1.setCapitulo(capituloFic1);
		lectura1.setFecha(fecha);
		lectura1.setUsuario(usuario);
		Lectura lectura2 = new Lectura();
		lectura2.setIdLectura(2);
		lectura2.setCapitulo(capituloFic2);
		lectura2.setFecha(fecha);
		lectura2.setUsuario(usuario);
		
		historialLectura = new ArrayList<Lectura>();
		historialLectura.add(lectura1);
		historialLectura.add(lectura2);
		
	}
	
	@Test
	public void obtenerHistorialLecturaDeFormaExitosa() throws DaoException{
		chargeAuthentication();
		when(lecturaServicio.obtenerHistorialLectura(usuario.getIdUsuario())).thenReturn(historialLectura);
		String vistaEsperada = "historialLecturas";
		
		ModelAndView vista = controlador.obtenerHistorialLectura();
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Lectura> lecturasRecibidas = (List<Lectura>) vista.getModelMap().get("lecturas");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(historialLectura,lecturasRecibidas);
	}
	
	private void chargeAuthentication() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
	}
}

