package com.ubb.fanfiction.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.domain.Lectura;
import com.ubb.fanfiction.domain.Puntuacion;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.model.CapituloPublicacion;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.CapituloServicio;
import com.ubb.fanfiction.service.ClasificacionParentalServicio;
import com.ubb.fanfiction.service.ComentarioServicio;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.GeneroServicio;
import com.ubb.fanfiction.service.HistoriaOriginalServicio;
import com.ubb.fanfiction.service.LecturaServicio;
import com.ubb.fanfiction.service.PuntuacionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class LeerCapituloControllerTests {
	private Usuario usuario;
	private Integer idUsuario;
	private String nombre;
	private String correo;
	private Integer idFanfiction;
	private Fanfiction fanfiction;
	private Capitulo primero;
	private Capitulo segundo;
	private Capitulo tercero;
	private List<Comentario> comentariosCapitulo;
	private Puntuacion puntuacion;
	

	@Mock
	private ComentarioServicio comentarioServicio;
	@Mock
	private PuntuacionServicio puntuacionServicio;
	
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	@Mock
	private HttpSession sesion;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private CapituloServicio capituloServicio;
	@Mock
	private HistoriaOriginalServicio historiaOriginalServicio;
	@Mock
	private GeneroServicio generoServicio;
	@Mock
	private ClasificacionParentalServicio clasificacionParentalServicio;
	
	@Mock
	private FanfictionServicio fanfictionServicio;
	
	@Mock
	private LecturaServicio lecturaServicio;
	
	
	@InjectMocks
	public LeerCapituloController controlador = new LeerCapituloController();
	
	@Before
	public void setUp(){
		idUsuario = 2;
		nombre = "mmunocan";
		idFanfiction = 1;
		correo = "mmunocan@gmail.com";
		
		usuario = new Usuario();
		usuario.setIdUsuario(idUsuario);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		usuario.setFechaCreacion(Calendar.getInstance().getTime());
		
		
		
		Categoria cat1 = new Categoria("Libro");
		Categoria cat2 = new Categoria("Película");
		HistoriaOriginal hist1 = new HistoriaOriginal(cat1,"Harry Potter");
		hist1.setIdHistoriaOriginal(12);
		HistoriaOriginal hist2 = new HistoriaOriginal(cat2,"Star Wars");
		hist2.setIdHistoriaOriginal(13);
		Genero genero1 = new Genero("Drama");
		genero1.setIdGenero(3);
		Genero genero2 = new Genero("Comedia");
		genero2.setIdGenero(4);
		ClasificacionParental clas1 = new ClasificacionParental("K (5+)");
		clas1.setIdClasificacionParental(12);
		ClasificacionParental clas2 = new ClasificacionParental("K+ (9+)");
		clas2.setIdClasificacionParental(4);
		
		
		HashSet<Genero> listaGeneros = new HashSet<Genero>();
		listaGeneros.add(genero1);
		listaGeneros.add(genero2);
		HashSet<HistoriaOriginal> listaHO = new HashSet<HistoriaOriginal>();
		listaHO.add(hist1);
		listaHO.add(hist2);
		
		Date fechaCreacion = Calendar.getInstance().getTime();
		Date fechaActualizacion = Calendar.getInstance().getTime();
		
		primero = new Capitulo();
		primero.setIdCapitulo(1);
		primero.setNumero(1);
		primero.setTitulo("Una carta amenazante");
		primero.setFechaPublicacion(fechaCreacion);
		primero.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		segundo = new Capitulo();
		segundo.setIdCapitulo(2);
		segundo.setNumero(2);
		segundo.setTitulo("La sorpresa de Hakimoto");
		segundo.setFechaPublicacion(fechaCreacion);
		segundo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		tercero = new Capitulo();
		tercero.setIdCapitulo(3);
		tercero.setNumero(3);
		tercero.setTitulo("La sorpresa de Hakimoto 2");
		tercero.setFechaPublicacion(fechaCreacion);
		tercero.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(fechaCreacion);
		fanfiction.setFechaActualizacion(fechaActualizacion);
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(usuario);
		fanfiction.setClasificacionParental(clas1);
		fanfiction.setGeneros(listaGeneros);
		fanfiction.setHistoriaOriginals(listaHO);
		primero.setFanfiction(fanfiction);
		segundo.setFanfiction(fanfiction);
		tercero.setFanfiction(fanfiction);
		
		comentariosCapitulo = new ArrayList<Comentario>();
		comentariosCapitulo.add(new Comentario(primero, usuario, fechaCreacion, "Es un gran capitulo"));
		comentariosCapitulo.add(new Comentario(primero, usuario, fechaCreacion, "Conti plz"));
		comentariosCapitulo.add(new Comentario(primero, usuario, fechaCreacion, "Me encantó bastante"));
		
		HashSet<Capitulo> listaCapitulos = new HashSet<Capitulo>();
		listaCapitulos.add(primero);
		listaCapitulos.add(segundo);
		listaCapitulos.add(tercero);
		fanfiction.setCapitulos(listaCapitulos);
		
		Publicacion fanfictionCompleto = new Publicacion();
		fanfictionCompleto.setFanfiction(fanfiction);
		fanfictionCompleto.setCantidadComentarios((long) 123);
		fanfictionCompleto.setPromedioPuntuacion(12.1);
		
		puntuacion = new Puntuacion();
		puntuacion.setCapitulo(segundo);
		puntuacion.setUsuario(usuario);
		puntuacion.setValor((byte)3);
	}
	
	@Test
	public void mostrarCapituloDeFormaExitosaAUsuarioNoRegistrado() throws DaoException, ObjectNotFoundException{
		notChargeAuthentication();
		int numeroCapitulo = 2;
		Long comentariosTotales = (long) 10;
		Double puntuacionPromedioCapitulo = 2.5;
		Double puntuacionesPromedioFanfiction = 3.51;
		when(capituloServicio.obtenerCapitulo(idFanfiction,numeroCapitulo)).thenReturn(segundo);
		when(comentarioServicio.contarComentariosPorFanfiction(idFanfiction)).thenReturn(comentariosTotales);
		when(comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(tercero.getIdCapitulo())).thenReturn(comentariosCapitulo);
		when(puntuacionServicio.obtenerPromedioPuntuacionCapitulo(segundo.getIdCapitulo())).thenReturn(puntuacionPromedioCapitulo);
		when(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction)).thenReturn(puntuacionesPromedioFanfiction);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		String vistaEsperada = "capitulo";
		String capituloAnterior = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo - 1);
		String capituloSiguiente = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo + 1);
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		CapituloPublicacion capituloRecibido = (CapituloPublicacion) vista.getModel().get("capituloPublicacion");
		Publicacion publicacionRecibida = (Publicacion) vista.getModel().get("publicacion");
		String capituloAnteriorRecibido = (String) vista.getModel().get("anterior");
		String capituloSiguienteRecibido = (String) vista.getModel().get("siguiente");
		
		assertNotNull(vista);
		assertNotNull(capituloRecibido);
		assertNotNull(publicacionRecibida);
		assertNotNull(publicacionRecibida.getFanfiction());
		assertNotNull(publicacionRecibida.getCantidadComentarios());
		assertNotNull(publicacionRecibida.getPromedioPuntuacion());
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(capituloAnterior, capituloAnteriorRecibido);
		assertEquals(capituloSiguiente, capituloSiguienteRecibido);
		
	}
	
	@Test
	public void mostrarPrimerCapituloDeFormaExitosaAUsuarioNoRegistrado() throws DaoException, ObjectNotFoundException{
		notChargeAuthentication();
		int numeroCapitulo = 1;
		Long comentariosTotales = (long) 10;
		Double puntuacionPromedio = 2.5;
		Double puntuacionesPromedioFanfiction = 3.51;
		when(capituloServicio.obtenerCapitulo(idFanfiction,numeroCapitulo)).thenReturn(primero);
		when(comentarioServicio.contarComentariosPorFanfiction(idFanfiction)).thenReturn(comentariosTotales);
		when(puntuacionServicio.obtenerPromedioPuntuacionCapitulo(primero.getIdCapitulo())).thenReturn(puntuacionPromedio);
		when(comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(tercero.getIdCapitulo())).thenReturn(comentariosCapitulo);
		when(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction)).thenReturn(puntuacionesPromedioFanfiction);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);	
		String vistaEsperada = "capitulo";
		String capituloSiguiente = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo + 1);
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		CapituloPublicacion capituloRecibido = (CapituloPublicacion) vista.getModel().get("capituloPublicacion");
		Publicacion publicacionRecibida = (Publicacion) vista.getModel().get("publicacion");
		String capituloAnteriorRecibido = (String) vista.getModel().get("anterior");
		String capituloSiguienteRecibido = (String) vista.getModel().get("siguiente");
		
		assertNotNull(vista);
		assertNotNull(capituloRecibido);
		assertNotNull(publicacionRecibida);
		assertNotNull(publicacionRecibida.getFanfiction());
		assertNotNull(publicacionRecibida.getCantidadComentarios());
		assertNotNull(publicacionRecibida.getPromedioPuntuacion());
		assertEquals(vistaEsperada, vistaRecibida);
		assertNull(capituloAnteriorRecibido);
		assertEquals(capituloSiguiente, capituloSiguienteRecibido);
		
	}
	
	@Test
	public void mostrarUltimoCapituloDeFormaExitosaAUsuarioNoRegistrado() throws DaoException, ObjectNotFoundException{
		notChargeAuthentication();
		int numeroCapitulo = 3;
		Long comentariosTotales = (long) 10;
		Double puntuacionPromedio = 2.5;
		Double puntuacionesPromedioFanfiction = 3.51;
		when(capituloServicio.obtenerCapitulo(idFanfiction,numeroCapitulo)).thenReturn(tercero);
		when(comentarioServicio.contarComentariosPorFanfiction(idFanfiction)).thenReturn(comentariosTotales);
		when(comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(tercero.getIdCapitulo())).thenReturn(comentariosCapitulo);
		when(puntuacionServicio.obtenerPromedioPuntuacionCapitulo(tercero.getIdCapitulo())).thenReturn(puntuacionPromedio);
		when(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction)).thenReturn(puntuacionesPromedioFanfiction);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);	
		String vistaEsperada = "capitulo";
		String capituloAnterior = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo - 1);
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		CapituloPublicacion capituloRecibido = (CapituloPublicacion) vista.getModel().get("capituloPublicacion");
		Publicacion publicacionRecibida = (Publicacion) vista.getModel().get("publicacion");
		String capituloAnteriorRecibido = (String) vista.getModel().get("anterior");
		String capituloSiguienteRecibido = (String) vista.getModel().get("siguiente");
		
		assertNotNull(vista);
		assertNotNull(capituloRecibido);
		assertNotNull(publicacionRecibida);
		assertNotNull(publicacionRecibida.getFanfiction());
		assertNotNull(publicacionRecibida.getCantidadComentarios());
		assertNotNull(publicacionRecibida.getPromedioPuntuacion());
		assertEquals(vistaEsperada, vistaRecibida);
		assertNull(capituloSiguienteRecibido);
		assertEquals(capituloAnterior, capituloAnteriorRecibido);

	}
	
	@Test
	public void mostrarCapituloDeFormaErroneaPorNoExistirElCapituloAUsuarioNoRegistrado() throws DaoException, ObjectNotFoundException{
		notChargeAuthentication();
		int numeroCapitulo = 2;
		doThrow(ObjectNotFoundException.class).when(capituloServicio).obtenerCapitulo(idFanfiction,numeroCapitulo);
		String vistaEsperada = "capituloNoExiste";
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		
	}
	
	@Test
	public void mostrarCapituloDeFormaErroneaPorNoExistirElCapituloAUsuarioRegistrado() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correo,usuario);
		int numeroCapitulo = 2;
		doThrow(ObjectNotFoundException.class).when(capituloServicio).obtenerCapitulo(idFanfiction,numeroCapitulo);
		String vistaEsperada = "capituloNoExiste";
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		
	}
	
	@Test
	public void mostrarCapituloDeFormaExitosaAUsuarioRegistradoQueHaPuntuado() throws DaoException, ObjectNotFoundException{
		int numeroCapitulo = 2;
		Long comentariosTotales = (long) 10;
		Double puntuacionPromedio = 2.5;
		Double puntuacionesPromedioFanfiction = 3.51;
		chargeAuthentication(correo,usuario);
		when(capituloServicio.obtenerCapitulo(idFanfiction,numeroCapitulo)).thenReturn(segundo);
		when(comentarioServicio.contarComentariosPorFanfiction(idFanfiction)).thenReturn(comentariosTotales);
		when(puntuacionServicio.obtenerPromedioPuntuacionCapitulo(segundo.getIdCapitulo())).thenReturn(puntuacionPromedio);
		when(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction)).thenReturn(puntuacionesPromedioFanfiction);
		when(comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(tercero.getIdCapitulo())).thenReturn(comentariosCapitulo);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		when(puntuacionServicio.obtenerPuntuacion(segundo.getIdCapitulo(), usuario.getIdUsuario())).thenReturn(puntuacion);
		String vistaEsperada = "capitulo";
		String capituloAnterior = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo - 1);
		String capituloSiguiente = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo + 1);
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		CapituloPublicacion capituloRecibido = (CapituloPublicacion) vista.getModel().get("capituloPublicacion");
		Publicacion publicacionRecibida = (Publicacion) vista.getModel().get("publicacion");
		String capituloAnteriorRecibido = (String) vista.getModel().get("anterior");
		String capituloSiguienteRecibido = (String) vista.getModel().get("siguiente");
		Integer idUsuarioRecibido = (Integer) vista.getModelMap().get("idUsuario");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		Integer puntuacionRealizada = (int) vista.getModelMap().get("puntuacion");
		
		assertNotNull(vista);
		assertNotNull(capituloRecibido);
		assertNotNull(publicacionRecibida);
		assertNotNull(publicacionRecibida.getFanfiction());
		assertNotNull(publicacionRecibida.getCantidadComentarios());
		assertNotNull(publicacionRecibida.getPromedioPuntuacion());
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(capituloAnterior, capituloAnteriorRecibido);
		assertEquals(capituloSiguiente, capituloSiguienteRecibido);
		assertEquals(idUsuario, idUsuarioRecibido);
		assertEquals(nombre, nombreUsuarioRecibido);
		verify(lecturaServicio).actualizarLectura((Lectura)anyObject());
		assertNotNull(puntuacionRealizada);
		
	}
	
	@Test
	public void mostrarCapituloDeFormaExitosaAUsuarioRegistradoQueNoHaPuntuado() throws DaoException, ObjectNotFoundException{
		int numeroCapitulo = 2;
		Long comentariosTotales = (long) 10;
		Double puntuacionPromedio = 2.5;
		Double puntuacionesPromedioFanfiction = 3.51;
		chargeAuthentication(correo,usuario);
		when(capituloServicio.obtenerCapitulo(idFanfiction,numeroCapitulo)).thenReturn(segundo);
		when(comentarioServicio.contarComentariosPorFanfiction(idFanfiction)).thenReturn(comentariosTotales);
		when(puntuacionServicio.obtenerPromedioPuntuacionCapitulo(segundo.getIdCapitulo())).thenReturn(puntuacionPromedio);
		when(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction)).thenReturn(puntuacionesPromedioFanfiction);
		when(comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(tercero.getIdCapitulo())).thenReturn(comentariosCapitulo);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		doThrow(ObjectNotFoundException.class).when(puntuacionServicio).obtenerPuntuacion(segundo.getIdCapitulo(), usuario.getIdUsuario());
		String vistaEsperada = "capitulo";
		String capituloAnterior = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo - 1);
		String capituloSiguiente = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo + 1);
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		CapituloPublicacion capituloRecibido = (CapituloPublicacion) vista.getModel().get("capituloPublicacion");
		Publicacion publicacionRecibida = (Publicacion) vista.getModel().get("publicacion");
		String capituloAnteriorRecibido = (String) vista.getModel().get("anterior");
		String capituloSiguienteRecibido = (String) vista.getModel().get("siguiente");
		Integer idUsuarioRecibido = (Integer) vista.getModelMap().get("idUsuario");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		int puntuacionRealizada = (int) vista.getModelMap().get("puntuacion");
		
		assertNotNull(vista);
		assertNotNull(capituloRecibido);
		assertNotNull(publicacionRecibida);
		assertNotNull(publicacionRecibida.getFanfiction());
		assertNotNull(publicacionRecibida.getCantidadComentarios());
		assertNotNull(publicacionRecibida.getPromedioPuntuacion());
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(capituloAnterior, capituloAnteriorRecibido);
		assertEquals(capituloSiguiente, capituloSiguienteRecibido);
		assertEquals(idUsuario, idUsuarioRecibido);
		assertEquals(nombre, nombreUsuarioRecibido);
		verify(lecturaServicio).actualizarLectura((Lectura)anyObject());
		assertEquals(0,puntuacionRealizada);
	}
	
	
	@Test
	public void mostrarPrimerCapituloDeFormaExitosaAUsuarioRegistrado() throws DaoException, ObjectNotFoundException{
		int numeroCapitulo = 1;
		Long comentariosTotales = (long) 10;
		Double puntuacionPromedio = 2.5;
		Double puntuacionesPromedioFanfiction = 3.51;
		chargeAuthentication(correo,usuario);
		when(capituloServicio.obtenerCapitulo(idFanfiction,numeroCapitulo)).thenReturn(primero);
		when(comentarioServicio.contarComentariosPorFanfiction(idFanfiction)).thenReturn(comentariosTotales);
		when(puntuacionServicio.obtenerPromedioPuntuacionCapitulo(primero.getIdCapitulo())).thenReturn(puntuacionPromedio);
		when(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction)).thenReturn(puntuacionesPromedioFanfiction);
		when(comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(primero.getIdCapitulo())).thenReturn(comentariosCapitulo);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		when(puntuacionServicio.obtenerPuntuacion(primero.getIdCapitulo(), usuario.getIdUsuario())).thenReturn(puntuacion);	
		String vistaEsperada = "capitulo";
		String capituloSiguiente = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo + 1);
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		CapituloPublicacion capituloRecibido = (CapituloPublicacion) vista.getModel().get("capituloPublicacion");
		Publicacion publicacionRecibida = (Publicacion) vista.getModel().get("publicacion");
		String capituloAnteriorRecibido = (String) vista.getModel().get("anterior");
		String capituloSiguienteRecibido = (String) vista.getModel().get("siguiente");
		Integer idUsuarioRecibido = (Integer) vista.getModelMap().get("idUsuario");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		Integer puntuacionRealizada = (int) vista.getModelMap().get("puntuacion");
		
		assertNotNull(vista);
		assertNotNull(capituloRecibido);
		assertNotNull(publicacionRecibida);
		assertNotNull(publicacionRecibida.getFanfiction());
		assertNotNull(publicacionRecibida.getCantidadComentarios());
		assertNotNull(publicacionRecibida.getPromedioPuntuacion());
		assertEquals(vistaEsperada, vistaRecibida);
		assertNull(capituloAnteriorRecibido);
		assertEquals(capituloSiguiente, capituloSiguienteRecibido);
		assertEquals(idUsuario, idUsuarioRecibido);
		assertEquals(nombre, nombreUsuarioRecibido);
		verify(lecturaServicio).actualizarLectura((Lectura)anyObject());
		assertNotNull(puntuacionRealizada);
	}
	
	@Test
	public void mostrarUltimoCapituloDeFormaExitosaAUsuarioRegistrado() throws DaoException, ObjectNotFoundException{
		int numeroCapitulo = 3;
		Long comentariosTotales = (long) 10;
		Double puntuacionPromedio = 2.5;
		Double puntuacionesPromedioFanfiction = 3.51;
		chargeAuthentication(correo,usuario);
		when(capituloServicio.obtenerCapitulo(idFanfiction,numeroCapitulo)).thenReturn(tercero);
		when(comentarioServicio.contarComentariosPorFanfiction(idFanfiction)).thenReturn(comentariosTotales);
		when(puntuacionServicio.obtenerPromedioPuntuacionCapitulo(tercero.getIdCapitulo())).thenReturn(puntuacionPromedio);
		when(puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction)).thenReturn(puntuacionesPromedioFanfiction);
		when(comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(tercero.getIdCapitulo())).thenReturn(comentariosCapitulo);
		when(fanfictionServicio.buscarFanfictionPorId(idFanfiction)).thenReturn(fanfiction);
		when(puntuacionServicio.obtenerPuntuacion(tercero.getIdCapitulo(), usuario.getIdUsuario())).thenReturn(puntuacion);
		String vistaEsperada = "capitulo";
		String capituloAnterior = "/fanfiction/mostrar/" + idFanfiction + "/" + (numeroCapitulo - 1);
		
		ModelAndView vista = controlador.leerCapitulo(idFanfiction,numeroCapitulo);
		String vistaRecibida = vista.getViewName();
		CapituloPublicacion capituloRecibido = (CapituloPublicacion) vista.getModel().get("capituloPublicacion");
		Publicacion publicacionRecibida = (Publicacion) vista.getModel().get("publicacion");
		String capituloAnteriorRecibido = (String) vista.getModel().get("anterior");
		String capituloSiguienteRecibido = (String) vista.getModel().get("siguiente");
		Integer idUsuarioRecibido = (Integer) vista.getModelMap().get("idUsuario");
		String nombreUsuarioRecibido = (String) vista.getModelMap().get("nombreUsuario");
		Integer puntuacionRealizada = (int) vista.getModelMap().get("puntuacion");
		
		assertNotNull(vista);
		assertNotNull(capituloRecibido);
		assertNotNull(publicacionRecibida);
		assertNotNull(publicacionRecibida.getFanfiction());
		assertNotNull(publicacionRecibida.getCantidadComentarios());
		assertNotNull(publicacionRecibida.getPromedioPuntuacion());
		assertEquals(vistaEsperada, vistaRecibida);
		assertNull(capituloSiguienteRecibido);
		assertEquals(capituloAnterior, capituloAnteriorRecibido);
		assertEquals(idUsuario, idUsuarioRecibido);
		assertEquals(nombre, nombreUsuarioRecibido);
		verify(lecturaServicio).actualizarLectura((Lectura)anyObject());
		assertNotNull(puntuacionRealizada);
		
	}
	
	private void chargeAuthentication(String correo, Usuario usuario) throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
	}
	
	private void notChargeAuthentication() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(null);
	}
}
