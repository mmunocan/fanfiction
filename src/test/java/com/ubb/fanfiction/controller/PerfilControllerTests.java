package com.ubb.fanfiction.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.OrdenarFanfictionForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class PerfilControllerTests {
	private Usuario usuarioAVer;
	private Usuario usuarioRegistrado;
	private Integer idUsuarioAver;
	private String nombreRegistrado;
	private String correoRegistrado;
	private List<Publicacion> fanfictionsDeUsuario;
	
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	@Mock
	private HttpSession sesion;
	
	@Mock
	private FanfictionServicio fanfictionServicio;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	
	@InjectMocks
	public PerfilController controlador = new PerfilController();
	
	@Before
	public void setUp(){
		nombreRegistrado = "mmunocan";
		correoRegistrado = "mmunocan@gmail.com";
		String claveRegistrado = "c14v3";
		idUsuarioAver = 2;
		String nombreAVer = "pedro";
		String correoAVer = "pedro@gmail.com";
		String claveAver = "c14v3DePedro";
		String descripcionAVer = "Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ";
		
		usuarioRegistrado = new Usuario();
		usuarioRegistrado.setIdUsuario(2);
		usuarioRegistrado.setNombre(nombreRegistrado);
		usuarioRegistrado.setCorreo(correoRegistrado);
		usuarioRegistrado.setClave(claveRegistrado);
		usuarioRegistrado.setRol("USER");
		usuarioRegistrado.setEstado(true);
		usuarioRegistrado.setFechaCreacion(Calendar.getInstance().getTime());
		
		usuarioAVer = new Usuario();
		usuarioAVer.setIdUsuario(idUsuarioAver);
		usuarioAVer.setNombre(nombreAVer);
		usuarioAVer.setCorreo(correoAVer);
		usuarioAVer.setClave(claveAver);
		usuarioAVer.setRol("USER");
		usuarioAVer.setEstado(true);
		usuarioAVer.setDescripcion(descripcionAVer);
		usuarioAVer.setFechaCreacion(Calendar.getInstance().getTime());
		
		ClasificacionParental clasificacion = new ClasificacionParental("K (5+)");
		Date fecha = Calendar.getInstance().getTime();
		
		Fanfiction fanfiction = new Fanfiction(clasificacion, usuarioAVer, "Una historia infinita", "Espero poder actualizar a diario", fecha, fecha, "Incompleto");
		
		fanfictionsDeUsuario = new ArrayList<Publicacion>();
		Publicacion fanfictionCompleto = new Publicacion();
		fanfictionCompleto.setFanfiction(fanfiction);
		fanfictionCompleto.setCantidadComentarios((long) 123);
		fanfictionCompleto.setPromedioPuntuacion(12.1);
		fanfictionsDeUsuario.add(fanfictionCompleto);
		
	}
	
	@Test
	public void mostrarPerfilDeUsuarioDeFormaExitosaAUsuarioNoRegistrado() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correoRegistrado, null);
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioAver)).thenReturn(usuarioAVer);
		when(fanfictionServicio.obtenerListaFanfictionsUsuario(idUsuarioAver, OrdenarFanfictionForm.FECHA_ACTUALIZACION, OrdenarFanfictionForm.DESC)).thenReturn(fanfictionsDeUsuario);
		String vistaEsperada = "perfilusuario";
		
		ModelAndView vista = controlador.mostrarPerfilDeUsuario(idUsuarioAver);
		String vistaRecibida = vista.getViewName();
		Usuario usuarioRecibido = (Usuario) vista.getModelMap().get("usuario");
		@SuppressWarnings("unchecked")
		List<Publicacion> fanfictionsDeUsuarioRecibido = (List<Publicacion>) vista.getModelMap().get("fanfictions");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(usuarioAVer,usuarioRecibido);
		assertEquals(fanfictionsDeUsuario, fanfictionsDeUsuarioRecibido);
		
	}
	
	@Test
	public void mostrarPerfilDeUsuarioDeFormaExitosaAUsuarioRegistrado() throws DaoException, ObjectNotFoundException{
		chargeAuthentication(correoRegistrado, usuarioRegistrado);
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioAver)).thenReturn(usuarioAVer);
		when(fanfictionServicio.obtenerListaFanfictionsUsuario(idUsuarioAver, OrdenarFanfictionForm.FECHA_ACTUALIZACION, OrdenarFanfictionForm.DESC)).thenReturn(fanfictionsDeUsuario);
		String vistaEsperada = "perfilusuario";
		
		ModelAndView vista = controlador.mostrarPerfilDeUsuario(idUsuarioAver);
		String vistaRecibida = vista.getViewName();
		Usuario usuarioRecibido = (Usuario) vista.getModelMap().get("usuario");
		Integer idRecibido = (Integer) vista.getModelMap().get("idUsuario");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		@SuppressWarnings("unchecked")
		List<Publicacion> fanfictionsDeUsuarioRecibido = (List<Publicacion>) vista.getModelMap().get("fanfictions");
		
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(usuarioAVer,usuarioRecibido);
		assertEquals(nombreRegistrado,nombreRecibido);
		assertNull(idRecibido);
		assertEquals(fanfictionsDeUsuario, fanfictionsDeUsuarioRecibido);
	}
	
	@Test
	public void mostrarPerfilDeUsuarioDeFormaFallidaPorNoExistirUsuario() throws DaoException, ObjectNotFoundException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioAver)).thenReturn(null);
		doThrow(ObjectNotFoundException.class).when(usuarioServicio).buscarUsuarioPorId(idUsuarioAver);
		String vistaEsperada = "usuarioNoExiste";
		
		ModelAndView vista = controlador.mostrarPerfilDeUsuario(idUsuarioAver);
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		
	}
	
	@Test
	public void mostrarPerfilDeUsuarioDeFormaFallidaPorEstarUsuarioInactivo() throws DaoException, ObjectNotFoundException{
		usuarioAVer.setEstado(false);
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioAver)).thenReturn(usuarioAVer);
		String vistaEsperada = "usuarioNoExiste";
		
		ModelAndView vista = controlador.mostrarPerfilDeUsuario(idUsuarioAver);
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);

	}
	
	@Test
	public void mostrarPerfilDeUsuarioDeFormaFallidaPorPedirCuentaAdministrador() throws DaoException, ObjectNotFoundException{
		usuarioAVer.setRol("ADMIN");
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioAver)).thenReturn(usuarioAVer);
		String vistaEsperada = "usuarioNoExiste";
		
		ModelAndView vista = controlador.mostrarPerfilDeUsuario(idUsuarioAver);
		String vistaRecibida = vista.getViewName();

		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);

	}
	
	private void chargeAuthentication(String correo, Usuario usuario) throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
	}
}
