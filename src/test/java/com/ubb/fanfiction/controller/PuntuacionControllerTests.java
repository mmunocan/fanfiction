package com.ubb.fanfiction.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.Puntuacion;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.service.PuntuacionServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class PuntuacionControllerTests {
	private String correo;
	private String nombre;
	private Usuario escritor;
	private List<Puntuacion> puntuaciones;
	private List<Puntuacion> puntuacionesRealizadas;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	
	@Mock
	private PuntuacionServicio puntuacionServicio;
	
	@InjectMocks
	private PuntuacionController controlador = new PuntuacionController();
	
	@Before
	public void setUp(){
		Integer idUsuario = 2;
		nombre = "mmunocan";
		correo = "mmunocan@gmail.com";
		Date fecha = Calendar.getInstance().getTime();
		
		escritor = new Usuario();
		escritor.setIdUsuario(idUsuario);
		escritor.setNombre(nombre);
		escritor.setCorreo(correo);
		escritor.setClave("c14v3");
		escritor.setRol("USER");
		escritor.setEstado(true);
		escritor.setFechaCreacion(fecha);
		
		Usuario puntuador1 = new Usuario();
		puntuador1.setIdUsuario(3);
		puntuador1.setNombre("pedro");
		puntuador1.setCorreo("pedro@gmail.com");
		puntuador1.setClave("hola3kmwkoo");
		puntuador1.setRol("USER");
		puntuador1.setEstado(true);
		puntuador1.setFechaCreacion(fecha);
		Usuario puntuado2 = new Usuario();
		puntuado2.setIdUsuario(4);
		puntuado2.setNombre("diego");
		puntuado2.setCorreo("diego@yahoo.com");
		puntuado2.setClave("dplvmpvwp");
		puntuado2.setRol("USER");
		puntuado2.setEstado(true);
		puntuado2.setFechaCreacion(fecha);
		
		Capitulo segundo = new Capitulo();
		segundo.setIdCapitulo(2);
		segundo.setNumero(2);
		segundo.setTitulo("La sorpresa de Hakimoto");
		segundo.setFechaPublicacion(fecha);
		segundo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		Puntuacion puntuacion1 = new Puntuacion();
		Puntuacion puntuacion2 = new Puntuacion();
		puntuacion1.setIdPuntuacion(1);
		puntuacion2.setIdPuntuacion(2);
		puntuacion1.setFecha(fecha);
		puntuacion2.setFecha(fecha);
		puntuacion1.setValor((byte) 3);
		puntuacion2.setValor((byte) 5);
		puntuacion1.setCapitulo(segundo);
		puntuacion2.setCapitulo(segundo);
		puntuacion1.setUsuario(puntuador1);
		puntuacion2.setUsuario(puntuado2);
		Puntuacion puntuacion3 = new Puntuacion();
		Puntuacion puntuacion4 = new Puntuacion();
		puntuacion1.setIdPuntuacion(3);
		puntuacion2.setIdPuntuacion(4);
		puntuacion1.setFecha(fecha);
		puntuacion2.setFecha(fecha);
		puntuacion1.setValor((byte) 2);
		puntuacion2.setValor((byte) 1);
		puntuacion1.setCapitulo(segundo);
		puntuacion2.setCapitulo(segundo);
		puntuacion1.setUsuario(escritor);
		puntuacion2.setUsuario(escritor);
		
		puntuaciones = new ArrayList<Puntuacion>();
		puntuaciones.add(puntuacion1);
		puntuaciones.add(puntuacion2);
		
		puntuacionesRealizadas = new ArrayList<Puntuacion>();
		puntuacionesRealizadas.add(puntuacion3);
		puntuacionesRealizadas.add(puntuacion4);
	}
	
	@Test
	public void abrirHistorialPuntuacionesRecibidas() throws DaoException{
		chargeAuthentication();
		when(puntuacionServicio.obtenerPuntuacionesRecibidas(escritor.getIdUsuario())).thenReturn(puntuaciones);
		String vistaEsperada = "puntuacionesRecibidas";
		
		ModelAndView vista = controlador.obtenerListaPuntuacionesRecibidas();
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Puntuacion> puntuacionResultante = (List<Puntuacion>) vista.getModelMap().get("puntuacionesRecibidas");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(puntuaciones,puntuacionResultante);
	}
	
	@Test
	public void abrirHistoriaPuntuacionesRealizadas() throws DaoException{
		chargeAuthentication();
		when(puntuacionServicio.obtenerPuntuacionesRealizadas(escritor.getIdUsuario())).thenReturn(puntuacionesRealizadas);
		String vistaEsperada = "puntuacionesRealizadas";
		
		ModelAndView vista = controlador.obtenerListaPuntuacionesRealizadas();
		String vistaRecibida = vista.getViewName();
		@SuppressWarnings("unchecked")
		List<Puntuacion> puntuacionResultante = (List<Puntuacion>) vista.getModelMap().get("puntuacionesRealizadas");
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada,vistaRecibida);
		assertEquals(nombre,nombreRecibido);
		assertEquals(puntuacionesRealizadas,puntuacionResultante);
	}
	
	private void chargeAuthentication() throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(escritor);
	}
}
