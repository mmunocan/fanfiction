package com.ubb.fanfiction.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.EmailException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.ContraseñaForm;
import com.ubb.fanfiction.form.CorreoForm;
import com.ubb.fanfiction.form.CuentaUsuarioForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.FanfictionServicio;
import com.ubb.fanfiction.service.LecturaServicio;
import com.ubb.fanfiction.service.MensajeCorreoServicio;
import com.ubb.fanfiction.service.UsuarioServicio;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class UsuarioControllerTests {
	private Usuario usuarioRegistrado;
	private Integer idUsuarioRegistrado;
	private String nombreRegistrado;
	private String correoRegistrado;
	private Usuario usuarioCompleto;
	private CuentaUsuarioForm formulario;
	private BindingResult resultado;
	private CorreoForm correo;
	private BindingResult resultadoCorreo;
	
	@Mock
	private SecurityContext security;
	@Mock
	private Authentication auth;
	@Mock
	private HttpSession sesion;
	
	@Mock
	private LecturaServicio lecturaServicio;
	@Mock
	private FanfictionServicio fanfictionServicio;
	
	@Mock
	private UsuarioServicio usuarioServicio;
	@Mock
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Mock
	private Usuario usuarioMock;
	@Mock
	private MensajeCorreoServicio mensajeCorreoServicio;
	
	@InjectMocks
	public UsuarioController controlador = new UsuarioController(); 
	
	@Before
	public void setUp(){
		idUsuarioRegistrado = 2;
		nombreRegistrado = "mmunocan";
		correoRegistrado = "mmunocan@gmail.com";
		String claveRegistrado = "c14v3";
		Calendar mañana = Calendar.getInstance();
		mañana.add(Calendar.HOUR, 24);
		
		usuarioRegistrado = new Usuario();
		usuarioRegistrado.setIdUsuario(idUsuarioRegistrado);
		usuarioRegistrado.setNombre(nombreRegistrado);
		usuarioRegistrado.setCorreo(correoRegistrado);
		usuarioRegistrado.setClave(claveRegistrado);
		usuarioRegistrado.setRol("USER");
		usuarioRegistrado.setEstado(true);
		usuarioRegistrado.setFechaCreacion(Calendar.getInstance().getTime());
		usuarioRegistrado.setDescripcion("Hola a todos!!!");
		usuarioRegistrado.setVencimientoToken(mañana.getTime());
		usuarioRegistrado.setCorreoActivo(true);
		when(usuarioMock.getIdUsuario()).thenReturn(usuarioRegistrado.getIdUsuario());
		when(usuarioMock.getNombre()).thenReturn(usuarioRegistrado.getNombre());
		when(usuarioMock.getCorreo()).thenReturn(usuarioRegistrado.getCorreo());
		when(usuarioMock.getClave()).thenReturn(usuarioRegistrado.getClave());
		when(usuarioMock.getRol()).thenReturn(usuarioRegistrado.getRol());
		when(usuarioMock.isEstado()).thenReturn(usuarioRegistrado.isEstado());
		when(usuarioMock.getDescripcion()).thenReturn(usuarioRegistrado.getDescripcion());
		when(usuarioMock.isCorreoActivo()).thenReturn(true);
		
		ClasificacionParental clasificacion = new ClasificacionParental("K (5+)");
		Date fecha = Calendar.getInstance().getTime();
		
		Fanfiction fanfiction = new Fanfiction(clasificacion, new Usuario(), "Una historia infinita", "Espero poder actualizar a diario", fecha, fecha, "Incompleto");
		
		Publicacion fanfictionCompleto = new Publicacion();
		fanfictionCompleto.setFanfiction(fanfiction);
		fanfictionCompleto.setCantidadComentarios((long) 123);
		fanfictionCompleto.setPromedioPuntuacion(12.1);
		
		usuarioCompleto = new Usuario();
		usuarioCompleto.setIdUsuario(4);
		usuarioCompleto.setNombre("Isaac");
		usuarioCompleto.setCorreo("isaack@hormai.com");
		usuarioCompleto.setClave("1223");
		usuarioCompleto.setRol("USER");
		usuarioCompleto.setEstado(true);
		
		formulario = new CuentaUsuarioForm();
		formulario.setNombre("Martita");
		formulario.setCorreo("martita.chile@hotmail.com");
		formulario.setClave("holaMundo");
		formulario.setClave2("holaMundo");
		formulario.setDescripcion("Cuenta editada");
		formulario.setPath("www.zonadefics.cl");
		
		resultado = new BeanPropertyBindingResult(formulario, "cuentaUsuarioForm");
		
		correo = new CorreoForm();
		correo.setCorreo(correoRegistrado);
		correo.setPath("www.zonadefics.cl");
		resultadoCorreo = new BeanPropertyBindingResult(formulario, "correoForm");
		
	}

	@Test
	public void abrirPaginaConfigurarUsuarioDeFormaExitosa() throws DaoException{
		chargeAuthentication(correoRegistrado, usuarioRegistrado);
		String vistaEsperada = "editarUsuario";
		
		ModelAndView vista = controlador.modificarCuentaUsuario();
		String vistaRecibida = vista.getViewName();
		String nombreRecibido = (String) vista.getModelMap().get("nombreUsuario");
		CuentaUsuarioForm formulario = (CuentaUsuarioForm) vista.getModelMap().get("cuentaUsuarioForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombreRegistrado,nombreRecibido);
		assertNotNull(formulario);
		assertNotNull(formulario.getNombre());
		assertNotNull(formulario.getCorreo());
	}
	
	
	@Test
	public void desactivarCuentaDeUsuarioConLecturasYFanfictionsDeFormaExitosa() throws DaoException{
		chargeAuthentication(usuarioCompleto.getCorreo(), usuarioCompleto);
		String vistaEsperada = "redirect:/logout";
		
		ModelAndView vista = controlador.desactivarCuentaUsuario();
		String vistaRecibida = vista.getViewName();
		
		verify(usuarioServicio).desactivarUsuario(usuarioCompleto);
		verify(fanfictionServicio).eliminarListaFanfictions(usuarioCompleto.getFanfictions());
		verify(lecturaServicio).eliminarListaLecturas(usuarioCompleto.getLecturas());
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void guardarCambiosSinNombreNiCorreo() throws DaoException{
		chargeAuthentication(correoRegistrado, usuarioMock);
		formulario.setCorreo(correoRegistrado);
		formulario.setClave("");
		formulario.setClave2("");
		when(usuarioServicio.buscarOtroUsuarioPorNombre(idUsuarioRegistrado, formulario.getNombre())).thenReturn(null);
		when(usuarioServicio.buscarOtroUsuarioPorCorreo(idUsuarioRegistrado, formulario.getCorreo())).thenReturn(null);
		when(bCryptPasswordEncoder.matches(formulario.getClaveAntigua(), usuarioRegistrado.getClave())).thenReturn(true);
		String vistaEsperada = "userhome";
		
		ModelAndView vista = controlador.guardarCambiosEdicionUsuario(formulario,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(formulario.getNombre(), nombreUsuario);
		assertEquals("Cambios guardados", mensajeExito);
		verify(usuarioServicio).editarUsuario(usuarioMock);
		verify(usuarioMock).setNombre(formulario.getNombre());
		verify(usuarioMock).setDescripcion(formulario.getDescripcion());
		verify(usuarioMock).setClave(formulario.getClaveAntigua());
	}
	
	@Test
	public void guardarCambiosCorreoUsuario() throws DaoException, EmailException{
		chargeAuthentication(correoRegistrado, usuarioMock);
		formulario.setClave("");
		formulario.setClave2("");
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.generarToken()).thenReturn(token);
		when(usuarioServicio.buscarOtroUsuarioPorNombre(idUsuarioRegistrado, formulario.getNombre())).thenReturn(null);
		when(usuarioServicio.buscarOtroUsuarioPorCorreo(idUsuarioRegistrado, formulario.getCorreo())).thenReturn(null);
		when(bCryptPasswordEncoder.matches(formulario.getClaveAntigua(), usuarioRegistrado.getClave())).thenReturn(true);
		String vistaEsperada = "userhome";
		
		ModelAndView vista = controlador.guardarCambiosEdicionUsuario(formulario,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		
		assertNotNull(vista);
		verify(mensajeCorreoServicio).enviarCorreoCambioCorreoCuenta(formulario.getPath(),usuarioRegistrado.getIdUsuario(),token, formulario.getCorreo());
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(formulario.getNombre(), nombreUsuario);
		assertEquals("Cambios guardados. Podrá iniciar sesión con su nuevo correo cuando sea activado. Si no llega un correo, intente nuevamente.", mensajeExito);
		verify(usuarioServicio).editarUsuario(usuarioMock);
		verify(usuarioMock).setNombre(formulario.getNombre());
		verify(usuarioMock).setDescripcion(formulario.getDescripcion());
		verify(usuarioMock).setToken(token);
		verify(usuarioMock).setNuevoCorreo(formulario.getCorreo());
	}
	
	@Test
	public void errorAlEnviarCorreoValidacionCorreoNuevo() throws DaoException, EmailException{
		chargeAuthentication(correoRegistrado, usuarioMock);
		formulario.setClave("");
		formulario.setClave2("");
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.generarToken()).thenReturn(token);
		when(usuarioServicio.buscarOtroUsuarioPorNombre(idUsuarioRegistrado, formulario.getNombre())).thenReturn(null);
		when(usuarioServicio.buscarOtroUsuarioPorCorreo(idUsuarioRegistrado, formulario.getCorreo())).thenReturn(null);
		when(bCryptPasswordEncoder.matches(formulario.getClaveAntigua(), usuarioRegistrado.getClave())).thenReturn(true);
		doThrow(EmailException.class).when(mensajeCorreoServicio).enviarCorreoCambioCorreoCuenta(formulario.getPath(),usuarioRegistrado.getIdUsuario(),token, formulario.getCorreo());
		String vistaEsperada = "editarUsuario";
		
		ModelAndView vista = controlador.guardarCambiosEdicionUsuario(formulario,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
		CuentaUsuarioForm formulario = (CuentaUsuarioForm) vista.getModelMap().get("cuentaUsuarioForm");
		String mensaje = (String) vista.getModelMap().get("mensajeError");
		
		assertNotNull(vista);
		assertNotNull(mensaje);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombreRegistrado, nombreUsuario);
		assertNotNull(formulario);
		assertNotNull(formulario.getNombre());
		assertNotNull(formulario.getCorreo());
	}
	
	@Test
	public void guardarCambiosContraseñaUsuario() throws DaoException{
		chargeAuthentication(correoRegistrado, usuarioMock);
		formulario.setCorreo(correoRegistrado);
		when(usuarioServicio.buscarOtroUsuarioPorNombre(idUsuarioRegistrado, formulario.getNombre())).thenReturn(null);
		when(usuarioServicio.buscarOtroUsuarioPorCorreo(idUsuarioRegistrado, formulario.getCorreo())).thenReturn(null);
		when(bCryptPasswordEncoder.matches(formulario.getClaveAntigua(), usuarioRegistrado.getClave())).thenReturn(true);
		String vistaEsperada = "userhome";
		
		ModelAndView vista = controlador.guardarCambiosEdicionUsuario(formulario,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
		String mensajeExito = (String) vista.getModelMap().get("mensaje");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(formulario.getNombre(), nombreUsuario);
		assertEquals("Cambios guardados", mensajeExito);
		verify(usuarioServicio).editarUsuario(usuarioMock);
		verify(usuarioMock).setNombre(formulario.getNombre());
		verify(usuarioMock).setDescripcion(formulario.getDescripcion());
		verify(usuarioMock).setClave(formulario.getClave());
	}
	
	@Test
	public void guardarCambiosErroneoPorErrorEnCorreoAntiguo() throws DaoException{
		chargeAuthentication(correoRegistrado, usuarioRegistrado);
		when(usuarioServicio.buscarOtroUsuarioPorNombre(idUsuarioRegistrado, formulario.getNombre())).thenReturn(null);
		when(usuarioServicio.buscarOtroUsuarioPorCorreo(idUsuarioRegistrado, formulario.getCorreo())).thenReturn(null);
		when(bCryptPasswordEncoder.matches(formulario.getClaveAntigua(), usuarioRegistrado.getClave())).thenReturn(false);
		String vistaEsperada = "editarUsuario";
		
		ModelAndView vista = controlador.guardarCambiosEdicionUsuario(formulario,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
		CuentaUsuarioForm formulario = (CuentaUsuarioForm) vista.getModelMap().get("cuentaUsuarioForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombreRegistrado, nombreUsuario);
		assertNotNull(formulario);
		assertNotNull(formulario.getNombre());
		assertNotNull(formulario.getCorreo());
	}
	
	@Test
	public void guardarCaambioNombreErroneoPorNombreExistente() throws DaoException{
		chargeAuthentication(correoRegistrado, usuarioRegistrado);
		when(usuarioServicio.buscarOtroUsuarioPorNombre(idUsuarioRegistrado, formulario.getNombre())).thenReturn(usuarioCompleto);
		when(usuarioServicio.buscarOtroUsuarioPorCorreo(idUsuarioRegistrado, formulario.getCorreo())).thenReturn(null);
		when(bCryptPasswordEncoder.matches(formulario.getClaveAntigua(), usuarioRegistrado.getClave())).thenReturn(true);
		String vistaEsperada = "editarUsuario";
		
		ModelAndView vista = controlador.guardarCambiosEdicionUsuario(formulario,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
		CuentaUsuarioForm formulario = (CuentaUsuarioForm) vista.getModelMap().get("cuentaUsuarioForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombreRegistrado, nombreUsuario);
		assertNotNull(formulario);
		assertNotNull(formulario.getNombre());
		assertNotNull(formulario.getCorreo());
	}
	
	@Test
	public void guardarCambioCorreoErroneoPorCorreoExistente() throws DaoException{
		chargeAuthentication(correoRegistrado, usuarioRegistrado);
		when(usuarioServicio.buscarOtroUsuarioPorNombre(idUsuarioRegistrado, formulario.getNombre())).thenReturn(null);
		when(usuarioServicio.buscarOtroUsuarioPorCorreo(idUsuarioRegistrado, formulario.getCorreo())).thenReturn(usuarioCompleto);
		when(bCryptPasswordEncoder.matches(formulario.getClaveAntigua(), usuarioRegistrado.getClave())).thenReturn(true);
		
		String vistaEsperada = "editarUsuario";
		
		ModelAndView vista = controlador.guardarCambiosEdicionUsuario(formulario,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
		CuentaUsuarioForm formulario = (CuentaUsuarioForm) vista.getModelMap().get("cuentaUsuarioForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombreRegistrado, nombreUsuario);
		assertNotNull(formulario);
		assertNotNull(formulario.getNombre());
		assertNotNull(formulario.getCorreo());
	}
	
	@Test
	public void guardarCambiosFallidoPorErrorEnFormulario() throws DaoException{
		chargeAuthentication(correoRegistrado, usuarioRegistrado);
		resultado.reject("nombre", "error.nombre");
		chargeAuthentication(correoRegistrado, usuarioRegistrado);
		when(usuarioServicio.buscarOtroUsuarioPorNombre(idUsuarioRegistrado, formulario.getNombre())).thenReturn(null);
		when(usuarioServicio.buscarOtroUsuarioPorCorreo(idUsuarioRegistrado, formulario.getCorreo())).thenReturn(null);
		when(bCryptPasswordEncoder.matches(formulario.getClaveAntigua(), usuarioRegistrado.getClave())).thenReturn(true);
		String vistaEsperada = "editarUsuario";
		
		ModelAndView vista = controlador.guardarCambiosEdicionUsuario(formulario,resultado);
		String vistaRecibida = vista.getViewName();
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
		CuentaUsuarioForm formulario = (CuentaUsuarioForm) vista.getModelMap().get("cuentaUsuarioForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(nombreRegistrado, nombreUsuario);
		assertNotNull(formulario);
		assertNotNull(formulario.getNombre());
		assertNotNull(formulario.getCorreo());
	}
		
	@Test
	public void validarCuentaDeFormaExitosaUsuarioNoSesion() throws ObjectNotFoundException, DaoException{
		chargeAuthentication(correoRegistrado, null);
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		int idUsuario = idUsuarioRegistrado;
		when(usuarioServicio.validarToken(token,idUsuario)).thenReturn(true);
		when(usuarioServicio.buscarUsuarioPorId(idUsuario)).thenReturn(usuarioRegistrado);
		String vistaEsperada = "login";
		
		ModelAndView vista = controlador.validarCuenta(token, idUsuario);
		String vistaRecibida = vista.getViewName();
		String mensaje = (String) vista.getModelMap().get("mensajeExito");
		
		verify(usuarioServicio).validarCuenta(usuarioRegistrado);
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertNotNull(mensaje);
	}
	
	@Test
	public void validarCuentaDeFormaExitosaUsuarioSesionInciada() throws ObjectNotFoundException, DaoException{
		chargeAuthentication(correoRegistrado, usuarioRegistrado);
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		int idUsuario = idUsuarioRegistrado;
		when(usuarioServicio.validarToken(token,idUsuario)).thenReturn(true);
		when(usuarioServicio.buscarUsuarioPorId(idUsuario)).thenReturn(usuarioRegistrado);
		String vistaEsperada = "cerrarSesion";
		
		ModelAndView vista = controlador.validarCuenta(token, idUsuario);
		String vistaRecibida = vista.getViewName();
		String mensaje = (String) vista.getModelMap().get("mensajeExito");
		String nombreUsuario = (String) vista.getModelMap().get("nombreUsuario");
				
		verify(usuarioServicio).validarCuenta(usuarioRegistrado);
		assertNotNull(vista);
		assertEquals(nombreRegistrado, nombreUsuario);
		assertEquals(vistaEsperada, vistaRecibida);
		assertNotNull(mensaje);
	}
	
	@Test
	public void validarCuentaDeFormaFallidaPorURLInvalida() throws ObjectNotFoundException, DaoException{
		chargeAuthentication(correoRegistrado, null);
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		int idUsuario = idUsuarioRegistrado;
		when(usuarioServicio.validarToken(token,idUsuario)).thenReturn(false);
		String vistaEsperada = "enlaceNoValido";
		
		ModelAndView vista = controlador.validarCuenta(token, idUsuario);
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		
	}
	
	@Test
	public void ingresarARecuperarContraseñaExitoso(){
		String vistaEsperada = "recuperarContraseña";
		
		ModelAndView vista = controlador.recibirCorreoRecuperacionContraseña();
		CorreoForm formulario = (CorreoForm) vista.getModelMap().get("correoForm");
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertNotNull(formulario);
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void enviarEnlaceRecuperacionContraseñaExitoso() throws DaoException, EmailException{
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.buscarUsuarioPorCorreo(correoRegistrado)).thenReturn(usuarioMock);
		when(usuarioServicio.generarToken()).thenReturn(token);
		String vistaEsperada = "login";
		
		ModelAndView vista = controlador.generarEnlaceRecuperacion(correo, resultadoCorreo);
		String vistaRecibida = vista.getViewName();
		String mensaje = (String) vista.getModelMap().get("mensajeExito");
		
		assertNotNull(vista);
		assertNotNull(mensaje);
		assertEquals(vistaEsperada, vistaRecibida);
		verify(usuarioMock).setToken(token);
		verify(usuarioMock).setVencimientoToken(any(Date.class));
		verify(usuarioServicio).guardarUsuarioDirecto(usuarioMock);
		verify(mensajeCorreoServicio).enviarCorreoRecuperacionContraseña(correo.getPath(), idUsuarioRegistrado, token, correoRegistrado);
	}
	
	@Test
	public void envioEnlaCeRecuperacionCuentaErroneoPorNoPoderEnviarCorreo() throws DaoException, EmailException{
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.buscarUsuarioPorCorreo(correoRegistrado)).thenReturn(usuarioMock);
		when(usuarioServicio.generarToken()).thenReturn(token);
		doThrow(EmailException.class).when(mensajeCorreoServicio).enviarCorreoRecuperacionContraseña(correo.getPath(), idUsuarioRegistrado, token, correoRegistrado);
		String vistaEsperada = "recuperarContraseña";
		
		ModelAndView vista = controlador.generarEnlaceRecuperacion(correo, resultadoCorreo);
		String vistaRecibida = vista.getViewName();
		CorreoForm formulario = (CorreoForm) vista.getModelMap().get("correoForm");
		String mensaje = (String) vista.getModelMap().get("mensajeError");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertNotNull(formulario);
		assertNotNull(mensaje);
	}
	
	@Test
	public void enviarEnlaceRecuperacionFallidoPorCuentaNoValidada() throws DaoException{
		usuarioRegistrado.setCorreoActivo(false);
		when(usuarioServicio.buscarUsuarioPorCorreo(correoRegistrado)).thenReturn(usuarioRegistrado);
		String vistaEsperada = "recuperarContraseña";
		
		ModelAndView vista = controlador.generarEnlaceRecuperacion(correo, resultadoCorreo);
		String vistaRecibida = vista.getViewName();
		CorreoForm formulario = (CorreoForm) vista.getModelMap().get("correoForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertNotNull(formulario);
	}
	
	@Test
	public void enviarEnlaceRecuperacionContraseñaFallidoPorCorreoNoRegistrado() throws DaoException{
		when(usuarioServicio.buscarUsuarioPorCorreo(correoRegistrado)).thenReturn(null);
		String vistaEsperada = "recuperarContraseña";
		
		ModelAndView vista = controlador.generarEnlaceRecuperacion(correo, resultadoCorreo);
		String vistaRecibida = vista.getViewName();
		CorreoForm formulario = (CorreoForm) vista.getModelMap().get("correoForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertNotNull(formulario);
	}
	
	@Test
	public void enviarEnlaceRecuperacionContraseñaFallidoPorErrorEnFormulario() throws DaoException{
		resultadoCorreo.reject("correo", "error.correo");
		when(usuarioServicio.buscarUsuarioPorCorreo(correoRegistrado)).thenReturn(usuarioRegistrado);
		String vistaEsperada = "recuperarContraseña";
		
		ModelAndView vista = controlador.generarEnlaceRecuperacion(correo, resultadoCorreo);
		String vistaRecibida = vista.getViewName();
		CorreoForm formulario = (CorreoForm) vista.getModelMap().get("correoForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertNotNull(formulario);
	}
	
	@Test
	public void recibirEnlaceRecuperacionContraseñaDeFormaExitosa() throws ObjectNotFoundException, DaoException{
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.validarToken(token, idUsuarioRegistrado)).thenReturn(true);
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioRegistrado)).thenReturn(usuarioRegistrado);
		String vistaEsperada = "recibirContraseña";
		
		ModelAndView vista = controlador.recibirEnlaceRecuperacionContraseña(idUsuarioRegistrado,token);
		String vistaRecibida = vista.getViewName();
		ContraseñaForm formulario = (ContraseñaForm) vista.getModelMap().get("contrasenaForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertNotNull(formulario);
		assertEquals(idUsuarioRegistrado,formulario.getIdUsuario());
	}
	
	@Test
	public void recibirEnlaceRecuperacionContraseñaFallidoPorEnlaceNoValido() throws DaoException, ObjectNotFoundException{
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.validarToken(token, idUsuarioRegistrado)).thenReturn(false);
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioRegistrado)).thenReturn(usuarioRegistrado);
		String vistaEsperada = "enlaceNoValido";
		
		ModelAndView vista = controlador.recibirEnlaceRecuperacionContraseña(idUsuarioRegistrado,token);
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		
	}
	
	@Test
	public void recibirEnlaceRecuperacionContraseñaVencido() throws DaoException, ObjectNotFoundException{
		Calendar ayer = Calendar.getInstance();
		ayer.add(Calendar.HOUR, -24);
		usuarioRegistrado.setVencimientoToken(ayer.getTime());
		String token = "24d53599-9f99-49d0-a704-244924b5d18b";
		when(usuarioServicio.validarToken(token, idUsuarioRegistrado)).thenReturn(true);
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioRegistrado)).thenReturn(usuarioRegistrado);
		String vistaEsperada = "enlaceVencido";
		
		ModelAndView vista = controlador.recibirEnlaceRecuperacionContraseña(idUsuarioRegistrado,token);
		String vistaRecibida = vista.getViewName();
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
	}
	
	@Test
	public void cambiarContraseñaDeFormaCorrecta() throws ObjectNotFoundException, DaoException{
		ContraseñaForm contraseña = new ContraseñaForm();
		contraseña.setClave("holaMundoDeNuevoParaTodosMisAmigos");
		contraseña.setIdUsuario(idUsuarioRegistrado);
		BindingResult resultado = new BeanPropertyBindingResult(contraseña, "contrasenaForm");
		when(usuarioServicio.buscarUsuarioPorId(idUsuarioRegistrado)).thenReturn(usuarioRegistrado);
		String vistaEsperada = "login";
		
		ModelAndView vista = controlador.restablecerContraseña(contraseña, resultado);
		String vistaRecibida = vista.getViewName();
		String mensaje = (String) vista.getModelMap().get("mensajeExito");
		
		verify(usuarioServicio).restablecerContraseña(usuarioRegistrado,contraseña.getClave());
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertNotNull(mensaje);
	}
	
	@Test
	public void cambiarContraseñaDeFormaFallidaPorErrorEnFormulario() throws ObjectNotFoundException, DaoException{
		ContraseñaForm contraseña = new ContraseñaForm();
		contraseña.setClave("ho");
		contraseña.setIdUsuario(idUsuarioRegistrado);
		BindingResult resultado = new BeanPropertyBindingResult(contraseña, "contrasenaForm");
		resultado.rejectValue("clave", "error.clave");
		String vistaEsperada = "recibirContraseña";
		
		ModelAndView vista = controlador.restablecerContraseña(contraseña, resultado);
		String vistaRecibida = vista.getViewName();
		ContraseñaForm formulario = (ContraseñaForm) vista.getModelMap().get("contrasenaForm");
		
		assertNotNull(vista);
		assertEquals(vistaEsperada, vistaRecibida);
		assertEquals(contraseña, formulario);
	}
	
	private void chargeAuthentication(String correo, Usuario usuario) throws DaoException{
		PowerMockito.mockStatic(SecurityContextHolder.class);
		PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(security);
		when(security.getAuthentication()).thenReturn(auth);
		when(auth.getName()).thenReturn(correo);
		when(usuarioServicio.buscarUsuarioPorCorreo(correo)).thenReturn(usuario);
	}
}
