package com.ubb.fanfiction.model;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Usuario;

@RunWith(MockitoJUnitRunner.class)
public class PublicacionComparatorTests {
	private Publicacion publicacion1;
	private Publicacion publicacion2;
	private Publicacion publicacion3;
	
	@Before
	public void setUp(){
		Date fecha = Calendar.getInstance().getTime();
		Usuario usuario = new Usuario();
		usuario.setIdUsuario(2);
		usuario.setNombre("mmmunocan");
		usuario.setCorreo("mmunocan@gmail.com");
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		ClasificacionParental clasificacionParental = new ClasificacionParental();
		clasificacionParental.setIdClasificacionParental(1);
		clasificacionParental.setNombre("K+(+9)");
		
		Fanfiction fanfiction1 = new Fanfiction(clasificacionParental,usuario,"Fanfiction A","Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ",
				fecha, fecha, "Incompleto");
		Set<Capitulo> capitulos1 = new HashSet<Capitulo>();
		capitulos1.add(new Capitulo(fanfiction1, 1, "Capítulo 1", fecha, fecha,	"Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... "));
		fanfiction1.setCapitulos(capitulos1);
		publicacion1 = new Publicacion();
		publicacion1.setFanfiction(fanfiction1);
		publicacion1.setCantidadComentarios((long) 2);
		publicacion1.setPromedioPuntuacion(5.0);
		Fanfiction fanfiction2 = new Fanfiction(clasificacionParental,usuario,"Fanfiction B","Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ",
				fecha, fecha, "Incompleto");
		Set<Capitulo> capitulos2 = new HashSet<Capitulo>();
		capitulos2.add(new Capitulo(fanfiction2, 1, "Capítulo 1", fecha, fecha,	"Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... "));
		capitulos2.add(new Capitulo(fanfiction2, 2, "Capítulo 2", fecha, fecha,	"Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... "));
		fanfiction2.setCapitulos(capitulos2);
		publicacion2 = new Publicacion();
		publicacion2.setFanfiction(fanfiction2);
		publicacion2.setCantidadComentarios((long) 5);
		publicacion2.setPromedioPuntuacion(5.0);
		Fanfiction fanfiction3 = new Fanfiction(clasificacionParental,usuario,"Fanfiction c","Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ",
				fecha, fecha, "Incompleto");
		Set<Capitulo> capitulos3 = new HashSet<Capitulo>();
		capitulos3.add(new Capitulo(fanfiction3, 1, "Capítulo 1", fecha, fecha,	"Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... "));
		capitulos3.add(new Capitulo(fanfiction3, 2, "Capítulo 2", fecha, fecha,	"Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... "));
		capitulos3.add(new Capitulo(fanfiction3, 3, "Capítulo 3", fecha, fecha,	"Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... "));
		fanfiction3.setCapitulos(capitulos3);
		publicacion3 = new Publicacion();
		publicacion3.setFanfiction(fanfiction3);
		publicacion3.setCantidadComentarios((long) 8);
		publicacion3.setPromedioPuntuacion(4.0);
		
		
	}
	
	@Test
	public void ordenarPorNumeroCapitulosAscendente(){
		PublicacionComparator comparador = new PublicacionComparator(PublicacionComparator.CAPITULOS, PublicacionComparator.ASC);
		
		int resultado = comparador.compare(publicacion1, publicacion2);
		int resultado2 = comparador.compare(publicacion1, publicacion3);
		
		assertEquals(-1, resultado);
		assertEquals(-2, resultado2);
	}
	
	@Test
	public void ordenarPorNumeroCapitulosDescendente(){
		PublicacionComparator comparador = new PublicacionComparator(PublicacionComparator.CAPITULOS, PublicacionComparator.DESC);
		
		int resultado = comparador.compare(publicacion1, publicacion2);
		int resultado2 = comparador.compare(publicacion1, publicacion3);
		
		assertEquals(1, resultado);
		assertEquals(2, resultado2);
	}
	
	@Test
	public void ordenarPorNumeroComentariosAscendente(){
		PublicacionComparator comparador = new PublicacionComparator(PublicacionComparator.COMENTARIO, PublicacionComparator.ASC);
		
		int resultado = comparador.compare(publicacion1, publicacion2);
		int resultado2 = comparador.compare(publicacion1, publicacion3);
		
		assertEquals(-3, resultado);
		assertEquals(-6, resultado2);
	}
	
	@Test
	public void ordenarPorNumeroComentariosDescendente(){
		PublicacionComparator comparador = new PublicacionComparator(PublicacionComparator.COMENTARIO, PublicacionComparator.DESC);
		
		int resultado = comparador.compare(publicacion1, publicacion2);
		int resultado2 = comparador.compare(publicacion1, publicacion3);
		
		assertEquals(3, resultado);
		assertEquals(6, resultado2);
	}
	
	@Test
	public void ordenarPorPuntuacionPromedioAscendente(){
		PublicacionComparator comparador = new PublicacionComparator(PublicacionComparator.PUNTUACION, PublicacionComparator.ASC);
		
		int resultado = comparador.compare(publicacion1, publicacion2);
		int resultado2 = comparador.compare(publicacion1, publicacion3);
		
		assertTrue(resultado == 0);
		assertTrue(resultado2 > 0);
	}
	
	@Test
	public void ordenarPorPuntuacionPromedioDescendente(){
		PublicacionComparator comparador = new PublicacionComparator(PublicacionComparator.PUNTUACION, PublicacionComparator.DESC);
		
		int resultado = comparador.compare(publicacion1, publicacion2);
		int resultado2 = comparador.compare(publicacion1, publicacion3);
		
		assertTrue(resultado == 0);
		assertTrue(resultado2 < 0);
	}
}
