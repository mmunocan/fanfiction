package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.dao.ClasificacionParentalDao;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.impl.ClasificacionParentalServicioImpl;

@RunWith(MockitoJUnitRunner.class)
public class ClasificacionParentalServicioTests {
	private Integer id;
	private ClasificacionParental clasificacionParental1;
	private ClasificacionParental clasificacionParental2;
	private List<ClasificacionParental> listaClasificaciones; 

	@Mock
	private ClasificacionParentalDao clasificacionParentalDao;
	
	@InjectMocks
	private ClasificacionParentalServicio clasificacionParentalServicio = new ClasificacionParentalServicioImpl();
	
	@Before
	public void setUp(){
		id = 1;
		clasificacionParental1 = new ClasificacionParental();
		clasificacionParental1.setIdClasificacionParental(id);
		clasificacionParental1.setNombre("K (5+)");
		
		clasificacionParental2 = new ClasificacionParental();
		clasificacionParental2.setIdClasificacionParental(2);
		clasificacionParental2.setNombre("K+ (9+)");
		
		listaClasificaciones = new ArrayList<ClasificacionParental>();
		listaClasificaciones.add(clasificacionParental1);
		listaClasificaciones.add(clasificacionParental2);
		
	}
	
	@Test
	public void busquedaClasificacionParentalPorIdDeFormaExitosa() throws ObjectNotFoundException, DaoException{
		when(clasificacionParentalDao.findOne(id)).thenReturn(clasificacionParental1);
		
		ClasificacionParental clasificacionRecibida = clasificacionParentalServicio.buscarClasificacionParentalPorId(id);
		
		assertEquals(clasificacionParental1, clasificacionRecibida);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void busquedaClasificacionParentalPorIdSinResultados() throws ObjectNotFoundException, DaoException{
		clasificacionParental1 = null;
		when(clasificacionParentalDao.findOne(id)).thenReturn(clasificacionParental1);
		
		clasificacionParentalServicio.buscarClasificacionParentalPorId(id);
		
	}
	
	@Test(expected = DaoException.class)
	public void busquedaClasificacionParentalPorIdFallidaPorErrorEnCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(clasificacionParentalDao).findOne(id);
		
		clasificacionParentalServicio.buscarClasificacionParentalPorId(id);
	}
	
	@Test
	public void obtenerClasificacionesParentales() throws DaoException{
		when(clasificacionParentalDao.findAll()).thenReturn(listaClasificaciones);
		
		List<ClasificacionParental> resultado = clasificacionParentalServicio.obtenerClasificacionesParentales();
		
		assertNotNull(resultado);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerClasificacionesParentalesFallidoPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(clasificacionParentalDao).findAll();
		
		clasificacionParentalServicio.obtenerClasificacionesParentales();
		
	}
}
