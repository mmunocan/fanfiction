package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.dao.CapituloDao;
import com.ubb.fanfiction.dao.ComentarioDao;
import com.ubb.fanfiction.dao.FanfictionDao;
import com.ubb.fanfiction.dao.PuntuacionDao;
import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.model.CapituloPublicacion;
import com.ubb.fanfiction.service.impl.CapituloServicioImpl;

@RunWith(MockitoJUnitRunner.class)
public class CapituloServicioTests {
	private Integer idCapitulo;
	private Integer idFanfiction;
	private Capitulo capitulo;
	private Fanfiction fanfiction;
	private List<Capitulo> capitulosDelFanfiction;
	private Integer idUsuario;
	private Capitulo segundo;
	
	@Mock
	private PuntuacionDao puntuacionDao;
	
	@Mock
	private ComentarioDao comentarioDao;
	
	@Mock
	private CapituloDao capituloDao;
	
	@Mock
	private FanfictionDao fanfictionDao;
	
	@InjectMocks
	private CapituloServicio capituloServicio = new CapituloServicioImpl();
	
	@Before
	public void setUp(){
		Date fechaCreacion = Calendar.getInstance().getTime();
		ClasificacionParental clasificacion = new ClasificacionParental("K (5+)");
		Date fechaActualizacion = Calendar.getInstance().getTime();
		idCapitulo = 1;
		idFanfiction = 1;
		
		idUsuario = 2;
		
		capitulo = new Capitulo();
		capitulo.setIdCapitulo(idCapitulo);
		capitulo.setNumero(1);
		capitulo.setTitulo("Una carta amenazante");
		capitulo.setFechaPublicacion(fechaCreacion);
		capitulo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		segundo = new Capitulo();
		segundo.setIdCapitulo(3);
		segundo.setNumero(2);
		segundo.setTitulo("El secreto explosivo");
		segundo.setFechaPublicacion(fechaCreacion);
		segundo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(fechaCreacion);
		fanfiction.setFechaActualizacion(fechaActualizacion);
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(new Usuario("mmunocan","mmunocan@hotmail.com","c14v3","USER",true,fechaCreacion, true));
		fanfiction.setClasificacionParental(clasificacion);
		capitulo.setFanfiction(fanfiction);
		segundo.setFanfiction(fanfiction);
		HashSet<Capitulo> listaCapitulos = new HashSet<Capitulo>();
		listaCapitulos.add(capitulo);
		listaCapitulos.add(segundo);
		fanfiction.setCapitulos(listaCapitulos);
		
		capitulosDelFanfiction = new ArrayList<Capitulo>();
		capitulosDelFanfiction.add(capitulo);
		capitulosDelFanfiction.add(segundo);
	}
	
	@Test
	public void guardarCapituloDeFormaExitosa() throws DaoException{
		Calendar hoy = Calendar.getInstance();
		Calendar publicacion = Calendar.getInstance();
		Calendar actualizacion = Calendar.getInstance();
		
		capituloServicio.guardarCapitulo(capitulo);
		publicacion.setTime(capitulo.getFechaPublicacion());
		actualizacion.setTime(capitulo.getFechaActualizacion());
		
		assertEquals(hoy.get(Calendar.YEAR),publicacion.get(Calendar.YEAR));
		assertEquals(hoy.get(Calendar.MONTH),publicacion.get(Calendar.MONTH));
		assertEquals(hoy.get(Calendar.DAY_OF_MONTH),publicacion.get(Calendar.DAY_OF_MONTH));
		assertEquals(hoy.get(Calendar.YEAR),actualizacion.get(Calendar.YEAR));
		assertEquals(hoy.get(Calendar.MONTH),actualizacion.get(Calendar.MONTH));
		assertEquals(hoy.get(Calendar.DAY_OF_MONTH),actualizacion.get(Calendar.DAY_OF_MONTH));
		verify(capituloDao).save(capitulo);
		
		
	}
	
	@Test(expected = DaoException.class)
	public void errorAlGuardarCapituloPorFallaEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(capituloDao).save(capitulo);
		
		capituloServicio.guardarCapitulo(capitulo);

	}
	
	@Test
	public void errorAlGuardarPrimerCapituloPorFallaEnCapaDao() throws DaoException{
		capitulo.setNumero(1);
		boolean excepction = false;
		doThrow(Exception.class).when(capituloDao).save(capitulo);
		try{
			capituloServicio.guardarCapitulo(capitulo);
		}catch(DaoException e){
			excepction = true;
		}
		
		assertTrue(excepction);
		verify(fanfictionDao).delete(capitulo.getFanfiction());

	}
	
	@Test
	public void obtenerCapituloPorIdFanfictionYNumeroCapituloDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		int numero = capitulo.getNumero();
		when(capituloDao.findByFanfictionAndNumero(idFanfiction,numero)).thenReturn(capitulo);
		
		Capitulo recibido = capituloServicio.obtenerCapitulo(idFanfiction, numero);
		
		assertEquals(capitulo,recibido);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void obtenerCapituloPorIdFanfictionYNumeroCapituloDeFormaFallidaPorNoEncontrarCapitulo() throws ObjectNotFoundException, DaoException{
		int numero = capitulo.getNumero();
		capitulo = null;
		when(capituloDao.findByFanfictionAndNumero(idFanfiction,numero)).thenReturn(capitulo);
		
		capituloServicio.obtenerCapitulo(idFanfiction, numero);
		
	}
	
	@Test(expected = DaoException.class)
	public void obtenerCapituloPorIdFanfictionYNumeroCapituloDeFormaFallidaPorErrorEnCapaDao() throws ObjectNotFoundException, DaoException{
		int numero = capitulo.getNumero();
		doThrow(Exception.class).when(capituloDao).findByFanfictionAndNumero(idFanfiction,numero);
		
		capituloServicio.obtenerCapitulo(idFanfiction, numero);
		
	}
	
	@Test
	public void obtenerListaDeCapitulosPorFanfictionDeFormaExitosa() throws Exception{
		Double puntuacion = 3.0;
		Long comentarios = (long) 5;
		when(puntuacionDao.findAverageValor(idCapitulo)).thenReturn(puntuacion);
		when(comentarioDao.countByCapitulo(idCapitulo)).thenReturn(comentarios);
		when(capituloDao.findByFanfictionOrderByNumeroAsc(idFanfiction)).thenReturn(capitulosDelFanfiction);
		
		List<CapituloPublicacion> resultado = capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(1);
		
		assertNotNull(resultado);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListaDeCaPitulosPorFanfictionDeFormaFallidaPorErrorCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(capituloDao).findByFanfictionOrderByNumeroAsc(idFanfiction);
		
		capituloServicio.obtenerListaCapitulosDeFanfictionOrdenadosPorNumero(1);
	}
	
	@Test
	public void obtenerCapituloPorIdDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		when(capituloDao.findOne(idCapitulo)).thenReturn(capitulo);
		
		Capitulo recibido = capituloServicio.obtenerCapitulo(idCapitulo);
		
		assertEquals(capitulo,recibido);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void obtenerCapituloPorIdDeFormaFallidaPorNoEncontrarCapitulo() throws ObjectNotFoundException, DaoException{
		capitulo = null;
		when(capituloDao.findOne(idCapitulo)).thenReturn(capitulo);
		
		capituloServicio.obtenerCapitulo(idCapitulo);
		
	}
	
	@Test(expected = DaoException.class)
	public void obtenerCapituloPorIdDeFormaFallidaPorErrorEnCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(capituloDao).findOne(idCapitulo);
		
		capituloServicio.obtenerCapitulo(idCapitulo);
		
	}
	
	@Test
	public void consultarSiUsuarioEsDueñoDelCapituloVerdadero() throws DaoException{
		when(capituloDao.findByIdAndUsuario(idCapitulo,idUsuario)).thenReturn(capitulo);
		
		boolean resultado = capituloServicio.capituloEsDeUsuario(idCapitulo,idUsuario);
		
		assertTrue(resultado);
	}
	
	@Test
	public void consultarSiUsuarioEsDueñoDelCapituloFalso() throws DaoException{
		when(capituloDao.findByIdAndUsuario(idCapitulo,idUsuario)).thenReturn(null);
		
		boolean resultado = capituloServicio.capituloEsDeUsuario(idCapitulo,idUsuario);
		
		assertFalse(resultado);
	}
	
	@Test(expected = DaoException.class)
	public void consultarSiUsuarioEsDueñoDelCapituloFallidoPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(capituloDao).findByIdAndUsuario(idCapitulo,idUsuario);
		
		capituloServicio.capituloEsDeUsuario(idCapitulo,idUsuario);
	}
	
	@Test
	public void editarCapituloDeFormaExitosa() throws DaoException{
		Calendar hoy = Calendar.getInstance();
		Calendar actualizacion = Calendar.getInstance();
		
		capituloServicio.editarCapitulo(capitulo);
		actualizacion.setTime(capitulo.getFechaActualizacion());
		
		verify(capituloDao).save(capitulo);
		assertEquals(hoy.get(Calendar.YEAR),actualizacion.get(Calendar.YEAR));
		assertEquals(hoy.get(Calendar.MONTH),actualizacion.get(Calendar.MONTH));
		assertEquals(hoy.get(Calendar.DAY_OF_MONTH),actualizacion.get(Calendar.DAY_OF_MONTH));
	}
	
	@Test(expected = DaoException.class)
	public void editarCapituloDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(capituloDao).save(capitulo);
		
		capituloServicio.editarCapitulo(capitulo);

	}
	
	@Test
	public void eliminarUltimoCapituloDeFormaExitosa() throws DaoException{
		when(capituloDao.findByFanfictionOrderByNumeroAsc(idFanfiction)).thenReturn(capitulosDelFanfiction);
		
		capituloServicio.eliminarCapitulo(segundo);
		
		verify(capituloDao).borrar(segundo.getIdCapitulo());
	}
	
	@Test
	public void eliminarCapituloNoFinalDeFormaExitosa() throws DaoException{
		when(capituloDao.findByFanfictionOrderByNumeroAsc(idFanfiction)).thenReturn(capitulosDelFanfiction);
		capituloServicio.eliminarCapitulo(capitulo);
		
		verify(capituloDao).borrar(capitulo.getIdCapitulo());
		verify(capituloDao).save((Capitulo)anyObject());
	}
	
	@Test(expected = DaoException.class)
	public void eliminarCapituloDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(capituloDao).borrar(capitulo.getIdCapitulo());
		
		capituloServicio.eliminarCapitulo(capitulo);

	}
}
