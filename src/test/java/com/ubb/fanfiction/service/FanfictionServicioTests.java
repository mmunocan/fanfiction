package com.ubb.fanfiction.service;


import static org.junit.Assert.*;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ubb.fanfiction.dao.ComentarioDao;
import com.ubb.fanfiction.dao.FanfictionDao;
import com.ubb.fanfiction.dao.PuntuacionDao;
import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.form.BusquedaForm;
import com.ubb.fanfiction.form.OrdenarFanfictionForm;
import com.ubb.fanfiction.model.Publicacion;
import com.ubb.fanfiction.service.impl.FanfictionServicioImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Collections.class)
public class FanfictionServicioTests {
	private Fanfiction fanfiction;
	private Usuario usuario;
	private Integer idFanfiction;
	private Integer idUsuario;
	private List<Fanfiction> listaFanfiction;
	private Set<Fanfiction> fanfictions;
	/*Atributos busqueda*/
	private String titulo = "Una Historia Infinita";
	private String autor = "mmunocan";
	private String inicioCreacion = "2017-10-28";
	private String finCreacion = "2017-10-28";
	private String inicioPublicacion = "2017-10-28";
	private String finPublicacion = "2017-10-28";
	private Integer comentarioMinimo = 1;
	private Integer comentarioMaximo = 3;
	private Integer capituloMinimo = 1;
	private Integer capituloMaximo = 3;
	private Integer puntuacionMinima = 1;
	private Integer puntuacionMaxima = 3;
	private Integer genero = null;
	private Integer categoria = null;
	private Integer historiaOriginal = null;
	private Integer clasificacionParental = null;
	private String estado = "Terminado";
	private String tipo = "Normal";
	private Double puntuacion = 2.5;
	private Long comentarios = (long) 13;
	
	@Mock
	private PuntuacionDao puntuacionDao;
	
	@Mock
	private ComentarioDao comentarioDao;
	
	@Mock
	private FanfictionDao fanfictionDao;
	
	@InjectMocks
	private FanfictionServicio fanfictionServicio = new FanfictionServicioImpl();
	
	@Before
	public void setUp(){
		idUsuario = 2;
		String nombre = "mmunocan";
		String correo = "mmunocan@gmail.com";
		idFanfiction = 2;
		
		usuario = new Usuario();
		usuario.setIdUsuario(idUsuario);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave("c14v3");
		usuario.setRol("USER");
		usuario.setEstado(true);
		
		ClasificacionParental clasificacion = new ClasificacionParental("K (5+)");
		
		HashSet<Genero> listaGeneros = new HashSet<Genero>();
		Genero genero1 = new Genero();
		genero1.setIdGenero(1);
		genero1.setNombre("Drama");
		Genero genero2 = new Genero();
		genero2.setIdGenero(2);
		genero2.setNombre("Comedia");
		Genero genero3 = new Genero();
		genero3.setIdGenero(3);
		genero3.setNombre("Romance");
		listaGeneros.add(genero1);
		listaGeneros.add(genero2);
		listaGeneros.add(genero3);
		
		Categoria categoria = new Categoria();
		categoria.setIdCategoria(1);
		categoria.setNombre("Libro");
		
		HashSet<HistoriaOriginal> listaHistoriaOriginal = new HashSet<HistoriaOriginal>();
		HistoriaOriginal historiaOriginal = new HistoriaOriginal();
		historiaOriginal.setCategoria(categoria);
		historiaOriginal.setNombre("Harry Potter");
		listaHistoriaOriginal.add(historiaOriginal);
		
		Date fechaCreacion = Calendar.getInstance().getTime();
		
		Date fechaActualizacion = Calendar.getInstance().getTime();
		
		Capitulo unico = new Capitulo();
		unico.setIdCapitulo(1);
		unico.setNumero(1);
		unico.setTitulo("Una carta amenazante");
		unico.setFechaPublicacion(fechaCreacion);
		unico.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		HashSet<Capitulo> listaCapitulos = new HashSet<Capitulo>();
		listaCapitulos.add(unico);
		
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(fechaCreacion);
		fanfiction.setFechaActualizacion(fechaActualizacion);
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(usuario);
		fanfiction.setClasificacionParental(clasificacion);
		fanfiction.setGeneros(new HashSet<Genero>());
		fanfiction.setGeneros(listaGeneros);
		fanfiction.setHistoriaOriginals(listaHistoriaOriginal);
		unico.setFanfiction(fanfiction);
		fanfiction.setCapitulos(listaCapitulos);
		
		listaFanfiction = new ArrayList<Fanfiction>();
		listaFanfiction.add(fanfiction);
		
		fanfictions = new HashSet<Fanfiction>();
		fanfictions.addAll(listaFanfiction);
		
		
	}
	
	@Test
	public void guardarFanfictionDeFormaExitosa() throws DaoException{
		Fanfiction fic = fanfiction;
		fic.setIdFanfiction(null);
		when(fanfictionDao.save(fic)).thenReturn(fanfiction);
		
		Fanfiction resultado = fanfictionServicio.guardarFanfiction(fic);
		
		verify(fanfictionDao).save(fic);
		assertNotNull(resultado);
		assertNotEquals((Integer) 0, resultado.getIdFanfiction());
		assertEquals(fanfiction.getTitulo(),resultado.getTitulo());
		assertEquals(fanfiction.getDescripcion(),resultado.getDescripcion());
		assertEquals(fanfiction.getFechaCreacion(), resultado.getFechaCreacion());
		assertEquals(fanfiction.getFechaActualizacion(), resultado.getFechaActualizacion());
		assertEquals(fanfiction.getEstado(), resultado.getEstado());
		assertEquals(fanfiction.getUsuario(), resultado.getUsuario());
		assertEquals(fanfiction.getClasificacionParental(), resultado.getClasificacionParental());
		assertEquals(fanfiction.getGeneros(), resultado.getGeneros());
		assertEquals(fanfiction.getHistoriaOriginals(), resultado.getHistoriaOriginals());
		
		
	}
	
	@Test(expected = DaoException.class)
	public void guardarFanfictionDeFormaErroneaPorErrorEnCapaDao() throws DaoException{
		fanfiction.setIdFanfiction(null);
		doThrow(Exception.class).when(fanfictionDao).save(fanfiction);
		
		fanfictionServicio.guardarFanfiction(fanfiction);
	}
	
	@Test
	public void buscarFanfictionPorIdDeFormaExitosa() throws ObjectNotFoundException, DaoException{
		when(fanfictionDao.findOne(idFanfiction)).thenReturn(fanfiction);
		
		Fanfiction resultado = fanfictionServicio.buscarFanfictionPorId(idFanfiction);
		
		assertEquals(fanfiction, resultado);
		
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void buscarFanfictionPorIdSinResultados() throws ObjectNotFoundException, DaoException{
		fanfiction = null;
		when(fanfictionDao.findOne(idFanfiction)).thenReturn(fanfiction);
		
		fanfictionServicio.buscarFanfictionPorId(idFanfiction);
		
	}
	
	@Test(expected = DaoException.class)
	public void buscarFanfictionPorIdDeFormaFallidaPorErrorCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(fanfictionDao).findOne(idFanfiction);
		
		fanfictionServicio.buscarFanfictionPorId(idFanfiction);
	}
	
	@Test
	public void obtenerListaDeFanfictionsDeUnUsuarioOrdenadosFechaActualizacionDesc() throws Exception{
		when(fanfictionDao.findByUsuarioOrderByFechaActualizacionDesc(usuario.getIdUsuario())).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(anyDouble());
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(anyLong());
		
		List<Publicacion> resultado = fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_ACTUALIZACION, OrdenarFanfictionForm.DESC);
		
		assertNotNull(resultado);
		verify(fanfictionDao).findByUsuarioOrderByFechaActualizacionDesc(usuario.getIdUsuario());
	}
	
	@Test
	public void obtenerListaDeFanfictionsDeUnUsuarioOrdenadosFechaActualizacionAsc() throws Exception{
		when(fanfictionDao.findByUsuarioOrderByFechaActualizacionAsc(usuario.getIdUsuario())).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(anyDouble());
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(anyLong());
		
		List<Publicacion> resultado = fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_ACTUALIZACION, OrdenarFanfictionForm.ASC);
		
		assertNotNull(resultado);
		verify(fanfictionDao).findByUsuarioOrderByFechaActualizacionAsc(usuario.getIdUsuario());
	}
	
	@Test
	public void obtenerListaDeFanfictionsDeUnUsuarioOrdenadosCreacionDesc() throws Exception{
		when(fanfictionDao.findByUsuarioOrderByFechaCreacionDesc(usuario.getIdUsuario())).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(anyDouble());
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(anyLong());
		
		List<Publicacion> resultado = fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.DESC);
		
		assertNotNull(resultado);
		verify(fanfictionDao).findByUsuarioOrderByFechaCreacionDesc(usuario.getIdUsuario());
	}
	
	@Test
	public void obtenerListaDeFanfictionsDeUnUsuarioOrdenadosCreacionAsc() throws Exception{
		when(fanfictionDao.findByUsuarioOrderByFechaCreacionAsc(usuario.getIdUsuario())).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(anyDouble());
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(anyLong());
		
		List<Publicacion> resultado = fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.FECHA_CREACION, OrdenarFanfictionForm.ASC);
		
		assertNotNull(resultado);
		verify(fanfictionDao).findByUsuarioOrderByFechaCreacionAsc(usuario.getIdUsuario());
	}
	
	@Test
	public void obtenerListaDeFanfictionsDeUnUsuarioOrdenadosPorComparator() throws Exception{
		PowerMockito.mockStatic(Collections.class);
		when(fanfictionDao.findByIdUsuario(usuario.getIdUsuario())).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(anyDouble());
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(anyLong());
		
		List<Publicacion> resultado = fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.CAPITULOS, OrdenarFanfictionForm.ASC);
		
		Collections.sort(anyObject(),anyObject());
		PowerMockito.verifyStatic();
		Collections.sort(anyObject(),anyObject());
		assertNotNull(resultado);
		verify(fanfictionDao).findByIdUsuario(usuario.getIdUsuario());
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListaDeFanfictionsDeUnUsuarioFallidoPorErrorCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(fanfictionDao).findByIdUsuario(usuario.getIdUsuario());
		
		fanfictionServicio.obtenerListaFanfictionsUsuario(usuario.getIdUsuario(), OrdenarFanfictionForm.CAPITULOS, OrdenarFanfictionForm.ASC);
	}
	
	@Test
	public void obtenerResultadoBusquedaFanfictionDeFormaExitosa() throws Exception{
		int atributoOrden = BusquedaForm.FECHA_ACTUALIZACION;
		int ordenBusqueda = BusquedaForm.DESC;
		when(fanfictionDao.buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, "fechaActualizacion", "desc")).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(puntuacion);
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(comentarios);
		
		List<Publicacion> resultado = fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda);
		
		assertNotNull(resultado);
		verify(fanfictionDao).buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, "fechaActualizacion", "desc");
	}
	
	@Test(expected = DaoException.class)
	public void obtenerResultadoBusquedaFanfictionDeFormaFallidaPorErrorCapaDao() throws ObjectNotFoundException, DaoException, ParseException{
		int atributoOrden = BusquedaForm.FECHA_ACTUALIZACION;
		int ordenBusqueda = BusquedaForm.DESC;
		doThrow(Exception.class).when(fanfictionDao).buscar(eq(titulo), eq(autor), eq(inicioCreacion), eq(finCreacion), 
				eq(inicioPublicacion), eq(finPublicacion), eq(comentarioMinimo), eq(comentarioMaximo), eq(capituloMinimo), eq(capituloMaximo), 
				eq(puntuacionMinima), eq(puntuacionMaxima), eq(genero), eq(categoria), eq(historiaOriginal), eq(clasificacionParental), 
				eq(estado), eq(tipo), anyString(), anyString());
		
		fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda);
	}
	
	@Test
	public void ordenarResultadoBusquedaPorFechaActualizacionAscendente() throws DaoException, ParseException{
		int atributoOrden = BusquedaForm.FECHA_ACTUALIZACION;
		int ordenBusqueda = BusquedaForm.ASC;
		when(fanfictionDao.buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, "fechaActualizacion", "asc")).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(puntuacion);
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(comentarios);
		
		List<Publicacion> resultado = fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda);
		
		assertNotNull(resultado);
		verify(fanfictionDao).buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, "fechaActualizacion", "asc");
	}
	
	@Test
	public void ordenarResultadoBusquedaPorFechaCreacionDescendente() throws DaoException, ParseException{
		int atributoOrden = BusquedaForm.FECHA_CREACION;
		int ordenBusqueda = BusquedaForm.DESC;
		when(fanfictionDao.buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, "fechaCreacion", "desc")).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(puntuacion);
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(comentarios);
		
		List<Publicacion> resultado = fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda);
		
		assertNotNull(resultado);
		verify(fanfictionDao).buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, "fechaCreacion", "desc");
	}
	
	@Test
	public void ordenarResultadoBusquedaPorFechaCreacionAscendente() throws DaoException, ParseException{
		int atributoOrden = BusquedaForm.FECHA_CREACION;
		int ordenBusqueda = BusquedaForm.ASC;
		when(fanfictionDao.buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, "fechaCreacion", "asc")).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(puntuacion);
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(comentarios);
		
		List<Publicacion> resultado = fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda);
		
		assertNotNull(resultado);
		verify(fanfictionDao).buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, "fechaCreacion", "asc");
	}
	
	//Problems with Collections Mock
	@Test
	public void ordenarResultadoBusquedaUsandoComparador() throws DaoException, ParseException{
		int atributoOrden = BusquedaForm.CAPITULOS;
		int ordenBusqueda = BusquedaForm.ASC;
		PowerMockito.mockStatic(Collections.class);
		when(fanfictionDao.buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, null, null)).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(puntuacion);
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(comentarios);
				
		List<Publicacion> resultado = fanfictionServicio.buscarFanfiction(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, atributoOrden, ordenBusqueda);
		
		Collections.sort(anyObject(),anyObject());
		PowerMockito.verifyStatic();
		Collections.sort(anyObject(),anyObject());
		assertNotNull(resultado);
		verify(fanfictionDao).buscar(titulo, autor, inicioCreacion, finCreacion, 
				inicioPublicacion, finPublicacion, comentarioMinimo, comentarioMaximo, capituloMinimo, capituloMaximo, puntuacionMinima, 
				puntuacionMaxima, genero, categoria, historiaOriginal, clasificacionParental, estado, tipo, null, null);
	}
	
	@Test
	public void consultarSiUsuarioEscribioFanfictionConResultadoVerdadero() throws DaoException{
		when(fanfictionDao.findByIdAndUsuario(idFanfiction,idUsuario)).thenReturn(fanfiction);
		
		boolean resultado = fanfictionServicio.fanfictionEsDeUsuario(idFanfiction,idUsuario);
		
		assertTrue(resultado);
	}
	
	@Test
	public void consultarSiUsuarioEscribioFanfictionConResultadoFalso() throws DaoException{
		when(fanfictionDao.findByIdAndUsuario(idFanfiction,idUsuario)).thenReturn(null);
		
		boolean resultado = fanfictionServicio.fanfictionEsDeUsuario(idFanfiction,idUsuario);
		
		assertFalse(resultado);
	}
	
	@Test(expected = DaoException.class)
	public void consultarSiUsuarioEscribioFanfictionDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(fanfictionDao).findByIdAndUsuario(idFanfiction,idUsuario);
		
		fanfictionServicio.fanfictionEsDeUsuario(idFanfiction,idUsuario);
	}
	
	@Test
	public void actualizarFanfictionDeFormaExitosa() throws DaoException{
		
		fanfictionServicio.actualizarFanfiction(fanfiction);
		
		verify(fanfictionDao).save(fanfiction);
		
	}
	
	@Test(expected = DaoException.class)
	public void actualizarFanfictionDeFormaErroneaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(fanfictionDao).save(fanfiction);
		
		fanfictionServicio.actualizarFanfiction(fanfiction);
	}
	
	@Test
	public void eliminarFanfictionDeFormaExitosa() throws DaoException{
		
		fanfictionServicio.eliminarFanfiction(fanfiction);
		
		verify(fanfictionDao).delete(fanfiction);
		
	}
	
	@Test(expected = DaoException.class)
	public void eliminarFanfictionDeFormaErroneaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(fanfictionDao).delete(fanfiction);
		
		fanfictionServicio.eliminarFanfiction(fanfiction);
	}
	
	@Test
	public void eliminarListaFanfictionsDeFormaExitosa() throws DaoException{
		
		fanfictionServicio.eliminarListaFanfictions(fanfictions);
		
		verify(fanfictionDao).delete(fanfictions);
		
	}
	
	@Test(expected = DaoException.class)
	public void eliminarListaFanfictionsDeFormFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(fanfictionDao).delete(fanfictions);
		
		fanfictionServicio.eliminarListaFanfictions(fanfictions);
	}
	
	@Test
	public void ordenarListaFanfictionUsuarioFechaCreacionAsc(){
		OrdenarFanfictionForm orden = new OrdenarFanfictionForm();
		orden.setAtributoOrden(OrdenarFanfictionForm.FECHA_CREACION);
		orden.setOrdenBusqueda(OrdenarFanfictionForm.ASC);
		when(fanfictionDao.findByUsuarioOrderByFechaCreacionAsc(usuario.getIdUsuario())).thenReturn(listaFanfiction);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(anyDouble());
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(anyLong());
		
		//List<Publicacion> resultado = fanfictionServicio.ordenarFanfictionDeAcuerdoAParamentros(idUsuario, orden);
		
		//assertNotNull(resultado);
	}
	
	public void ordenarListaFanfictionUsuarioFechaCreacionDesc(){
		
	}
	
	public void ordenarListaFanfictionUsuarioFechaActualizacionAsc(){
		
	}
	
	public void ordenarListaFanfictionUsuarioFechaActualizacionDesc(){
		
	}
	
	public void ordenarListaFanfictionUsuarioUsandoComparator(){
		
	}
	
	public void ordenarListaFanfictionUsuarioFallidoPorErrorEnCapaDao(){
		
	}
}
