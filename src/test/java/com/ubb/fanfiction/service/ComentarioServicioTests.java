package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.dao.ComentarioDao;
import com.ubb.fanfiction.dao.FanfictionDao;
import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Comentario;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.impl.ComentarioServicioImpl;

@RunWith(MockitoJUnitRunner.class)
public class ComentarioServicioTests {
	private Fanfiction fanfiction;
	private Capitulo capitulo;
	private Integer idCapitulo;
	private Integer idFanfiction;
	private Integer idUsuario;
	private List<Comentario> listaComentarios;
	private Comentario comentario;
	
	@Mock
	private ComentarioDao comentarioDao;
	
	@Mock
	private FanfictionDao fanfictionDao;
	
	@InjectMocks
	private ComentarioServicio comentarioServicio = new ComentarioServicioImpl();
	
	@Before
	public void setUp(){
		Date fechaCreacion = Calendar.getInstance().getTime();
		ClasificacionParental clasificacion = new ClasificacionParental("K (5+)");
		Date fechaActualizacion = Calendar.getInstance().getTime();
		idFanfiction = 1;
		idCapitulo = 1;
		idUsuario = 2;
		
		Usuario comentador = new Usuario("pedro","pedro@gmail.com","pedropedro","USER",true,fechaCreacion, true);
		comentador.setIdUsuario(3);
		
		Usuario autor = new Usuario("mmunoca","mmunocan@hotmail.com","c14v3","USER",true,fechaCreacion, true);
		autor.setIdUsuario(idUsuario);
		
		capitulo = new Capitulo();
		capitulo.setIdCapitulo(idCapitulo);
		capitulo.setNumero(1);
		capitulo.setTitulo("Una carta amenazante");
		capitulo.setFechaPublicacion(fechaCreacion);
		capitulo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(fechaCreacion);
		fanfiction.setFechaActualizacion(fechaActualizacion);
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(autor);
		fanfiction.setClasificacionParental(clasificacion);
		capitulo.setFanfiction(fanfiction);
		
		Set<Comentario> comentariosCapitulo = new HashSet<Comentario>();
		comentariosCapitulo.add(new Comentario(capitulo, comentador, fechaCreacion, "Es un gran capitulo"));
		comentariosCapitulo.add(new Comentario(capitulo, comentador, fechaCreacion, "Conti plz"));
		comentariosCapitulo.add(new Comentario(capitulo, comentador, fechaCreacion, "Me encantó bastante"));
		capitulo.setComentarios(comentariosCapitulo);
		
		this.listaComentarios = new ArrayList<Comentario>();
		this.listaComentarios.addAll(comentariosCapitulo);

		HashSet<Capitulo> listaCapitulos = new HashSet<Capitulo>();
		listaCapitulos.add(capitulo);
		fanfiction.setCapitulos(listaCapitulos);
		
		comentario = new Comentario();
		comentario.setIdComentario(1);
		comentario.setCapitulo(capitulo);
		comentario.setUsuario(comentador);
		comentario.setFecha(fechaCreacion);
		comentario.setContenido("Es una maravillosa historia. Me gustó mucho.");
	}
	
	@Test
	public void obtenerNumeroDeComentariosDeUnFanfictionDeFormaExitosa() throws DaoException{
		Long esperado = (long) listaComentarios.size();
		when(comentarioDao.countByFanfiction(idFanfiction)).thenReturn(esperado);
		
		Long resultado = comentarioServicio.contarComentariosPorFanfiction(idFanfiction);
		
		assertEquals(esperado, resultado);
		
	}
	
	@Test(expected = DaoException.class)
	public void obtenerNumeroDeComentariosDeUnFanfictionDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(comentarioDao).countByFanfiction(idFanfiction);
		
		comentarioServicio.contarComentariosPorFanfiction(idFanfiction);
	}
	
	@Test
	public void obtenerNumeroDeComentariosDeUnCapituloDeFormaExitosa() throws Exception{
		Long esperado = (long) listaComentarios.size();
		when(comentarioDao.countByCapitulo(idCapitulo)).thenReturn(esperado);
		
		Long resultado = comentarioServicio.contarComentariosPorCapitulo(idCapitulo);
		
		assertEquals(esperado, resultado);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerNumeroDeComentariosDeUnCapituloDeFormaFallidaPorErrorEnCapaDao() throws Exception{
		Integer idCapitulo = capitulo.getIdCapitulo();
		doThrow(Exception.class).when(comentarioDao).countByCapitulo(idCapitulo);
		
		comentarioServicio.contarComentariosPorCapitulo(idCapitulo);
	}
	
	
	@Test
	public void obtenerListaComentariosDeUnCapituloOrdenadosPorFechaDeFormaExitosa() throws DaoException{
		when(comentarioDao.findByCapituloOrderByFechaDesc(idCapitulo)).thenReturn(listaComentarios);
		
		List<Comentario> comentariosRecibido = comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(capitulo.getIdCapitulo());
		
		assertEquals(comentariosRecibido,listaComentarios);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListaComentariosDeUnCapituloOrdenadosPorFechaDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(comentarioDao).findByCapituloOrderByFechaDesc(idCapitulo);
		
		comentarioServicio.obtenerListaComentariosPorCapituloOrdenadoPorFecha(capitulo.getIdCapitulo());
		
	}
	
	@Test
	public void guardarComentarioDeFormaExitosa() throws DaoException{
		
		comentarioServicio.guardarComentario(comentario);
		
		verify(comentarioDao).save(comentario);

	}
	
	@Test(expected = DaoException.class)
	public void errorAlGuardarComentario() throws DaoException{
		doThrow(Exception.class).when(comentarioDao).save(comentario);
		
		comentarioServicio.guardarComentario(comentario);

	}
	
	@Test
	public void obtenerListaComentariosRecibidosDeFormaExitosa() throws DaoException{
		when(comentarioDao.findByUsuarioFanfictionOrderByFechaDesc(idUsuario)).thenReturn(listaComentarios);
		
		List<Comentario> listaRecibida = comentarioServicio.obtenerComentariosRecibidos(idUsuario);
		
		assertEquals(listaComentarios,listaRecibida);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListaComentariosRecibidosDeFormaFallidaPorEroorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(comentarioDao).findByUsuarioFanfictionOrderByFechaDesc(idUsuario);
		
		comentarioServicio.obtenerComentariosRecibidos(idUsuario);
	}
	
	@Test
	public void buscarComentarioDeFormaExitosa() throws ObjectNotFoundException, DaoException{
		when(comentarioDao.findOne(comentario.getIdComentario())).thenReturn(comentario);
		
		Comentario recibido = comentarioServicio.buscarComentarioPorId(comentario.getIdComentario());
		
		assertEquals(comentario,recibido);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void buscarComentarioDeFormaFallidaPorNoExistirComentario() throws ObjectNotFoundException, DaoException{
		when(comentarioDao.findOne(comentario.getIdComentario())).thenReturn(null);
		
		comentarioServicio.buscarComentarioPorId(comentario.getIdComentario());
	}
	
	@Test(expected = DaoException.class)
	public void buscarComentarioDeFormaFallidaPorErrorEnCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(comentarioDao).findOne(comentario.getIdComentario());
		
		comentarioServicio.buscarComentarioPorId(comentario.getIdComentario());
	}
	
	@Test
	public void eliminarComentarioDeFormaExitosa() throws DaoException{
		
		comentarioServicio.eliminarComentario(comentario);
		
		verify(comentarioDao).delete(comentario);
	}
	
	@Test(expected = DaoException.class)
	public void eliminarComentarioDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(comentarioDao).delete(comentario);
		
		comentarioServicio.eliminarComentario(comentario);
	}
	
	@Test
	public void obtenerListaComentariosRealizadoseFormaExitosa() throws DaoException{
		when(comentarioDao.findByUsuarioOrderByFechaDesc(idUsuario)).thenReturn(listaComentarios);
		
		List<Comentario> listaRecibida = comentarioServicio.obtenerComentariosRealizados(idUsuario);
		
		assertEquals(listaComentarios,listaRecibida);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListaComentariosRealizadosDeFormaFallidaPorEroorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(comentarioDao).findByUsuarioOrderByFechaDesc(idUsuario);
		
		comentarioServicio.obtenerComentariosRealizados(idUsuario);
	}
}
