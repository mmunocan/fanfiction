package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ubb.fanfiction.dao.UsuarioDao;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.impl.UsuarioServicioImpl;

@RunWith(MockitoJUnitRunner.class)
public class UsuarioServicioTests {
	private Usuario usuario;
	private String nombre;
	private String correo;
	private Integer id;
	private String token;
	
	@Mock
	private Usuario usuarioMock;
	
	@Mock
	private BCryptPasswordEncoder codificador;
	
	@Mock
	private UsuarioDao usuarioDao;
	@Mock
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@InjectMocks
	private UsuarioServicio usuarioServicio = new UsuarioServicioImpl();
	
	@Before
	public void setUp(){
		id = 2;
		String rol = "USER";
		correo = "mmunocan@gmail.com";
		nombre = "mmunocan";
		token = "24d53599-9f99-49d0-a704-244924b5d18b";
		String clave = "c14v3";
		usuario = new Usuario();
		usuario.setIdUsuario(id);
		usuario.setNombre(nombre);
		usuario.setCorreo(correo);
		usuario.setClave(clave);
		usuario.setRol(rol);
		usuario.setEstado(true);
		usuario.setFechaCreacion(Calendar.getInstance().getTime());
		usuario.setToken(token);
		
		when(usuarioMock.getIdUsuario()).thenReturn(usuario.getIdUsuario());
		when(usuarioMock.getNombre()).thenReturn(usuario.getNombre());
		when(usuarioMock.getCorreo()).thenReturn(usuario.getCorreo());
		when(usuarioMock.getClave()).thenReturn(usuario.getClave());
		when(usuarioMock.getRol()).thenReturn(usuario.getRol());
		when(usuarioMock.isEstado()).thenReturn(usuario.isEstado());
		when(usuarioMock.getDescripcion()).thenReturn(usuario.getDescripcion());
		when(usuarioMock.isCorreoActivo()).thenReturn(true);
		
	}
	
	@Test
	public void busquedaUsuarioPorCorreoConResultado() throws  DaoException{
		when(usuarioDao.findByCorreo(correo)).thenReturn(usuario);
		
		Usuario usuarioRecibido = usuarioServicio.buscarUsuarioPorCorreo(correo);
		
		assertEquals(usuario,usuarioRecibido);
	}
	
	@Test(expected = DaoException.class)
	public void busquedaUsuarioPorCorreoFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).findByCorreo(correo);
		
		usuarioServicio.buscarUsuarioPorCorreo(correo);
	}
	
	@Test
	public void busquedaUsuarioPorNombreConResultado() throws DaoException{
		when(usuarioDao.findByNombre(nombre)).thenReturn(usuario);
		
		Usuario usuarioRecibido = usuarioServicio.buscarUsuarioPorNombre(nombre);
		
		assertEquals(usuario,usuarioRecibido);
	}
	
	@Test(expected = DaoException.class)
	public void busquedaUsuarioPorNombreFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).findByNombre(nombre);
		
		usuarioServicio.buscarUsuarioPorNombre(nombre);
	}
	
	@Test
	public void guardarUsuarioDeFormaExitosa() throws DaoException{
		when(usuarioDao.save(usuarioMock)).thenReturn(usuarioMock);
		
		Integer id = usuarioServicio.guardarUsuario(usuarioMock);
		
		verify(usuarioDao).save(usuarioMock);
		verify(bCryptPasswordEncoder).encode(anyString());
		verify(usuarioMock).setRol("USER");
		verify(usuarioMock).setEstado(true);
		verify(usuarioMock).setFechaCreacion((Date)anyObject());
		verify(usuarioMock).setCorreoActivo(false);
		verify(usuarioMock).setDescripcion(("<p>" + usuario.getDescripcion() + "</p>").replaceAll("\n", "</p><p>"));
		assertEquals(this.id,id);
	}
	
	@Test(expected = DaoException.class)
	public void errorAlGuardarUsuario() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).save(usuario);
		
		usuarioServicio.guardarUsuario(usuario);
		
	}
	
	@Test
	public void busquedaUsuarioPorIdConResultado() throws DaoException, ObjectNotFoundException{
		when(usuarioDao.findOne(id)).thenReturn(usuario);
		
		Usuario usuarioRecibido = usuarioServicio.buscarUsuarioPorId(id);
		
		assertEquals(usuario,usuarioRecibido);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void busquedaUsuarioPorIdSinResultado() throws DaoException, ObjectNotFoundException {
		usuario = null;
		when(usuarioDao.findOne(id)).thenReturn(usuario);
		
		Usuario usuarioRecibido = usuarioServicio.buscarUsuarioPorId(id);
		
		assertEquals(usuario,usuarioRecibido);
	}
	
	@Test(expected = DaoException.class)
	public void busquedaUsuarioPorIdFallidaPorErrorEnCapaDao() throws DaoException, ObjectNotFoundException{
		doThrow(Exception.class).when(usuarioDao).findOne(id);
		
		usuarioServicio.buscarUsuarioPorId(id);
	}
	
	@Test
	public void eliminarUsuarioDeFormaExitosa() throws DaoException{
		
		usuarioServicio.eliminarUsuario(usuario);
		
		verify(usuarioDao).delete(usuario);
	}
	
	@Test(expected = DaoException.class)
	public void eliminarUsuarioDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).delete(usuario);
		
		usuarioServicio.eliminarUsuario(usuario);
	}
	
	@Test
	public void desactivarUsuarioDeFormaExitosa() throws DaoException{

		usuarioServicio.desactivarUsuario(usuarioMock);
		
		verify(usuarioMock).setEstado(false);
		verify(usuarioDao).save(usuarioMock);
	}
	
	@Test(expected = DaoException.class)
	public void desactivarUsuarioDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).save(usuario);
		
		usuarioServicio.desactivarUsuario(usuario);
	}
	
	@Test
	public void actualizarUsuarioDeFormaExitosa() throws DaoException{
		String clave = usuario.getClave();
		String claveEncriptada = "sdlvjdsoivj";
		when(bCryptPasswordEncoder.encode(clave)).thenReturn(claveEncriptada);
		
		usuarioServicio.editarUsuario(usuarioMock);
		
		verify(usuarioDao).save(usuarioMock);
		verify(bCryptPasswordEncoder).encode(clave);
		verify(usuarioMock).setClave(claveEncriptada);
		verify(usuarioMock).setDescripcion(("<p>" + usuario.getDescripcion() + "</p>").replaceAll("\n", "</p><p>"));
		
	}
	
	@Test(expected = DaoException.class)
	public void actualizarUsuarioDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).save(usuario);
		
		usuarioServicio.editarUsuario(usuario);
	}
	
	@Test
	public void busquedaOtroUsuarioPorCorreoConResultado() throws  DaoException{
		when(usuarioDao.findByNotIdUsuarioAndCorreo(id,correo)).thenReturn(usuario);
		
		Usuario usuarioRecibido = usuarioServicio.buscarOtroUsuarioPorCorreo(id, correo);
		
		assertEquals(usuario,usuarioRecibido);
	}
	
	
	@Test(expected = DaoException.class)
	public void busquedaOtroUsuarioPorCorreoFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).findByNotIdUsuarioAndCorreo(id,correo);
		
		usuarioServicio.buscarOtroUsuarioPorCorreo(id,correo);
	}
	
	@Test
	public void busquedaOtroUsuarioPorNombreConResultado() throws  DaoException{
		when(usuarioDao.findByNotIdUsuarioAndNombre(id,nombre)).thenReturn(usuario);
		
		Usuario usuarioRecibido = usuarioServicio.buscarOtroUsuarioPorNombre(id, nombre);
		
		assertEquals(usuario,usuarioRecibido);
	}
	
	
	@Test(expected = DaoException.class)
	public void busquedaOtroUsuarioPorNombreFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).findByNotIdUsuarioAndNombre(id,nombre);
		
		usuarioServicio.buscarOtroUsuarioPorNombre(id,nombre);
	}
	
	@Test
	public void validarTokenTrue() throws DaoException{
		when(usuarioDao.findByIdUsuarioAndToken(id,token)).thenReturn(usuario);
		
		boolean resultado = usuarioServicio.validarToken(token, id);
		
		assertTrue(resultado);
	}
	
	@Test
	public void validarTokenFalse() throws DaoException{
		when(usuarioDao.findByIdUsuarioAndToken(id,token)).thenReturn(null);
		
		boolean resultado = usuarioServicio.validarToken(token, id);
		
		assertFalse(resultado);
	}
	
	@Test(expected = DaoException.class)
	public void validarTokenFallidoPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).findByIdUsuarioAndToken(id,token);
		
		usuarioServicio.validarToken(token, id);
	}
	
	@Test
	public void validarCuentaNuevaExitoso() throws DaoException{
		when(usuarioMock.getNuevoCorreo()).thenReturn(null);
		when(usuarioMock.isCorreoActivo()).thenReturn(false);
		
		usuarioServicio.validarCuenta(usuarioMock);
		
		verify(usuarioMock).setCorreoActivo(true);
		verify(usuarioMock).setToken(null);
		verify(usuarioDao).save(usuarioMock);
	}
	
	@Test
	public void validarCuentaEditadaExitoso() throws DaoException{
		String correo = "mmunocan@gmail.com";
		when(usuarioMock.getNuevoCorreo()).thenReturn(correo);
		when(usuarioMock.isCorreoActivo()).thenReturn(true);
		
		usuarioServicio.validarCuenta(usuarioMock);
		
		verify(usuarioMock).setCorreo(correo);
		verify(usuarioMock).setToken(null);
		verify(usuarioMock).setNuevoCorreo(null);
		verify(usuarioDao).save(usuarioMock);
	}
	
	@Test(expected = DaoException.class)
	public void validarCuentaFallidoPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).save(usuario);
		
		usuarioServicio.validarCuenta(usuario);
	}
	
	@Test
	public void generarTokenExitoso(){
		
		String token = usuarioServicio.generarToken();
		
		assertNotNull(token);
	}
	
	@Test
	public void guardarUsuarioSinTocarContraseñaExitoso() throws DaoException{

		usuarioServicio.guardarUsuarioDirecto(usuario);
		
		verify(usuarioDao).save(usuario);
	}
	
	@Test(expected = DaoException.class)
	public void errorAlGuardarUsuarioSinContraseña() throws DaoException{
		doThrow(Exception.class).when(usuarioDao).save(usuario);
		
		usuarioServicio.guardarUsuarioDirecto(usuario);
		
	}
	
	@Test
	public void restablecerContraseñaExitoso() throws DaoException{
		String clave = "nuevaContraseña";
		String claveEncriptada = "dlkvnefdpoavbmfdlkvneoiagnemvfoaernivgo";
		when(bCryptPasswordEncoder.encode(clave)).thenReturn(claveEncriptada);
		
		usuarioServicio.restablecerContraseña(usuarioMock, clave);
		
		verify(bCryptPasswordEncoder).encode(clave);
		verify(usuarioMock).setToken(null);
		verify(usuarioMock).setClave(claveEncriptada);
	}
	
	@Test(expected = DaoException.class)
	public void restrablecerContraseñaFallidoPorErrorEnCapaDao() throws DaoException{
		String clave = "nuevaContraseña";
		doThrow(Exception.class).when(usuarioDao).save(usuario);
		
		usuarioServicio.restablecerContraseña(usuario, clave);
	}
}
