package com.ubb.fanfiction.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.ubb.fanfiction.exception.EmailException;
import com.ubb.fanfiction.service.impl.MensajeCorreoServicioImpl;

@RunWith(MockitoJUnitRunner.class)
public class MensajeCorreoServicioTests {
	private String correo;
	private String contextPath;
	private String token;
	private Integer idUsuario;
	
	@Mock
	private JavaMailSender emailSender;
	
	@InjectMocks
	private MensajeCorreoServicio mensajeCorreoServicio = new MensajeCorreoServicioImpl();
	
	@Before
	public void setUp(){
		correo = "mmunocan@gmail.com";
		contextPath = "www.zonadefics.cl";
		token = "24d53599-9f99-49d0-a704-244924b5d18b";
		idUsuario = 2;
	}
	
	@Test
	public void enviarMensajeActivacionCuentaExitoso() throws EmailException{
		
		mensajeCorreoServicio.enviarCorreoActivacionCuenta(contextPath, idUsuario, token, correo);
		
		verify(emailSender).send(any(SimpleMailMessage.class));
		
	}
	
	@Test
	public void enviarMensajeActivacionCuentaRepetidoPorErrorEnEnvio() throws EmailException{
		doThrow(Exception.class).doThrow(Exception.class).doNothing().when(emailSender).send(any(SimpleMailMessage.class));
		
		mensajeCorreoServicio.enviarCorreoActivacionCuenta(contextPath, idUsuario, token, correo);
		
		verify(emailSender,times(3)).send(any(SimpleMailMessage.class));

	}
	
	@Test(expected = EmailException.class)
	public void enviarMensajeActivaconCuentaFallidoPorDemasiadosIntentos() throws EmailException{
		doThrow(Exception.class).doThrow(Exception.class).doThrow(Exception.class).when(emailSender).send(any(SimpleMailMessage.class));
		
		mensajeCorreoServicio.enviarCorreoActivacionCuenta(contextPath, idUsuario, token, correo);
		
		verify(emailSender,times(4)).send(any(SimpleMailMessage.class));
	}
	
	@Test
	public void enviarMensajeCambioCorreoExitoso() throws EmailException{
		
		mensajeCorreoServicio.enviarCorreoCambioCorreoCuenta(contextPath, idUsuario, token, correo);
		
		verify(emailSender).send(any(SimpleMailMessage.class));
		
	}
	
	@Test
	public void enviarMensajeCambioCorreoRepetidoPorErrorEnEnvio() throws EmailException{
		doThrow(Exception.class).doThrow(Exception.class).doNothing().when(emailSender).send(any(SimpleMailMessage.class));
		
		mensajeCorreoServicio.enviarCorreoCambioCorreoCuenta(contextPath, idUsuario, token, correo);
		
		verify(emailSender,times(3)).send(any(SimpleMailMessage.class));

	}
	
	@Test(expected = EmailException.class)
	public void enviarMensajeCambioCorreoFallidoPorDemasiadosIntentos() throws EmailException{
		doThrow(Exception.class).doThrow(Exception.class).doThrow(Exception.class).when(emailSender).send(any(SimpleMailMessage.class));
		
		mensajeCorreoServicio.enviarCorreoCambioCorreoCuenta(contextPath, idUsuario, token, correo);
		
		verify(emailSender,times(4)).send(any(SimpleMailMessage.class));
	}
	
	@Test
	public void enviarMensajeRestablecerContraseñaExitoso() throws EmailException{

		mensajeCorreoServicio.enviarCorreoRecuperacionContraseña(contextPath, idUsuario, token, correo);
		
		verify(emailSender).send(any(SimpleMailMessage.class));
	}
	
	@Test
	public void enviarMensajeRestablecerContraseñaRepetidoPorErrorEnEnvio() throws EmailException{
		doThrow(Exception.class).doThrow(Exception.class).doNothing().when(emailSender).send(any(SimpleMailMessage.class));
		
		mensajeCorreoServicio.enviarCorreoRecuperacionContraseña(contextPath, idUsuario, token, correo);
		
		verify(emailSender,times(3)).send(any(SimpleMailMessage.class));

	}
	
	@Test(expected = EmailException.class)
	public void enviarMensajeRestablecerContraseñaFallidoPorDemasiadosIntentos() throws EmailException{
		doThrow(Exception.class).doThrow(Exception.class).doThrow(Exception.class).when(emailSender).send(any(SimpleMailMessage.class));
		
		mensajeCorreoServicio.enviarCorreoRecuperacionContraseña(contextPath, idUsuario, token, correo);
		
		verify(emailSender,times(4)).send(any(SimpleMailMessage.class));
	}
}
