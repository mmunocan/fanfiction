package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.dao.HistoriaOriginalDao;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.domain.HistoriaOriginal;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.impl.HistoriaOriginalServicioImpl;

@RunWith(MockitoJUnitRunner.class)
public class HistoriaOriginalServicioTests {
	private Integer id;
	private Integer idCategoria;
	private HistoriaOriginal historiaOriginal1;
	private HistoriaOriginal historiaOriginal2;
	private List<HistoriaOriginal> listaHistorias;
	
	@Mock
	private HistoriaOriginalDao historiaOriginalDao;
	
	@InjectMocks
	private HistoriaOriginalServicio historiaOriginalServicio = new HistoriaOriginalServicioImpl();
	
	@Before
	public void setUp(){
		id = 1;
		idCategoria = 4;
		Categoria categoria = new Categoria("Caricatura");
		historiaOriginal1 = new HistoriaOriginal();
		historiaOriginal1.setIdHistoriaOriginal(id);
		historiaOriginal1.setCategoria(categoria);
		historiaOriginal1.setNombre("The Loud House");
		
		historiaOriginal2 = new HistoriaOriginal();
		historiaOriginal2.setIdHistoriaOriginal(2);
		historiaOriginal2.setCategoria(categoria);
		historiaOriginal2.setNombre("My Little Pony");
		
		listaHistorias = new ArrayList<HistoriaOriginal>();
		listaHistorias.add(historiaOriginal1);
		listaHistorias.add(historiaOriginal2);
	}
	
	@Test
	public void busquedaHistoriaOriginalPorId() throws DaoException, ObjectNotFoundException{
		when(historiaOriginalDao.findOne(id)).thenReturn(historiaOriginal1);
		
		HistoriaOriginal historiaOriginalRecibida = historiaOriginalServicio.buscarHistoriaOriginalPorId(id);
		
		assertEquals(historiaOriginal1,historiaOriginalRecibida);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void busquedaHistoriaOriginalPorIdSinResultado() throws DaoException, ObjectNotFoundException {
		historiaOriginal1 = null;
		when(historiaOriginalDao.findOne(id)).thenReturn(historiaOriginal1);
		
		historiaOriginalServicio.buscarHistoriaOriginalPorId(id);
		
	}
	
	@Test(expected = DaoException.class)
	public void busquedaHistoriaOriginalPorIdFallidaPorErrorEnCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(historiaOriginalDao).findOne(id);
		
		historiaOriginalServicio.buscarHistoriaOriginalPorId(id);
	}

	@Test
	public void obtenerListaDeHistoriasOriginales() throws DaoException{
		when(historiaOriginalDao.findAllOrderByNombre()).thenReturn(listaHistorias);
		
		List<HistoriaOriginal> resultado = historiaOriginalServicio.obtenerListaHistoriasOriginales();
		
		assertNotNull(resultado);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerClasificacionesParentalesFallidoPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(historiaOriginalDao).findAllOrderByNombre();
		
		historiaOriginalServicio.obtenerListaHistoriasOriginales();
		
	}
	
	@Test
	public void obtenerHistoriasOriginalesPorCategoria() throws DaoException{
		when(historiaOriginalDao.findByCategoriaOrderByNombre(idCategoria)).thenReturn(listaHistorias);
		
		List<HistoriaOriginal> resultado = historiaOriginalServicio.buscarHistoriaPorCategoria(idCategoria);
		
		assertEquals(listaHistorias,resultado);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerHistoriasOriginalesPorCategoriaFallidoPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(historiaOriginalDao).findByCategoriaOrderByNombre(idCategoria);
		
		historiaOriginalServicio.buscarHistoriaPorCategoria(idCategoria);
		
	}
	
	@Test
	public void guardarHistoriaOriginalDeFormaExitosa() throws DaoException{
		
		historiaOriginalServicio.guardarHistoriaOriginal(historiaOriginal1);
		
		verify(historiaOriginalDao).save(historiaOriginal1);
	}
	
	@Test(expected = DaoException.class)
	public void guardarHistoriaOriginalDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(historiaOriginalDao).save(historiaOriginal1);
		
		historiaOriginalServicio.guardarHistoriaOriginal(historiaOriginal1);
	}
}
