package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.dao.GeneroDao;
import com.ubb.fanfiction.domain.Genero;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.impl.GeneroServicioImpl;


@RunWith(MockitoJUnitRunner.class)
public class GeneroServicioTests {
	private Integer id;
	private Genero genero1;
	private Genero genero2;
	private List<Genero> listaGenero;
	
	@Mock
	private GeneroDao generoDao;
	
	@InjectMocks
	private GeneroServicio generoServicio = new GeneroServicioImpl(); 
	
	@Before
	public void setUp(){
		id = 1;
		genero1 = new Genero();
		genero1.setIdGenero(id);
		genero1.setNombre("Romance");
		
		genero2 = new Genero();
		genero2.setIdGenero(2);
		genero2.setNombre("Comedia");
		
		listaGenero = new ArrayList<Genero>();
		listaGenero.add(genero1);
		listaGenero.add(genero2);
		
	}
	
	@Test
	public void busquedaGeneroPorId() throws DaoException, ObjectNotFoundException{
		when(generoDao.findOne(id)).thenReturn(genero1);
		
		Genero generoRecibido = generoServicio.buscarGeneroPorId(id);
		
		assertEquals(genero1, generoRecibido);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void busquedaGeneroPorIdSinResultados() throws DaoException, ObjectNotFoundException{
		genero1 = null;
		when(generoDao.findOne(id)).thenReturn(genero1);
		
		generoServicio.buscarGeneroPorId(id);
		
	}
	
	@Test(expected = DaoException.class)
	public void busquedaGeneroPorIdFallidaPorErrorEnCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(generoDao).findOne(id);
		
		generoServicio.buscarGeneroPorId(id);
	}
	
	@Test
	public void obtenerListaGeneros() throws DaoException{
		when(generoDao.findAllOrderByNombre()).thenReturn(listaGenero);
		
		List<Genero> resultado = generoServicio.obtenerListaGeneros();
		
		assertNotNull(resultado);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListaGenerosFallidoPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(generoDao).findAllOrderByNombre();
		
		generoServicio.obtenerListaGeneros();
		
	}
}
