package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.dao.CategoriaDao;
import com.ubb.fanfiction.domain.Categoria;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.impl.CategoriaServicioImpl;

@RunWith(MockitoJUnitRunner.class)
public class CategoriaServicioTests {
	private Integer id;
	private Categoria cat1;
	private Categoria cat2;
	private List<Categoria> listaCategoria;
	
	@Mock
	private CategoriaDao categoriaDao;
	
	@InjectMocks
	private CategoriaServicio categoriaServicio = new CategoriaServicioImpl();
	
	@Before
	public void setUp(){
		id = 1;
		cat1 = new Categoria();
		cat1.setIdCategoria(id);
		cat1.setNombre("Libro");
		
		cat2 = new Categoria();
		cat2.setIdCategoria(2);
		cat2.setNombre("Película");
		
		listaCategoria = new ArrayList<Categoria>();
		listaCategoria.add(cat1);
		listaCategoria.add(cat2);
	}
	
	@Test
	public void obtenerListaCategorias() throws DaoException{
		when(categoriaDao.findAll()).thenReturn(listaCategoria);
		
		List<Categoria> resultado = categoriaServicio.obtenerListaCategorias();
		
		assertEquals(listaCategoria,resultado);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListaCategoriasFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(categoriaDao).findAll();
		
		categoriaServicio.obtenerListaCategorias();
	}
	
	@Test
	public void obtenerCategoriaPorIdDeFormaExitosa() throws ObjectNotFoundException, DaoException{
		when(categoriaDao.findOne(cat1.getIdCategoria())).thenReturn(cat1);
		
		Categoria recibida = categoriaServicio.obtenerCategoriaPorId(cat1.getIdCategoria());
		
		assertEquals(cat1,recibida);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void obtenerCategoriaPorIdDeFormaFallidaPorCategoriaNoEncontrada() throws ObjectNotFoundException, DaoException{
		when(categoriaDao.findOne(cat1.getIdCategoria())).thenReturn(null);
		
		categoriaServicio.obtenerCategoriaPorId(cat1.getIdCategoria());
	}
	
	@Test(expected = DaoException.class)
	public void obtenerCategoriaPorIdDeFormaFallidaPorErrorEnCapaDao() throws ObjectNotFoundException, DaoException{
		doThrow(Exception.class).when(categoriaDao).findOne(cat1.getIdCategoria());
		
		categoriaServicio.obtenerCategoriaPorId(cat1.getIdCategoria());
	}
}
