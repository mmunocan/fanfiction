package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.dao.LecturaDao;
import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Lectura;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.service.impl.LecturaServicioImpl;

@RunWith(MockitoJUnitRunner.class)
public class LecturaServicioTests {
	private Usuario autor;
	private Usuario lector;
	private Integer idFanfiction;
	private Lectura lecturaCapituloUno;
	private Lectura lecturaCapituloDos;
	private Fanfiction fanfiction;
	private List<Lectura> listaLecturas;
	private Set<Lectura> lecturas;
	
	@Mock
	private LecturaDao lecturaDao;
	
	@InjectMocks
	private LecturaServicio lecturaServicio = new LecturaServicioImpl();
	
	@Before
	public void setUp(){
		idFanfiction = 1;
		Date fechaCreacion = Calendar.getInstance().getTime();
		Date fechaActualizacion = Calendar.getInstance().getTime();
		ClasificacionParental clasificacion = new ClasificacionParental("K (5+)");
		
		autor = new Usuario();
		autor.setIdUsuario(2);
		autor.setNombre("mmunocan");
		autor.setCorreo("mmunocan@gmail.com");
		autor.setClave("c14v3");
		autor.setRol("USER");
		autor.setEstado(true);
		
		lector = new Usuario();
		lector.setIdUsuario(2);
		lector.setNombre("pedro");
		lector.setCorreo("pedro@gmail.com");
		lector.setClave("pedropedro");
		lector.setRol("USER");
		lector.setEstado(true);
		
		Capitulo capituloUno = new Capitulo();
		capituloUno.setIdCapitulo(1);
		capituloUno.setNumero(1);
		capituloUno.setTitulo("Una carta amenazante");
		capituloUno.setFechaPublicacion(fechaCreacion);
		capituloUno.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		Capitulo capituloDos = new Capitulo();
		capituloDos.setIdCapitulo(2);
		capituloDos.setNumero(2);
		capituloDos.setTitulo("La sorpresa de Hakimoto");
		capituloDos.setFechaPublicacion(fechaCreacion);
		capituloDos.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(fechaCreacion);
		fanfiction.setFechaActualizacion(fechaActualizacion);
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(autor);
		fanfiction.setClasificacionParental(clasificacion);
		capituloUno.setFanfiction(fanfiction);
		capituloDos.setFanfiction(fanfiction);
		HashSet<Capitulo> listaCapitulos = new HashSet<Capitulo>();
		listaCapitulos.add(capituloUno);
		listaCapitulos.add(capituloDos);
		fanfiction.setCapitulos(listaCapitulos);
		
		lecturaCapituloUno = new Lectura();
		lecturaCapituloUno.setFecha(fechaCreacion);
		lecturaCapituloUno.setUsuario(lector);
		lecturaCapituloUno.setCapitulo(capituloUno);
		
		lecturaCapituloDos = new Lectura();
		lecturaCapituloDos.setFecha(fechaCreacion);
		lecturaCapituloDos.setUsuario(lector);
		lecturaCapituloDos.setCapitulo(capituloDos);
		
		listaLecturas = new ArrayList<Lectura>();
		listaLecturas.add(lecturaCapituloUno);
		
		lecturas = new HashSet<Lectura>();
		lecturas.addAll(listaLecturas);
	}
	
	@Test
	public void crearNuevaLecturaAFanfictionNoLeidoAntes() throws DaoException{
		when(lecturaDao.findByUsuarioAndFanfiction(lector,idFanfiction)).thenReturn(null);
		
		lecturaServicio.actualizarLectura(lecturaCapituloUno);
		
		verify(lecturaDao).save(lecturaCapituloUno);
		
	}
	
	@Test
	public void actualizarLecturaParaFanfictionLeido() throws DaoException{
		when(lecturaDao.findByUsuarioAndFanfiction(lector,idFanfiction)).thenReturn(lecturaCapituloUno);
		
		lecturaServicio.actualizarLectura(lecturaCapituloUno);
		
		verify(lecturaDao).save((Lectura)anyObject());
	}
	
	@Test(expected = DaoException.class)
	public void errorAlActualizarLecturaPorFallaEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(lecturaDao).findByUsuarioAndFanfiction(lector,idFanfiction);
		
		lecturaServicio.actualizarLectura(lecturaCapituloUno);
	}
	
	@Test
	public void eliminarLecturaDeFormaExitosa() throws DaoException{
		
		lecturaServicio.eliminarLectura(lecturaCapituloUno);
		
		verify(lecturaDao).delete(lecturaCapituloUno);
	}
	
	@Test(expected = DaoException.class)
	public void eliminarLecturaDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(lecturaDao).delete(lecturaCapituloUno);
		
		lecturaServicio.eliminarLectura(lecturaCapituloUno);
	}
	
	@Test
	public void eliminarListaLecturasDeFormaExitosa() throws DaoException{
		
		lecturaServicio.eliminarListaLecturas(lecturas);
		
		verify(lecturaDao).delete(lecturas);
		
	}
	
	@Test(expected = DaoException.class)
	public void eliminarListaLecturasDeFormFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(lecturaDao).delete(lecturas);
		
		lecturaServicio.eliminarListaLecturas(lecturas);
	}
	
	@Test
	public void mostrarHistorialLecturaDeFormaExitosa() throws DaoException{
		when(lecturaDao.findByUsuarioOrderByFecha(lector.getIdUsuario())).thenReturn(listaLecturas);
		
		List<Lectura> recibido = lecturaServicio.obtenerHistorialLectura(lector.getIdUsuario());
		
		assertEquals(listaLecturas, recibido);
	}
	
	@Test(expected = DaoException.class)
	public void mostrarHistorialLecturaDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(lecturaDao).findByUsuarioOrderByFecha(lector.getIdUsuario());
		
		lecturaServicio.obtenerHistorialLectura(lector.getIdUsuario());
	}
}
