package com.ubb.fanfiction.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ubb.fanfiction.dao.PuntuacionDao;
import com.ubb.fanfiction.domain.Capitulo;
import com.ubb.fanfiction.domain.ClasificacionParental;
import com.ubb.fanfiction.domain.Fanfiction;
import com.ubb.fanfiction.domain.Puntuacion;
import com.ubb.fanfiction.domain.Usuario;
import com.ubb.fanfiction.exception.DaoException;
import com.ubb.fanfiction.exception.ObjectNotFoundException;
import com.ubb.fanfiction.service.impl.PuntuacionServicioImpl;


@RunWith(MockitoJUnitRunner.class)
public class PuntuacionServicioTests {
	private Fanfiction fanfiction;
	private Capitulo capitulo;
	private Integer idCapitulo;
	private Integer idFanfiction;
	private Integer idUsuario;
	private Puntuacion puntuacion;
	private List<Puntuacion> lista;
		
	@Mock
	private PuntuacionDao puntuacionDao;
	
	@InjectMocks
	private PuntuacionServicio puntuacionServicio = new PuntuacionServicioImpl();
	
	@Before
	public void setUp(){
		Date fechaCreacion = Calendar.getInstance().getTime();
		ClasificacionParental clasificacion = new ClasificacionParental("K (5+)");
		Date fechaActualizacion = Calendar.getInstance().getTime();
		idFanfiction = 1;
		idCapitulo = 1;
		idUsuario = 2;
		
		Usuario evaluador = new Usuario("pedro","pedro@gmail.com","pedropedro","USER",true,fechaCreacion, true);
		
		Usuario autor = new Usuario("mmunoca","mmunocan@hotmail.com","c14v3","USER",true,fechaCreacion, true);
		autor.setIdUsuario(idUsuario);
		
		capitulo = new Capitulo();
		capitulo.setIdCapitulo(idCapitulo);
		capitulo.setNumero(1);
		capitulo.setTitulo("Una carta amenazante");
		capitulo.setFechaPublicacion(fechaCreacion);
		capitulo.setContenido("Lorem ipsum dolor sit amet, egestas est purus wisi augue. Nunc pellentesque... ");
		
		fanfiction = new Fanfiction();
		fanfiction.setIdFanfiction(idFanfiction);
		fanfiction.setTitulo("Una historia infinita");
		fanfiction.setDescripcion("Espero poder actualizar a diario");
		fanfiction.setFechaCreacion(fechaCreacion);
		fanfiction.setFechaActualizacion(fechaActualizacion);
		fanfiction.setEstado("Incompleto");
		fanfiction.setUsuario(autor);
		fanfiction.setClasificacionParental(clasificacion);
		capitulo.setFanfiction(fanfiction);
		
		Set<Puntuacion> puntuacionCapitulo = new HashSet<Puntuacion>();
		puntuacionCapitulo.add(new Puntuacion(capitulo, evaluador, fechaCreacion, (byte)1));
		puntuacionCapitulo.add(new Puntuacion(capitulo, evaluador, fechaCreacion, (byte)5));
		puntuacionCapitulo.add(new Puntuacion(capitulo, evaluador, fechaCreacion, (byte)4));
		capitulo.setPuntuacions(puntuacionCapitulo);
		
		HashSet<Capitulo> listaCapitulos = new HashSet<Capitulo>();
		listaCapitulos.add(capitulo);
		fanfiction.setCapitulos(listaCapitulos);
		
		puntuacion = new Puntuacion();
		puntuacion.setIdPuntuacion(1);
		puntuacion.setFecha(fechaActualizacion);
		puntuacion.setCapitulo(capitulo);
		puntuacion.setUsuario(evaluador);
		puntuacion.setValor((byte)3);
		
		lista = new ArrayList<Puntuacion>();
		lista.add(puntuacion);
	}
	
	@Test
	public void obtenerPromedioPuntuacionesDeUnFanfictionExitosa() throws DaoException{
		Double esperado = 0.0;
		for(Puntuacion p : capitulo.getPuntuacions())
			esperado += p.getValor();
		
		esperado = esperado / (capitulo.getPuntuacions().size());
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(esperado);
		
		Double resultado = puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction);
		
		assertEquals(esperado, resultado);
		
	}
	
	@Test
	public void obtenerPromedioPuntuacionesDeUnFanfictionCuandoNoHayPuntuaciones() throws DaoException{
		Double esperado = 0.0;
		capitulo.setPuntuacions(null);
		when(puntuacionDao.findAverageValorByFanfiction(idFanfiction)).thenReturn(null);
		
		Double resultado = puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction);
		
		assertEquals(esperado, resultado);
		
	}
	
	@Test(expected = DaoException.class)
	public void obtenerPromedioPuntuacionesDeUnFanfictionFallidaPorErrorEnCapaDao() throws DaoException{
		Double esperado = 0.0;
		for(Puntuacion p : capitulo.getPuntuacions())
			esperado += p.getValor();
		
		esperado = esperado / (capitulo.getPuntuacions().size());
		doThrow(Exception.class).when(puntuacionDao).findAverageValorByFanfiction(idFanfiction);
		
		puntuacionServicio.obtenerPromedioPuntuacionFanfiction(idFanfiction);
	}
	
	@Test
	public void obtenerPromedioPuntuacionesDeUnCapituloExitosa() throws DaoException{
		int idCapitulo = capitulo.getIdCapitulo();
		Double esperado = 0.0;
		for(Puntuacion p : capitulo.getPuntuacions())
			esperado += p.getValor();
		esperado = esperado / (capitulo.getPuntuacions().size());
		when(puntuacionDao.findAverageValor(idCapitulo)).thenReturn(esperado);
		
		Double resultado = puntuacionServicio.obtenerPromedioPuntuacionCapitulo(idCapitulo);
		
		assertEquals(esperado, resultado);
		
	}
	
	@Test
	public void obtenerPromedioPuntuacionesDeUnCapituloCuandoNoHayPuntuaciones() throws DaoException{
		Double esperado = 0.0;
		capitulo.setPuntuacions(null);
		when(puntuacionDao.findAverageValor(idCapitulo)).thenReturn(null);
		
		Double resultado = puntuacionServicio.obtenerPromedioPuntuacionCapitulo(idCapitulo);
		
		assertEquals(esperado, resultado);
		
	}
	
	@Test(expected = DaoException.class)
	public void obtenerPromedioPuntuacionesDeUnCapituloFallidaPorErrorEnCapaDao() throws DaoException{
		Double esperado = 0.0;
		for(Puntuacion p : capitulo.getPuntuacions())
			esperado += p.getValor();
		esperado = esperado / (capitulo.getPuntuacions().size());
		doThrow(Exception.class).when(puntuacionDao).findAverageValor(idCapitulo);
		
		puntuacionServicio.obtenerPromedioPuntuacionCapitulo(idCapitulo);
	}
	
	@Test
	public void obtenerPuntuacionPorIdCapituloEIdUsuarioDeFormaExitosa() throws DaoException, ObjectNotFoundException{
		int idUsuario = 2;
		when(puntuacionDao.findByCapituloAndUsuario(idCapitulo,idUsuario)).thenReturn(puntuacion);
		
		Puntuacion recibido = puntuacionServicio.obtenerPuntuacion(idCapitulo, idUsuario);
		
		assertEquals(puntuacion,recibido);
	}
	
	@Test(expected = ObjectNotFoundException.class)
	public void obtenerPuntuacionPorIdCapituloEIdUsuarioDeFormaFallidaPorNoEncontrarCapitulo() throws ObjectNotFoundException, DaoException{
		int idUsuario = 2;
		puntuacion = null;
		when(puntuacionDao.findByCapituloAndUsuario(idCapitulo,idUsuario)).thenReturn(puntuacion);
		
		puntuacionServicio.obtenerPuntuacion(idCapitulo, idUsuario);
		
		
	}
	
	@Test(expected = DaoException.class)
	public void obtenerPuntuacionPorIdCapituloEIdUsuarioDeFormaFallidaPorErrorEnCapaDao() throws ObjectNotFoundException, DaoException{
		int idUsuario = 2;
		doThrow(Exception.class).when(puntuacionDao).findByCapituloAndUsuario(idCapitulo,idUsuario);
		
		puntuacionServicio.obtenerPuntuacion(idCapitulo, idUsuario);
		
	}
	
	@Test
	public void guardarPuntuacionDeFormaExitosa() throws DaoException{
		
		puntuacionServicio.guardarPuntuacion(puntuacion);
		
		verify(puntuacionDao).save(puntuacion);

	}
	
	@Test(expected = DaoException.class)
	public void errorAlGuardarPuntuacion() throws DaoException{
		doThrow(Exception.class).when(puntuacionDao).save(puntuacion);
		
		puntuacionServicio.guardarPuntuacion(puntuacion);

	}
	
	@Test
	public void obtenerListadoPuntuacionesRecibidasDeFormaExitosa() throws DaoException{
		when(puntuacionDao.findByUsuarioFanfictionOrderByFechaDesc(idUsuario)).thenReturn(lista);
		
		List<Puntuacion> listaRecibida = puntuacionServicio.obtenerPuntuacionesRecibidas(idUsuario);
		
		assertEquals(lista, listaRecibida);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListadoPuntuacionesRecibidasDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(puntuacionDao).findByUsuarioFanfictionOrderByFechaDesc(idUsuario);
		
		puntuacionServicio.obtenerPuntuacionesRecibidas(idUsuario);
	}
	
	@Test
	public void obtenerListadoPuntuacionesRealizadasDeFormaExitosa() throws DaoException{
		when(puntuacionDao.findByUsuarioOrderByFechaDesc(idUsuario)).thenReturn(lista);
		
		List<Puntuacion> listaRecibida = puntuacionServicio.obtenerPuntuacionesRealizadas(idUsuario);
		
		assertEquals(lista, listaRecibida);
	}
	
	@Test(expected = DaoException.class)
	public void obtenerListadoPuntuacionesrealizadasDeFormaFallidaPorErrorEnCapaDao() throws DaoException{
		doThrow(Exception.class).when(puntuacionDao).findByUsuarioOrderByFechaDesc(idUsuario);
		
		puntuacionServicio.obtenerPuntuacionesRealizadas(idUsuario);
	}
}
