SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `fanfictiondb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `fanfictiondb` ;

-- -----------------------------------------------------
-- Table `fanfictiondb`.`categoria`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`categoria` (
  `id_categoria` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_categoria`) ,
  UNIQUE INDEX `id_categoria_UNIQUE` (`id_categoria` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`historia_original`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`historia_original` (
  `id_historia_original` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NOT NULL ,
  `id_categoria` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id_historia_original`) ,
  UNIQUE INDEX `id_historia_original_UNIQUE` (`id_historia_original` ASC) ,
  INDEX `historia_original_categoria_idx` (`id_categoria` ASC) ,
  CONSTRAINT `historia_original_categoria`
    FOREIGN KEY (`id_categoria` )
    REFERENCES `fanfictiondb`.`categoria` (`id_categoria` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`usuario` (
  `id_usuario` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NOT NULL ,
  `correo` VARCHAR(45) NOT NULL ,
  `clave` VARCHAR(75) NOT NULL ,
  `rol` VARCHAR(5) NOT NULL ,
  `estado` TINYINT(1) NOT NULL ,
  `fecha_creacion` DATE NOT NULL ,
  `descripcion` MEDIUMTEXT NULL ,
  `token` VARCHAR(45) NULL ,
  `correo_activo` TINYINT(1) NOT NULL ,
  `nuevo_correo` VARCHAR(45) NULL ,
  `vencimiento_token` DATETIME NULL ,
  PRIMARY KEY (`id_usuario`) ,
  UNIQUE INDEX `id_usuario_UNIQUE` (`id_usuario` ASC) ,
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) ,
  UNIQUE INDEX `correo_UNIQUE` (`correo` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`clasificacion_parental`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`clasificacion_parental` (
  `id_clasificacion_parental` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(25) NOT NULL ,
  PRIMARY KEY (`id_clasificacion_parental`) ,
  UNIQUE INDEX `id_clasificacion_parental_UNIQUE` (`id_clasificacion_parental` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`fanfiction`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`fanfiction` (
  `id_fanfiction` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `titulo` VARCHAR(45) NOT NULL ,
  `descripcion` VARCHAR(16000) NOT NULL ,
  `fecha_creacion` DATETIME NOT NULL ,
  `fecha_actualizacion` DATETIME NOT NULL ,
  `estado` VARCHAR(25) NOT NULL ,
  `id_usuario` INT UNSIGNED NOT NULL ,
  `id_clasificacion_parental` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id_fanfiction`) ,
  UNIQUE INDEX `id_fanfiction_UNIQUE` (`id_fanfiction` ASC) ,
  INDEX `fanfiction_usuario_idx` (`id_usuario` ASC) ,
  INDEX `fanfiction_clasificacion_parental_idx` (`id_clasificacion_parental` ASC) ,
  INDEX `fecha_creacion` (`fecha_creacion` ASC) ,
  INDEX `fecha_actualizacion` (`fecha_actualizacion` ASC) ,
  INDEX `estado` (`estado` ASC) ,
  CONSTRAINT `fanfiction_usuario`
    FOREIGN KEY (`id_usuario` )
    REFERENCES `fanfictiondb`.`usuario` (`id_usuario` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fanfiction_clasificacion_parental`
    FOREIGN KEY (`id_clasificacion_parental` )
    REFERENCES `fanfictiondb`.`clasificacion_parental` (`id_clasificacion_parental` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`origen`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`origen` (
  `id_historia_original` INT UNSIGNED NOT NULL ,
  `id_fanfiction` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id_historia_original`, `id_fanfiction`) ,
  INDEX `origen_historia_original_idx` (`id_historia_original` ASC) ,
  INDEX `origen_fanfiction_idx` (`id_fanfiction` ASC) ,
  CONSTRAINT `origen_historia_original`
    FOREIGN KEY (`id_historia_original` )
    REFERENCES `fanfictiondb`.`historia_original` (`id_historia_original` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `origen_fanfiction`
    FOREIGN KEY (`id_fanfiction` )
    REFERENCES `fanfictiondb`.`fanfiction` (`id_fanfiction` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`genero`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`genero` (
  `id_genero` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_genero`) ,
  UNIQUE INDEX `id_genero_UNIQUE` (`id_genero` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`lista_genero`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`lista_genero` (
  `id_fanfiction` INT UNSIGNED NOT NULL ,
  `id_genero` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id_fanfiction`, `id_genero`) ,
  INDEX `lista_genero_fanfiction_idx` (`id_fanfiction` ASC) ,
  INDEX `lsta_genero_genero_idx` (`id_genero` ASC) ,
  CONSTRAINT `lista_genero_fanfiction`
    FOREIGN KEY (`id_fanfiction` )
    REFERENCES `fanfictiondb`.`fanfiction` (`id_fanfiction` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `lsta_genero_genero`
    FOREIGN KEY (`id_genero` )
    REFERENCES `fanfictiondb`.`genero` (`id_genero` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`capitulo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`capitulo` (
  `id_capitulo` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `numero` INT UNSIGNED NOT NULL ,
  `titulo` VARCHAR(45) NOT NULL ,
  `fecha_publicacion` DATETIME NOT NULL ,
  `fecha_actualizacion` DATETIME NOT NULL ,
  `contenido` MEDIUMTEXT NOT NULL ,
  `id_fanfiction` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id_capitulo`) ,
  UNIQUE INDEX `id_capitulo_UNIQUE` (`id_capitulo` ASC) ,
  INDEX `capitulo_fanfiction_idx` (`id_fanfiction` ASC) ,
  CONSTRAINT `capitulo_fanfiction`
    FOREIGN KEY (`id_fanfiction` )
    REFERENCES `fanfictiondb`.`fanfiction` (`id_fanfiction` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`lectura`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`lectura` (
  `id_lectura` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `fecha` DATETIME NOT NULL ,
  `id_capitulo` INT UNSIGNED NOT NULL ,
  `id_usuario` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id_lectura`) ,
  UNIQUE INDEX `id_lectura_UNIQUE` (`id_lectura` ASC) ,
  INDEX `lectura_capitulo_idx` (`id_capitulo` ASC) ,
  INDEX `lectura_usuario_idx` (`id_usuario` ASC) ,
  INDEX `fecha` (`fecha` ASC) ,
  CONSTRAINT `lectura_capitulo`
    FOREIGN KEY (`id_capitulo` )
    REFERENCES `fanfictiondb`.`capitulo` (`id_capitulo` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `lectura_usuario`
    FOREIGN KEY (`id_usuario` )
    REFERENCES `fanfictiondb`.`usuario` (`id_usuario` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`puntuacion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`puntuacion` (
  `id_puntuacion` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `fecha` DATETIME NOT NULL ,
  `valor` TINYINT(3) UNSIGNED NOT NULL ,
  `id_capitulo` INT UNSIGNED NOT NULL ,
  `id_usuario` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id_puntuacion`) ,
  UNIQUE INDEX `id_puntuacion_UNIQUE` (`id_puntuacion` ASC) ,
  INDEX `puntuacion_capitulo_idx` (`id_capitulo` ASC) ,
  INDEX `puntuacion_usuario_idx` (`id_usuario` ASC) ,
  INDEX `fecha` (`fecha` ASC) ,
  CONSTRAINT `puntuacion_capitulo`
    FOREIGN KEY (`id_capitulo` )
    REFERENCES `fanfictiondb`.`capitulo` (`id_capitulo` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `puntuacion_usuario`
    FOREIGN KEY (`id_usuario` )
    REFERENCES `fanfictiondb`.`usuario` (`id_usuario` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fanfictiondb`.`comentario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fanfictiondb`.`comentario` (
  `id_comentario` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `fecha` DATETIME NOT NULL ,
  `contenido` VARCHAR(16000) NOT NULL ,
  `id_capitulo` INT UNSIGNED NOT NULL ,
  `id_usuario` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id_comentario`) ,
  UNIQUE INDEX `id_comentario_UNIQUE` (`id_comentario` ASC) ,
  INDEX `comentario_capitulo_idx` (`id_capitulo` ASC) ,
  INDEX `comentario_usuario_idx` (`id_usuario` ASC) ,
  INDEX `fecha` (`fecha` ASC) ,
  CONSTRAINT `comentario_capitulo`
    FOREIGN KEY (`id_capitulo` )
    REFERENCES `fanfictiondb`.`capitulo` (`id_capitulo` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `comentario_usuario`
    FOREIGN KEY (`id_usuario` )
    REFERENCES `fanfictiondb`.`usuario` (`id_usuario` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

USE `fanfictiondb` ;

CREATE USER 'fanfictionapp' IDENTIFIED BY 'c0ntr4$3n4';

GRANT ALL ON `fanfictiondb`.* TO 'fanfictionapp';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `fanfictiondb`.`categoria`
-- -----------------------------------------------------
START TRANSACTION;
USE `fanfictiondb`;
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (1, 'Animé o Manga');
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (2, 'Libro');
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (3, 'Caricatura');
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (4, 'Comic');
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (5, 'Videojuego');
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (6, 'Película');
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (7, 'Serie de Televisión');
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (8, 'Músico o banda');
INSERT INTO `fanfictiondb`.`categoria` (`id_categoria`, `nombre`) VALUES (9, 'Otro');

COMMIT;

-- -----------------------------------------------------
-- Data for table `fanfictiondb`.`historia_original`
-- -----------------------------------------------------
START TRANSACTION;
USE `fanfictiondb`;
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (1, 'Naruto', 1);
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (2, 'Harry Potter', 2);
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (3, 'Avatar: Last Airbender', 3);
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (4, 'Homestuck', 4);
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (5, 'Pokémon', 5);
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (6, 'Star Wars', 6);
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (7, 'Supernatural', 7);
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (8, 'Screenplays', 8);
INSERT INTO `fanfictiondb`.`historia_original` (`id_historia_original`, `nombre`, `id_categoria`) VALUES (9, 'Wrestling', 9);

COMMIT;

-- -----------------------------------------------------
-- Data for table `fanfictiondb`.`usuario`
-- -----------------------------------------------------
START TRANSACTION;
USE `fanfictiondb`;
INSERT INTO `fanfictiondb`.`usuario` (`id_usuario`, `nombre`, `correo`, `clave`, `rol`, `estado`, `fecha_creacion`, `descripcion`, `token`, `correo_activo`, `nuevo_correo`, `vencimiento_token`) VALUES (1, 'admin', 'admin@admin.cl', '$2a$10$3kCi6ggPWw5vlodSxqf.POi/KMnCjI43l4fKA8XONnp12rdtAm9wO', 'ADMIN', 1, '2018-01-14', 'Cuenta de admiinistrador de prueba', NULL, 1, NULL, NULL);
INSERT INTO `fanfictiondb`.`usuario` (`id_usuario`, `nombre`, `correo`, `clave`, `rol`, `estado`, `fecha_creacion`, `descripcion`, `token`, `correo_activo`, `nuevo_correo`, `vencimiento_token`) VALUES (2, 'mmunocan', 'mmunocan@hotmail.com', '$2a$10$K4Wiwwb2ClfQwUDXYG1R6OZnDQVAQd2P9Ts5znp1OaBVv9iVX0JPq', 'USER', 1, '2018-01-14', 'Cuenta de usuario de prueba', NULL, 1, NULL, NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for table `fanfictiondb`.`clasificacion_parental`
-- -----------------------------------------------------
START TRANSACTION;
USE `fanfictiondb`;
INSERT INTO `fanfictiondb`.`clasificacion_parental` (`id_clasificacion_parental`, `nombre`) VALUES (1, 'K (5+)');
INSERT INTO `fanfictiondb`.`clasificacion_parental` (`id_clasificacion_parental`, `nombre`) VALUES (2, 'K+ (9+)');
INSERT INTO `fanfictiondb`.`clasificacion_parental` (`id_clasificacion_parental`, `nombre`) VALUES (3, 'T (13+)');
INSERT INTO `fanfictiondb`.`clasificacion_parental` (`id_clasificacion_parental`, `nombre`) VALUES (4, 'M (16+)');
INSERT INTO `fanfictiondb`.`clasificacion_parental` (`id_clasificacion_parental`, `nombre`) VALUES (5, 'MA (18+)');

COMMIT;

-- -----------------------------------------------------
-- Data for table `fanfictiondb`.`genero`
-- -----------------------------------------------------
START TRANSACTION;
USE `fanfictiondb`;
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (1, 'Romance');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (2, 'Comedia');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (3, 'Drama');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (4, 'Poema');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (5, 'Aventura');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (6, 'Misterio');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (7, 'Horror');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (8, 'Parodia');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (9, 'Tragedia');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (10, 'Supernatural');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (11, 'Suspenso');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (12, 'Ciencia Ficción');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (13, 'Fantasía');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (14, 'Espiritual');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (15, 'Familiar');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (16, 'Amistad');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (17, 'Policial');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (18, 'Acción');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (19, 'Juvenil');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (20, 'Paranormal');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (21, 'Dolor');
INSERT INTO `fanfictiondb`.`genero` (`id_genero`, `nombre`) VALUES (22, 'Historia épica');

COMMIT;
